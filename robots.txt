User-Agent: *
Allow: /wp-content/uploads/
Disallow: /wp-content/plugins/
Disallow: /readme.html

Sitemap: https://www.atlanticprincess.com/post-sitemap.xml
Sitemap: https://www.atlanticprincess.com/page-sitemap.xml