var App = (function(window){
  "use strict";
  var _this = null;
  var cacheCollection = {}; 
  var wow = new WOW({
    animateClass: 'animated',
    offset:100,
    callback:function(box) {}
  });
  wow.init();
  return {
  	owlCarousel:null,
    init : function(){
      _this = this;  
      /* CUSTOM SELECT */
      this.CustomSelectBox();  
      /* BOAT TYPE */
      this.BoatType(); 
      /* TESTIMONIAL */  
      this.Testimonial();
      /* DATEPICKER */ 
      this.Datepicker();
      /* RANGE SLIDE */ 
      this.PriceRange();
      /* PRODUCT RELATED */ 
      this.RelatedProduct();
      /* OUR TEAM */ 
      this.OurTeam();
      /* HOME SLIDER */ 
      this.HomeSlider();
      jQuery(window).load(function() {
        jQuery("#y-loading").hide().delay(3000);
      });
      if (jQuery('[data-toggle="tooltip"]').length > 0){jQuery('[data-toggle="tooltip"]').tooltip();}
      if (jQuery('.y-back_to_top').length > 0){
        jQuery('.y-back_to_top').on('click', function(){  
          jQuery(".y-line").height(jQuery(this).offset().top)
          jQuery(".y-line").fadeIn();
          setTimeout(function(){ 
            jQuery('html, body').animate({scrollTop : 0},800);  
            return false; 
          }, 500);
        }); 
      }
      if (jQuery('.flexnav').length > 0){jQuery(".flexnav").flexNav();}
      jQuery(window).scroll(function() { 
        if (jQuery('.y-back_to_top').length > 0){
          scroll = jQuery('.y-back_to_top').scrollTop();
          jQuery(".y-line").height(jQuery('.y-back_to_top').offset().top - scroll)
          if (jQuery(document).scrollTop() > 1) {
              jQuery('#y-back_to_top').fadeIn();
              jQuery(".y-line").fadeIn();
              jQuery(".y-yessticky .header").addClass("y-sticky");
              jQuery(".y-notsticky").removeClass("y-header_strip"); 
          } else {
              jQuery('#y-back_to_top').fadeOut();
              jQuery(".y-line").fadeOut();
              jQuery(".header").removeClass("y-sticky");
              jQuery(".y-notsticky").addClass("y-header_strip"); 			  
          }
        }  
      });
      /* Counter */
      if (jQuery('.y-counter').length > 0){jQuery('.y-counter').counterUp({ delay: 100, time: 3000 });}
    },
    getObject: function(selector){
      if(typeof cacheCollection[selector] == "undefined"){
        cacheCollection[selector] = jQuery(selector);
      }
      return cacheCollection[selector];
    },
    CustomSelectBox : function(){
    	jQuery(".custom-select").each(function(){
            jQuery(this).wrap("<span class='select-wrapper'></span>");
            jQuery(this).after("<span class='holder'></span>");
        });
        jQuery(".custom-select").change(function(){
            var selectedOption = jQuery(this).find(":selected").text();
            jQuery(this).next(".holder").text(selectedOption);
        }).trigger('change');
    },
    BoatType: function(){
    	this.owlCarousel = jQuery('#y-boat_carousel');
    	this.owlCarousel.owlCarousel({
		    loop:true,
		    margin:10,
		    nav:true,
		    dots: false,
        responsiveClass:true,
		    navText:["<i class='fa fa-anchor y-left'></i>","<i class='fa fa-anchor y-right'></i>"],
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:4
		        }
		    }
		  }); 
    },
    HomeSlider : function(){
      var yvs = jQuery('#y-home_slider'); 
      if (yvs.length > 0){ 
        yvs.bxSlider({ 
          controls: true
        });   
      }
    },
    Datepicker : function(){
     if (jQuery('#y-check_in').length > 0){  
      jQuery( "#y-check_in" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: 'mm-dd-yy',
        onClose: function( selectedDate ) {
          jQuery( "#y-check_out" ).datepicker( "option", "minDate", selectedDate );
        }
      });
      jQuery( "#y-check_out" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: 'mm-dd-yy',
        onClose: function( selectedDate ) {
          jQuery( "#y-check_in" ).datepicker( "option", "maxDate", selectedDate );
        }
      });
     } 
    },
    Testimonial: function(){
      jQuery('#y-client_testimonial_carousel').owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      }) 
    },
    OurTeam: function(){
      var Ourteam = jQuery('#y-our_team_carousel');
      Ourteam.owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          dots: false,
          navText:["<i class='fa fa-anchor y-left'></i>","<i class='fa fa-anchor y-right'></i>"],
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:2
              },
              1000:{
                  items:4
              }
          }
      }) 
      jQuery('.y-right_anchor').click(function() { 
          Ourteam.trigger('next.owl.carousel');
      }) 
      jQuery('.y-left_anchor').click(function() { 
          Ourteam.trigger('prev.owl.carousel', [300]);
      })
    },
    RelatedProduct: function(){
      var related_slide = jQuery('#y-rel_item_slide');
      related_slide.owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          dots: false,
          navText:["<i class='fa fa-anchor y-left'></i>","<i class='fa fa-anchor y-right'></i>"],
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:2
              },
              1000:{
                  items:4
              }
          }
      }) 
      jQuery('.y-right_anchor').click(function() { 
          related_slide.trigger('next.owl.carousel');
      }) 
      jQuery('.y-left_anchor').click(function() { 
          related_slide.trigger('prev.owl.carousel', [300]);
      })
    },
    PriceRange : function (){
      if (jQuery('#y-price_range').length > 0){ 
        var slider = document.getElementById('y-price_range');
        noUiSlider.create(slider, {
          start: [3000, 20000],
          connect: true,
          range: {
            'min': 0,
            'max': 20000
          }
        })
        var valueInput = document.getElementById('y-value_input'),
          valueSpan = document.getElementById('y-value_span');
        slider.noUiSlider.on('update', function( values, handle ) {
          if ( handle ) {
            valueInput.innerHTML = values[handle];
          } else {
            valueSpan.innerHTML = values[handle];
          }
        });
        valueInput.addEventListener('change', function(){
          slider.noUiSlider.set([null, this.value]);
        });
      }
    } 
  }
})(window);
/****** ANIMATION *****/
jQuery(document).ready(function($) {
	 $(".y-breadcrum .pull-right li:last-child a").replaceWith(function(){
        return $("<span>" + $(this).html() + "</span>");
    });
  App.init();
if(jQuery("#camera_wrap_3").length > 0){ 
	jQuery('#camera_wrap_3').camera({
	  height: '56%',
	  pagination: false,
	  thumbnails: true,
	  imagePath: '../images/'
	});
}
/* Remove empty P tag starts here */
	jQuery('p').each(function() {
		var $this = $(this);
		if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
			$this.remove();
	});
/* Remove empty P tag ends here */
});