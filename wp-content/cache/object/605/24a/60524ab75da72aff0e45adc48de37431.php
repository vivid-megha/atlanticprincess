�p]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1773;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-01-03 11:02:07";s:13:"post_date_gmt";s:19:"2017-01-03 11:02:07";s:12:"post_content";s:4616:"[vc_row css=".vc_custom_1474964690450{padding-top: 0px !important;padding-bottom: 0px !important;}"]
[vc_column el_class="vc_col-sm-8"]

Fraser Island, the largest sand island in the world, is a magnificent island in Queensland situated on the east coast of Australia on the bottom end of the Great Barrier Reef

Home to beautiful adventures.
<h3>Liveaboard The Luxurious Atlantic Princess</h3>
If you don’t get excited for an adventure trip onboard the Atlantic Princess to Fraser Island almost nothing will excite you. The wild, undiscovered, untamed and super inaccessible parts of the island can be reached by tendering in from the Atlantic Princess mothership.

Enjoy waking up at whatever time you desire and getting picked up by your Marlin fishing boat captains for a day of marlin, game and sports fishing.

Then image returning to your floating hotel to a luxuriously air-conditioned room, complete with clean sheets, hot showers, and cold beer?  Then join us for dinner prepared by our chef on the upstairs or aft deck.  Just like a five-star hotel only with the best view in the world.
<h3>Fraser Island Marlin Fishing</h3>
Fraser Island is increasingly becoming the most popular juvenile marlin fishing grounds. The point where the warm currents converge with the coast of Fraser Island act as a reproduction nest for hundreds of fish species, therefore, a large target for small fish which have just been recently hatched.

Blue and black marlin are also the most popular fish around Fraser Island
<h3>Blue Marlin</h3>
Popularly known as the line burners, the blue marlin is a fighter and not easy to catch. But the good news is they are plenty when the warm East Australian Currents (EAC) hit the coast of Fraser Island. These warm waters create ideal conditions for blue marlin survival. Therefore, chances of catching one are incredibly high.
<h3>Black Marlin</h3>
Black marlin is usually fished 40 miles from the northern end of the island, just outside the Breaksea Spit or 50 to 60 miles from Hervey Bay; with an additional 20 miles to the continental shelf.

With an average catch of three marlin fish a day, Fraser Island is a world-class fishery, and if you are lucky you might even catch a striped marlin!
<h3>What Else Can I Do On Fraser Island?</h3>
<img class="alignleft size-medium wp-image-2649" style="margin: 5px 20px 8px 0;" src="https://www.atlanticprincess.com/wp-content/uploads/2017/01/LakeMcKenzieFraserIsland-min-300x200.jpg" alt="" width="300" height="200" />

Every year, deep sea fish enthusiasts and divers have a chance to book a tour to accompany their onboard the luxurious mothership adventure.

Being the largest sand Island in the world, Fraser Island has a little bit of everything. The towering eucalyptus woodland, cool towering rainforests and mangrove forests, peat and wallum swamps.

The freshwater lakes (Lake McKenzie and Lake Wabby) that you can swim in, incredible dunes and coastal heaths. There are also panoramic viewpoints which include the Indian Head and the Cathedrals which make for days of fun, a relaxing holdiay and fantastic photos.

If snorkelling or fishing is not your preferred method of relxaxing there is still plenty of activities to do on Fraser Island. The best part is that you can do all these tours while still living onboard the Atlantic Princess where you will always have a chance to catch a glimpse of the bright and early sun rising from the sea.

[/vc_column][vc_column el_class="vc_col-sm-4"][vc_column_inner]
[vc_column_text el_class="blu-bok-frm"]
<h3>book now</h3>
[booking startmonth='2018-1']
[/vc_column_text][/vc_column_inner][/vc_column]
[/vc_row]

[vc_row][vc_column][vc_row_inner el_class="map_bck"][vc_column_inner]
[vc_column_text]
<!--<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d923833.8535689561!2d152.5909621576785!3d-25.246152431550517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6bec611ec3c665f1%3A0x958afa2ce4c59bca!2sFraser+Island!5e0!3m2!1sen!2sin!4v1519217945069" width="100%" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>-->
<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d923834.3514337823!2d152.5883780050399!3d-25.246086950696675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6bec8bd2e5355601%3A0x400eef17f20f070!2sFraser+Island+QLD+4581!5e0!3m2!1sen!2sau!4v1519379010029" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]";s:10:"post_title";s:13:"Fraser Island";s:12:"post_excerpt";s:513:"<h4>Set in the beautiful adertic </h4>
<ul>
                                  <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</li>
                                  <li>tempor incididunt ut laboreet dolore magna aliqua. Ut enim ad minim.</li>
                                  <li>Lorem ipsum dolor sit amet, consectetur.</li> 
                                  <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</li>
                                </ul>";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:13:"fraser-island";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-02-26 19:37:50";s:17:"post_modified_gmt";s:19:"2018-02-26 09:37:50";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:73:"http://118.102.206.67/wp/yacht1/?post_type=yachtsailing_desti&#038;p=1773";s:10:"menu_order";i:0;s:9:"post_type";s:18:"yachtsailing_desti";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}