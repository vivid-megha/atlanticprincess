D�p]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"2528";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-02-19 10:47:32";s:13:"post_date_gmt";s:19:"2018-02-19 05:47:32";s:12:"post_content";s:5246:"The much-awaited 12 days of action-packed celebration of the Gold Coast 2018 Commonwealth Games kicks off on April 4 and concludes with the closing ceremony on April 15. Have you booked your accommodation? If yes, then good on you. If not, well we have an exciting idea for you to live aboard the majestic Atlantic Princess Super Yacht!

Whether this is your first time to experience this world-famous Commonwealth Games, you won’t want to miss the world-class event on the Gold Coast. With 70 nations and territories participating, we're definitely sure that you and your family or the rest of your group will enjoy a whole new accommodation experience when you book the Atlantic Princess as your floating hotel.

Besides, what better way to relax and enjoy leisure activities than at sea? After all, you get a chance to experience life as a true blooded Aussie, in the water just like our world famous swimmers. The Atlantic Princess sets its anchor on the best swimming spots on the Gold Coast and can also tender you into the beach or jetties so you avoid traffic jams.

Discover the pristine, deep waters of Stradbroke Island and Wave Break Island and enjoy beginner to advanced snorkeling like no other! Guests from cooler Commonwealth member countries will absolutely enjoy the sun, the sand, and the waves of the Gold Coast!

<strong>Book the Atlantic Princess As Your Floating Hotel for the Upcoming Commonwealth Games</strong>

Reserving any of the local hotels is a good accommodation choice for the upcoming Commonwealth Games, but you have a choice to relax aboard the luxurious Atlantic Princess Super Yacht, fully catered by your own private chef with all meals and beverages included. Dine on your favorite meals while our stewardess takes care of all your other requirements.

Let the calm waters rock you to sleep at night and wake up to breakfast being served to overlook the best view on the Gold Coast.

Locals, interstate and international visitors will surely enjoy a one-of-a-kind on-board accommodation experience on our very own sweetheart of the sea, the Atlantic Princess.

<strong>Cruising and Fishing Adventures</strong>

The Gold Coast also happens to be one of the host cities of the <strong>2018 Commonwealth Games</strong>, and is a premier destination for the Atlantic Princess with her home berth at the Versace Marina in Main Beach.

Witness stunning views of the sunrise and sunset aboard the superyacht and created a  totally memorable cruising experience. How about trying some fishing sports? Not only will you get to take home your catch, but you also score nice stories to tell back home in between going to the events of the Commonwealth Games.

<strong>Spacious Deck View</strong>

The Atlantic Princess also boasts of a spacious top deck where you can hang out with your loved ones or friends and enjoy conversations with a few beers, glasses of wine or champagne. Let our chef cook you a mouthwatering BBQ or better yet the first taste your own freshly-caught fish.  Make the conversation at sea more interesting and your Commonwealth Games more memorable aboard the Atlantic Princess.

<strong>Luxurious Staterooms</strong>

The Atlantic Princess certainly makes the perfect accommodation choice for the <strong>Commonwealth Games on the Gold Coast</strong>. Relax in our Master Stateroom, Starboard Suite, or Port Suite. The Master Stateroom can accommodate 2 people while both the Starboard and Port Suites can comfortably sleep 3 pax each. All 3 cabins have their own ensuite and air conditioning. The Atlantic Princess is yours to enjoy on your chosen dates and what a better way to see the real Australia than staying in a hotel room?

<strong>Exquisite, Chef-Prepared Meals</strong>

Besides the at-sea recreation activities, our guests will also delight in our all-inclusive chef-prepared meals. Our onboard chef always serves fresh and mouth-watering dishes spiced with their personal touch. Think about fine-dining at the sea with the company of loved ones and some good conversation. Choosing us as your accommodation of choice for the <strong>Commonwealth Games 2018</strong> will definitely give you an unforgettable getaway.

<strong>On-Shore Sports Activities</strong>

Meet fellow locals and foreigners from participating countries as we dock for some beach volleyball, restaurant and pub hopping across the beach strip, and attendance to the world-class sports venues on the Gold Coast to cheer for your country’s representatives.

Transportation can be arranged for you to attend the events being held at the Broadbeach Bowls Club, Carrara Sports and Leisure Centre, Carrara Stadium, Coolangatta Beachfront, Coomera Indoor Sports Centre, Gold Coast Convention and Exhibition Centre, Gold Coast Hockey Centre, Nerang Mountain Bike Trails, Optus Aquatic Centre, Oxenford Studios, Robina Stadium, and Southport Broadwater Parklands.

So, make sure you bring your camera, power bank, and even a selfie stick for an action-packed 2018 Gold Coast Commonwealth Games experience. You're sure to enjoy this leg of the sports battle.

<strong>Enquire About Our Rates Today So You Don't Miss Out On This Once In A Lifetime Experience.</strong>";s:10:"post_title";s:85:"Celebrate This Years 2018 Commonwealth Games Aboard The Atlantic Princess Super Yacht";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:89:"celebrate-the-gold-coast-2018-commonwealth-games-aboard-the-atlantic-princess-super-yacht";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-02-26 19:09:49";s:17:"post_modified_gmt";s:19:"2018-02-26 09:09:49";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:40:"https://www.atlanticprincess.com/?p=2528";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}