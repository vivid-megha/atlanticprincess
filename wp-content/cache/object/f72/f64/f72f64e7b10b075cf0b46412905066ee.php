D�p]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"2610";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2018-02-22 23:14:54";s:13:"post_date_gmt";s:19:"2018-02-22 13:14:54";s:12:"post_content";s:2555:"For those seeking a way out of their mundane day-to-day living, booking a night on our Atlantic Princess Superyacht is the best decision that you can make today! Not only that, because you get to invite 7 other awesome family members, friends, or colleagues to spend an overnight or weekend getaway with you onboard our floating hotel.

Enjoy Luxury Accommodation At The Sea

Live like kings and queens for your stay onboard the Atlantic Princess Superyacht as we take you on an unforgettable journey to any of our 4 destinations: (1) Cairns, (2) The Whitsundays, (3) Fraser Island, and (4) The Gold Coast. These premier destinations in Australia boasts pristine waters, stunning views of the sunrise and the sunset, lovely islands, rich and colourful marine life, and so much more. Locals and foreign tourists alike enjoy the scenery, the adventure, and the 5-star treatment they get to experience onboard.

Our overnight accommodation costs only $5,990 AUD for up to 8 guests in 3 luxury staterooms and includes a private chef and stewardess plus all your standard beverages and water activities. Plus you cruise between different locations like the world-renowned Great Barrier Reef and all amenities are included and you get the entire Atlantic Princess to yourselves.

The Unmatched Atlantic Princess Superyacht Luxury Experience

Atlantic Princess takes you to a sweet escapade away from the hustle and bustle of the city, so you enjoy breathtaking views, adrenaline-pumping water sports like fishing, snorkeling, paddle-boarding, cruising between islands, swimming, and a whole lot more.

You can even enjoy your freshly-caught fish because our chef just knows exactly how to cook them to your liking. Oh, and the rate is also inclusive of morning and afternoon teas, all 3 delectable meals (breakfast, lunch, and dinner), and standard alcoholic beverages. What better way to dine on fresh, exquisite delicacies than on our spacious upper view deck while chatting with good company. Everyone can likewise enjoy a barbecue night while admiring the picturesque Australian sky.

How many times in a year will you get to experience luxurious comfort at the sea? With all the hard work that you've been doing your whole life, you certainly deserve this one-of-a-kind treat for yourself and friends.

Book Now and Set Sail

Are you starting to fancy all these awesome things awaiting you onboard? <a href="https://www.atlanticprincess.com/" target="_blank" rel="noopener">Enquire now</a> so we can help you organise the perfect holiday.";s:10:"post_title";s:57:"Book the Atlantic Princess For Your Next Friend's Holiday";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:56:"book-the-atlantic-princess-for-your-next-friends-holiday";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-02-26 19:21:25";s:17:"post_modified_gmt";s:19:"2018-02-26 09:21:25";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:40:"https://www.atlanticprincess.com/?p=2610";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}