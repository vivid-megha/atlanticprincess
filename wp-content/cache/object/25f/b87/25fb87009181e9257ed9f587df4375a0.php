�p]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1772;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-01-03 11:01:48";s:13:"post_date_gmt";s:19:"2017-01-03 11:01:48";s:12:"post_content";s:5338:"[vc_row css=".vc_custom_1474964690450{padding-top: 0px !important;padding-bottom: 0px !important;}"]
[vc_column el_class="vc_col-sm-8"]

The Gold Coast is Australia’s Las Vegas located at the lower end of tropical Queensland and one of the most popular tourist destinations is famous for its surfing beaches, high-rise skyline, theme parks, nightlife, and rainforest hinterland.

The iconic Surfer's Paradise with its golden sandy beaches and year round waves offer a memorable experience to both new and experienced surfers, while the sheltered Broadwater provides perfect conditions for cruising all year round.

Gold Coast is an ideal charter destination, which offers a variety of diverse cruising options that include:
<h5><b>Stradbroke Island</b></h5>
<img class="alignleft size-medium wp-image-2650" style="margin: 5px 20px 8px 0;" src="https://www.atlanticprincess.com/wp-content/uploads/2017/01/peoplesnorkelling-min-300x200.jpg" alt="" width="300" height="200" />

Initially known as theMinjerribah to the Quandamooka people (original inhabitants), Stradbroke Island is truly a cultural paradise. Just a few miles off the Gold Coast, both the North and South Stradbroke Island captivate locals and tourists alike. They offer a perfect getaway from the hustle and bustle of the city life.
<h5>Wave Break Island</h5>
Wave Break Island is a favourite spot for day visitors since it is located only 7 miles off the Gold Coast.  Beginner and advanced snorkeling is best around Wave Island.The calm waters are also suitable for diving making it easy to spot hundreds of tiny and beautiful fish, of up to 50 different species and other magical marine life in as shallow as 0-15metres. Most of the marine life is human-friendly, thereby providing excellent opportunities for fish feeding and underwater photography.
<h3>Unchartered Waters</h3>
Our popular charter packages onboard the Atlantic Princess Super Yacht cater for leisure holidays, family celebrations, birthdays, anniversaries, corporate events and weekend getaways, offering a perfect hideaway from the chaos of the city.

Let us help you create a custom charter that suits your requirements and preferences. Think about the union of serenity, adventure, and recreation at the sea.
<h3>Atlantic Princess Super Yacht Corporate Charters</h3>
Atlantic Princess Super Yacht corporate charters offer unique and spectacular backdrops with a vibrant atmosphere and attractive platform for the guests. Our unparalleled services provided by our dedicated crew are ideally suited for networking and formal business meetings, which will give you an edge over your competitors and leave a lasting impression on your business associates.

The following corporate events can be organised aboard the Atlantic Princess Super Yacht :
<ul style="list-style: disc !important; margin-left: 28px;">
 	<li>Product Launches</li>
 	<li>Meetings and Conferences</li>
 	<li>Corporate Entertainment and Hospitality</li>
 	<li>Photo Shoots</li>
 	<li>Team Building</li>
 	<li>Employee Rewards</li>
</ul>
<h3>Gold Coast Day Tours</h3>
Atlantic Princess is famous for our day or half-day trips. We offer fantastic charter packages for cruising between the nearest islands– the North or the South Stradbroke Islands and Wave Island.
<h3>Gold Coast Sunset Tours</h3>
Rarely do we get a chance to enjoy a backdrop of the beautiful tropical hinterland and a perfect view of the sunset amidst the city. This is the reason why our guests keep coming back to Atlantic Princess and spreading the word about their perfect by the sea getaway.

Just imagine sipping a glass of crisp cold wine while chatting with your significant other or friends on our spacious top deck. Truly, Atlantic Princess offers an experience like no other. From the smell of fresh sea air to the feel of the Gold Coast breeze to the 5-star rated onboard services of our crew, everyone will leave the Atlantic Princess with a big smile and fond memories.

Why keep dreaming when you can make all these a reality? Join us for an unforgettable sunset charter package on the Gold Coast Broadwater! Our sundowner packages are designed for a friendly social atmosphere. Share the onboard experience with old and new friends, special someone, colleagues, or business partners.

The Atlantic Princess is a luxury private super yacht and out extra-large deck can host corporate team buildings for up to 30 pax for special events. Located at the Versace Marina we are very close to Surfers Paradise and can organis a transfer from your hotel.

[/vc_column][vc_column el_class="vc_col-sm-4"][vc_column_inner]
[vc_column_text el_class="blu-bok-frm"]
<h3>book now</h3>
[booking startmonth='2018-1']
[/vc_column_text][/vc_column_inner][/vc_column]
[/vc_row]

[vc_row][vc_column][vc_row_inner el_class="map_bck"][vc_column_inner]
[vc_column_text]
<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d45806.27808954312!2d153.37646171527004!3d-27.94387268483305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b910fcacb5b1c6f%3A0xf02a35bd720f020!2sWave+Break+Island!5e0!3m2!1sen!2sau!4v1519379123570" width="100%" height="600" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]";s:10:"post_title";s:21:"Gold Coast Queensland";s:12:"post_excerpt";s:513:"<h4>Set in the beautiful adertic </h4>
<ul>
                                  <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</li>
                                  <li>tempor incididunt ut laboreet dolore magna aliqua. Ut enim ad minim.</li>
                                  <li>Lorem ipsum dolor sit amet, consectetur.</li> 
                                  <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</li>
                                </ul>";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:21:"gold-coast-broadwater";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-02-26 19:17:06";s:17:"post_modified_gmt";s:19:"2018-02-26 09:17:06";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:73:"http://118.102.206.67/wp/yacht1/?post_type=yachtsailing_desti&#038;p=1772";s:10:"menu_order";i:0;s:9:"post_type";s:18:"yachtsailing_desti";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}