�p]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:1774;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-01-03 11:02:20";s:13:"post_date_gmt";s:19:"2017-01-03 11:02:20";s:12:"post_content";s:7255:"[vc_row css=".vc_custom_1474964690450{padding-top: 0px !important;padding-bottom: 0px !important;}"]
[vc_column el_class="vc_col-sm-8"]
<h3>Atlantic Princess Super Yacht Tours and Trips to The Whitsundays Islands</h3>
Whitsundays Islands, a classic and a favourite cruising destination due to its easy accessibility has made it one of the most popular travel destinations across Australia. With all 74 islands located within 100 miles of each other and 31 miles off Shute Harbor, cruises have been one of the most preferred modes of travel to experience the wonders of the Great Barrier Reef.
<h3>Summer Cruising At Its Best</h3>
The best way to experience what Whitsunday Islands have to offer is through Island hopping. Travelling to all 74 Islands could be hectic, but with a ‘sea-home’, it is the best way to truly explore all that is on offer.  Land-tours are also easily accessible from the Atlantic Princess rather than flying between islands, in fact, some islands are totally inaccessible unless you arrive by sea.

The Atlantic Princess is well suited for island hopping adventures. With a considerably high speed, large fuel tank, and experienced crew, we can create an exclusive package for you and your family or friends to your preferred islands including;
<ol>
 	<li>Whitsunday Island</li>
 	<li>Whitehaven Beach</li>
 	<li>Hamilton Island</li>
 	<li>Hayman Island</li>
 	<li>Hook Island</li>
</ol>
<h3>Australia’s Great Natural Wonder</h3>
The Whitsundays islands are spread through one of the greatest wonders of the world – the Great <img class="alignright size-medium wp-image-2624" style="margin: 5px 20px 8px 0;" src="https://www.atlanticprincess.com/wp-content/uploads/2018/02/BarrierReef3-300x228.jpg" alt="" width="300" height="228" />
Barrier Reef which is home to over 2,800 different coral reefs and stretches for over 2,300km from New Papua Guinea up North and all along the Australian coast down to Bundaberg. The Great Barrier Reef is made of the inner fringing reefs (which surround most of the Whitsundays Islands) and the outer Great Barrier Reef.

The inner reef is made of soft corals which create a habitable home for juvenile fish and a diverse variety of small fish species with exceptions of few larger marine species such as the Maori Wrasse, trevallies, reef sharks and green turtles.
<h3>Island Hopping Around The Whitsundays</h3>
The Whitsundays Islands offer a diverse array of activities, your visit would not be complete without these top ten best activities to do while on cruising around the Whitsundays.
<ul style="margin: 0 0 20px 25px;">
 	<li>Crocodile Safari on Proserpine River</li>
 	<li>Camping on one of the islands (check with us first as you may need a permit)</li>
 	<li>Hiking</li>
 	<li>Bird watching (watching the migratory birds of the Goorganga wetlands)</li>
 	<li>Skydiving (what a way to see all of the islands!)</li>
 	<li>Scenic drives across the Whitsundays National Park</li>
 	<li>Snorkelling</li>
 	<li>Line and Reef fishing</li>
 	<li>Paddleboarding</li>
</ul>
<h3>Five Of The Best Whitsunday Destinations You Won't Want to Miss</h3>
<h5><b>Hook Island</b></h5>
Popularly renown because of the Luncheon Bay, Manta Ray Bay, and Butterfly Bay, Hook Island still holds on to its most natural rocky and rugged appearance. It is also famous for its fringing reefs which offer a diverse and beautiful environment for snorkeling and scuba diving. The island has hiking trails which run through the Island’s National Park to the rainforests, rocky headlands viewpoints and the coral-strewn beaches

The long fjord-like Nara Inlet on the South coast act as a docking bay for many yachts. Just nearby, there is a favourite spot for sightseers who can hike to the ancient cave shelters created by the original Ngaro people, and to the beautiful waterfall.
<h5><b>Hamilton Island</b></h5>
Hamilton Island is one of the most inhabited islands. It’s famous for its holiday homes and bungalows. It is best suited for business and pleasure since it has its commercial airport and a golf course. Other notables on the island include poolside coconut cocktails, sulfer crested cockatoos, palm trees, bars, restaurants and sailboats This Island is easily accessible using the Atlantic Princess since its only 27km from the mainland. We can even pick you up from the Hamilton Island Marina.
<h5><b>Whitehaven Beach</b></h5>
<img class="alignleft wp-image-2424" style="margin: 5px 20px 8px 0;" src="https://www.atlanticprincess.com/wp-content/uploads/2018/02/img-1-300x300.jpg" alt="" width="300" height="300" />

Stretching for over seven kilometres, the beach is covered with a pristine environment which has earned it the worlds’ most eco-friendly beach title. It was also dubbed as Queensland cleanest beach and can only be reached by sea, seaplane or helicopter.The island is protected by Whitsunday Islands National Park and the large barrier reef. The pure white sand which is 98% silica is washed ashore by swirls of turquoise, blue and green water.
<h5><b>Daydream Island</b></h5>
Best suited for family and kids, this island is situated only 5 km off the mainland. It is ideal for family day trips with activities ranging from snorkeling, canoeing, swimming, and sailing Hobie cats to the indoor spa and an outdoor aquarium. After a recent cyclone, it is set to reopen this August 2018.
<h5><b>Whitsunday Island</b></h5>
The largest of all 74 islands and one of the most visited in Australia, Whitsunday Island, is the home to the exquisite Whitehaven Beach and the Hill Inlet Lookout. It is still in its most natural way since it has no resort or amenities the primary reason as to why it receives millions of tourists yearly who come to enjoy the beach, the silky silica sand, and the lookout. The island can be accessed using our mothership directly by the beach or through Tongue Bay.The Atlantic Princess super yacht charter packages provide an exclusive and luxurious experience unlike any other. Whether you want to explore close by on Daydream Island for a day trip with your family, or want an adventure along the other islands to the outer reef, we have something for everyone.

[/vc_column][vc_column el_class="vc_col-sm-4"][vc_column_inner]
[vc_column_text el_class="blu-bok-frm"]
<h3>Book Now</h3>
[booking startmonth='2018-1']
[/vc_column_text][/vc_column_inner][/vc_column]
[/vc_row]

[vc_row][vc_column][vc_row_inner el_class="map_bck"][vc_column_inner]
[vc_column_text]
<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d239404.00401533276!2d148.8236893311632!3d-20.354521941437948!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sau!4v1519293095700" width="100%" height="450" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

<!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d29941.387102024164!2d148.718118!3d-20.272376!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6bd835490bcaaf15%3A0x500eef17f210a10!2sAirlie+Beach+QLD+4802%2C+Australia!5e0!3m2!1sen!2sus!4v1519217867140" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]";s:10:"post_title";s:15:"The Whitsundays";s:12:"post_excerpt";s:351:"<h4>Set in the beautiful adertic</h4>
<ul>
 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</li>
 	<li>tempor incididunt ut laboreet dolore magna aliqua. Ut enim ad minim.</li>
 	<li>Lorem ipsum dolor sit amet, consectetur.</li>
 	<li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</li>
</ul>";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"whitsunday-island";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-02-26 19:18:40";s:17:"post_modified_gmt";s:19:"2018-02-26 09:18:40";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:73:"http://118.102.206.67/wp/yacht1/?post_type=yachtsailing_desti&#038;p=1774";s:10:"menu_order";i:0;s:9:"post_type";s:18:"yachtsailing_desti";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}