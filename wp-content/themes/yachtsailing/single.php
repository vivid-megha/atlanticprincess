<?php get_header(); ?>
<?php echo get_template_part("header","title");?>
<?php 
   $postid = $post->ID;
   $post_title_value = get_the_title($postid);
   $post_image = get_post_meta($postid,"_cmb_featured_image",true);	
   
	 $attachment_id = attachment_url_to_postid( $post_image );
	 $src = wp_get_attachment_image_src($attachment_id,'yachtsailing-blog-big');
	 $post_image = $src[0];	   
   $post_category = get_the_category();		   
?>      
<section id="y-single_info">
          <div class="y-single_info">
            <div class="container">
                <div class="row  y-section_content">                
                    <div class="clearfix row">
                      <div class="wpb_wrapper">
                        <div class="wpb_row vc_inner vc_row-fluid y-single_info_inner container">
                        <div class="col-lg-8 col-md-8 col-sm-7">
                          <div class="y-blog_detail_content">                          
                            <img src="<?php echo esc_url($post_image, 'yachtsailing')?>" class="img-responsive" alt="">
                            <div class="y-blog_detail_text">
                                                       
                            <?php 
                                while(have_posts()): the_post();
                                    echo wpautop(do_shortcode(get_the_content()));
                                endwhile;
                            ?>
                            </div>
                            <footer class="post-meta">
                                <span class="posy-categories">
                                    <i class="fa fa-tag" aria-hidden="true"></i>
                                <?php 
								 $count_categories = count($post_category);
								 $num = 0;
								 foreach($post_category as $category)
								 {
									 $num++;
									 $category_name = $category->name;
									 $category_id = $category->cat_ID;
									 $category_link = get_category_link( $category_id );
							 ?>
                             <a href="<?php echo $category_link; ?>"><?php echo $category_name; ?></a>
							<?php
									 if($num < $count_categories)
									 {
									 echo ",";
									 }
								 }
								 ?>
                                </span>
                            </footer> 
                            <?php comments_template(); ?>
                          </div>    
                        </div>
                        <?php get_sidebar("blog"); ?>
                    </div>
                </div>
                    </div>
                </div>
            </div>
          </div>
        </section>
		<?php get_footer(); ?>