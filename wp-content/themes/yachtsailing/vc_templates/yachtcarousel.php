<?php
$css = '';
extract(shortcode_atts(array(
    'css' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
$slider_values = vc_param_group_parse_atts( $atts['titles'] );
?>
<div id="y-slideshow">
   <div class="y-slideshow">
      <div class="row"> 
        <div class="camera_wrap camera_emboss" id="camera_wrap_3">
<?php
foreach($slider_values as $slider_value)
{
	$slide_title = $slider_value["slide_title"];
	$slide_imageID = $slider_value["slide_image"];
	$slide_imageARRAY = wp_get_attachment_image_src($slide_imageID,"full");
	$slide_thumbARRAY = wp_get_attachment_image_src($slide_imageID);
	 $slide_video = $slider_value["slide_video"]; 
	$slide_image = $slide_imageARRAY[0];
	$slide_thumb = $slide_thumbARRAY[0];
?>
                      <div data-thumb="<?php echo esc_url($slide_thumb, 'yachtsailing'); ?>" data-src="<?php echo esc_url($slide_image, 'yachtsailing');?>">
                      <?php if($slide_video != ''){ echo $slide_video; }?>                        
                      </div>
<?php	
}
?>
                    </div>
                  </div>
             </div>
</div>
<?php
 echo $this->endBlockComment('yachtcarousel'); ?>	