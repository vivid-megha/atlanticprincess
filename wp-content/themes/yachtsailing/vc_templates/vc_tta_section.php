<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $content - shortcode content
 * @var $this WPBakeryShortCode_VC_Tta_Section
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
$this->resetVariables( $atts, $content );
WPBakeryShortCode_VC_Tta_Section::$self_count ++;
WPBakeryShortCode_VC_Tta_Section::$section_info[] = $atts;
$isPageEditable = vc_is_page_editable();
$background_color = $atts["background_color"];
$border_color = $atts["border_color"];
$border_size = $atts["border_size"];
$padding_top = $atts["padding_top"];
$padding_bottom = $atts["padding_bottom"];
$padding_left = $atts["padding_left"];
$padding_right = $atts["padding_right"];
$styles = "";
$attributes = "";
$styles = array();
$attributes = array();
	if ( $background_color ) {
		$styles[] = vc_get_css_color( 'background', $background_color );
	}
	if ( $border_color ) {
		$styles[] = vc_get_css_color( 'border-color', $border_color );
	}
	if ( $border_size ) {
		$styles[] = vc_get_css_color( 'border-width', $border_size."px" );
	}
	if ( $padding_top ) {
		$styles[] = vc_get_css_color( 'padding-top', $padding_top."px");
	}
	if ( $padding_bottom ) {
		$styles[] = vc_get_css_color( 'padding-bottom', $padding_bottom."px" );
	}
	if ( $padding_left ) {
		$styles[] = vc_get_css_color( 'padding-left', $padding_left."px" );
	}
	if ( $padding_right ) {
		$styles[] = vc_get_css_color( 'padding_right', $padding_right."px" );
	}
if ( $styles ) {
	$attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}
$attributes = implode( ' ', $attributes );
$output = '';
$output .= '<div '.$attributes.' class="' . esc_attr( $this->getElementClasses() , 'yachtsailing') . '"';
$output .= ' id="' . esc_attr( $this->getTemplateVariable( 'tab_id' ) , 'yachtsailing') . '"';
$output .= ' data-vc-content=".vc_tta-panel-body">';
$output .= '<div class="vc_tta-panel-heading">';
$output .= $this->getTemplateVariable( 'heading' );
$output .= '</div>';
$output .= '<div class="vc_tta-panel-body" >';
if ( $isPageEditable ) {
	$output .= '<div data-js-panel-body>'; // fix for fe - shortcodes container, not required in b.e.
}
$output .= $this->getTemplateVariable( 'content' );
if ( $isPageEditable ) {
	$output .= '</div>';
}
$output .= '</div>';
$output .= '</div>';
echo $output;