<?php
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''   ,
    'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* ctitle variables starts here */
    $orderby = $atts["orderby"];
    $order = $atts["order"];
    $post_type = $atts["team_post_type"];
    $number_posts = $atts["number_posts"];
    $orderby = $atts["orderby"];
    $order = $atts["order"];
    
    if($number_posts == "" )
    {
        $number_posts = "-1";
    }
?>
 <div id="y-our_team">
    <div class="y-our_team"> 
      
        <div class="y-our_team_list">
           <div class="clearfix y-our_team_carousel" id="y-our_team_carousel">
<?php   
    $team_args = wp_get_recent_posts( array( 'numberposts' => $number_posts , 'orderby' => $orderby, 'order' => $order, 'post_type' => $post_type) );
        foreach($team_args as $team_arg)
        {
          $post_image = get_post_meta($team_arg["ID"],"_cmb_featured_image",true);            
          $team_designation = get_post_meta($team_arg['ID'],"_cmb_team_designation",true);
          $team_fbook = get_post_meta($team_arg['ID'],"_cmb_team_fbook",true);
          $team_twitter = get_post_meta($team_arg['ID'],"_cmb_team_twitter",true);
          $team_linkedlink = get_post_meta($team_arg['ID'],"_cmb_team_linkedlink",true);
          
?>
                                        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item">
                                            <div class="y-our_team_overlay">                                                
                                                   <img src="<?php echo esc_url($post_image, 'yachtsailing');?>" alt=""  class="y-team_img"  />                                               <?php 
                                                    if($team_fbook != "" || $team_twitter != "" || $team_linkedlink != "" )
                                                    {
                                                   ?> 
                                                <div class="y-our_team_social">
                                                    <ul>
                                                    <?php if($team_fbook != ''){?>
                                                        <li>
                                                            <a href="<?php echo esc_url($team_fbook, 'yachtsailing');?>" class="fa fa-facebook"></a>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if($team_twitter != ''){?>                                                    
                                                        <li>
                                                            <a href="<?php echo esc_url($team_twitter, 'yachtsailing');?>" class="fa fa-twitter"></a>
                                                        </li>
                                                    <?php } ?>                                                        
                                                    <?php if($team_linkedlink != ''){?>                                                        
                                                        <li>
                                                            <a href="<?php echo esc_url($team_linkedlink, 'yachtsailing'); ?>" class="fa fa-linkedin"></a>
                                                        </li>
                                                    <?php } ?>                                                        
                                                    </ul>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <div class="y-team_name">
                                                <span><?php echo esc_html($team_arg['post_title'], 'yachtsailing'); ?></span>
                                            </div>
                                            <?php if($team_designation != ''){ ?>
                                              <span class="y-team_post"><?php echo esc_html($team_designation, 'yachtsailing');?></span>
                                             <?php } ?>
                                        </div>
<?php } ?>   
                                    </div> 
                                     
                                    <div class="y-arrow_line">
                                      <i class="fa fa-anchor y-left_anchor"></i>
                                      <i class="fa fa-anchor y-right_anchor"></i>
                                    </div>
                                </div> 
                            </div>                            
                          
                        </div>                     
<?php echo $this->endBlockComment('teamowlCarousel'); ?>