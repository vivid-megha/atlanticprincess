<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $width
 * @var $css
 * @var $offset
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Column_Inner
 */
$el_class = $width = $css = $offset = '';
$output = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$width = wpb_translateColumnWidthToSpan( $width );
$width = vc_column_offset_class_merge( $offset, $width );
if(is_singular("yachtsailing_desti"))
{
	$row_class = " row";
}
else
{
	$row_class = "";	
}
$css_classes = array(
	$this->getExtraClass( $el_class ),
	'wpb_column'.$row_class,
	'',
	$width,
);
if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') )) {
	$css_classes[]='vc_col-has-fill';
}
$border_size = $atts["border_size"];
$border_style = $atts["border_style"];
$border_color = $atts["border_color"];
$styles = "";
$attributes = "";
$styles = array();
$attributes = array();
	if ( ! empty ($border_size)) {
		$styles[] = vc_get_css_color( 'border-width', $border_size."px" );
	}
	if ( ! empty ($border_style)) {
		$styles[] = vc_get_css_color( 'border-style', $border_style );
	}
	if ( ! empty ($border_color)) {
		$styles[] = vc_get_css_color( 'border-color', $border_color );
	}
	
if ( $styles ) {
	$attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}
$attributes = implode( ' ', $attributes );
$wrapper_attributes = array();
$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) , 'yachtsailing') . '"';
$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
$output .= '<div class="wpb_wrapper">';
$output .= wpb_js_remove_wpautop( $content );
$output .= '</div>';
$output .= '</div>';
echo $output;
