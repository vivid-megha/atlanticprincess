<?php
$css = '';
extract(shortcode_atts(array(
    'css' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* Blog arguments options starts here */
		$post_type = $atts["default_post_type"];
		$number_posts = $atts["number_posts"];
		$orderby = $atts["orderby"];
		$order = $atts["order"];
		$post_display_style = $atts["post_display_style"];
		$post_date = get_the_date("d");
		$post_month = get_the_date("F");
		$post_year = get_the_date("Y");
		$current_user = wp_get_current_user();
/* Blog arguments options ends here */
/* Blog listing options starts here */
	$listing_date_visibility = $atts["listing_date_visibility"];
	$listing_info_visibility = $atts["listing_info_visibility"];
/* Blog listing options ends here */
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$post_args = array("posts_per_page"=> $number_posts, "orderby" => $orderby, "order" => $order, "post_type" => $post_type );
	$post_loop = new wp_query($post_args);			
	if($post_display_style == "style_two"){
	?>
	<section id="y-blogs">
     <div class="y-blogs">
      <div class="container">
        <div class="row">  
          <div class="y-section_content_full clearfix">
            <h2 class="text-center">Our Blogs</h2> 
         <?php 
		$loopnum = 0;
	   
		  while($post_loop->have_posts()):$post_loop->the_post();
		  $post_id = get_the_id();
			  $post_title_value = get_the_title($post_id);
			  $post_content = get_the_content($post_id);
			  $permalink = get_the_permalink($post_id);
			  $date_value  = get_the_date('M j, Y', $post_id);
		  $loopnum  = $loopnum + 1 ; 
		   if($loopnum == 1 ){
			  
        $attachment_id = get_post_meta($post_id,"_cmb_featured_image_id",true);			   				
				 $src = wp_get_attachment_image_src($attachment_id,'yachtsailing-blog-listing-medium');
				 $post_image  = $src[0];	
		 ?>
            <div class="y-feature_post row clearfix">    
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                  <img src="<?php echo esc_url($post_image, 'yachtsailing');?>" alt="" class="img-responsive">                  
                </div>
                <div class="col-sm-6 pull-right y-blog_info">
                  <h3 class="y-heading">
                 <?php echo esc_html($post_title_value, 'yachtsailing');?>
                  </h3>
                  <?php if($listing_date_visibility == "show"){?>
                  <ul class="y-blog_general_info">
                      <li>
                        <span>
                          <i class="material-icons">access_time</i>
						    <?php echo esc_html($post_date, 'yachtsailing');?> 
							<?php echo esc_html($post_month, 'yachtsailing'); ?>, 
							<?php echo esc_html($post_year, 'yachtsailing'); ?>
                         </span>
                       </li>
                  </ul>
                  <?php } 
                  the_excerpt();
				  ?>
                <div class="clear"></div> 
                  <a href="<?php echo esc_url($permalink, 'yachtsailing');?>" class="y-button">Read More</a>  
                </div>
            </div>
            <?php } 
		 endwhile;
			?>
            
          <div class="row clearfix y-blog_listing">
            <?php 
		   $count = 0;
			 while($post_loop->have_posts()):$post_loop->the_post();
             $count++;
			  $post_id = get_the_id();						  
			  $post_title_value = get_the_title($post_id);
		      $permalink = get_the_permalink($post_id);  
			  $date_value  = get_the_date('M j, Y', $post_id);
	
		     $attachment_id =   get_post_meta($post_id,"_cmb_featured_image_id",true);				
			 $src = wp_get_attachment_image_src($attachment_id,'yachtsailing-blog-listing-thumb');
			 $post_image	 = $src[0];	
			 
			 if($count == 1 ){}else{
		   ?>
          
               <div class="col-sm-4 col-xs-12">
               <?php if($listing_info_visibility == "show"){ ?>
                  <div class="y-info_pop">
                      <a href="<?php echo esc_url($permalink, 'yachtsailing');?>">
					    <?php echo esc_html($post_title_value, 'yachtsailing');?>
                      </a>
                      
                  <?php if($listing_date_visibility == "show"){?>
                  <span>
                    <i class="material-icons">access_time</i>
				  <?php echo esc_html($date_value , 'yachtsailing'); ?>
                  </span>
                  <?php } ?>
                 </div>
               <?php }  ?>
                 <a href="<?php echo esc_url($permalink, 'yachtsailing');?>">
                  <img src="<?php echo esc_url($post_image, 'yachtsailing');?>" alt="" class="img-responsive">
                  </a>
                </div>
         
             <?php }
			
			 endwhile;
			?>
          </div>
               
          </div>
        </div>
      </div>
     </div> 
    </section>
    <?php } 
	else 
	{ ?>
    
  <div class="y-inner_page">
    <div class="clearfix">
                        <div class="col-lg-8 col-md-8 col-sm-7">
                         <div class="row">
                         
		  <?php 
	 
      while($post_loop->have_posts()):$post_loop->the_post();
	  $post_id = get_the_id();
			  $post_title_value = get_the_title($post_id);
			  $post_content = get_the_content($post_id);
			  $permalink = get_the_permalink($post_id);
			  $date_value  = get_the_date('M j, Y', $post_id);
              $attachment_id = get_post_meta($post_id,"_cmb_featured_image_id",true);			   			  
			 
			// $attachment_id = attachment_url_to_postid( $post_image );
			 $src = wp_get_attachment_image_src($attachment_id,'yachtsailing-blog-big');
			 $post_image	 = $src[0];				 
        ?>                         
                          <div class="y-single_blog">
                              <div class="y-home_blog_content">
                               <h3 class="y-heading"><?php echo esc_html($post_title_value, 'yachtsailing');?></h3>
                             <?php if($listing_date_visibility == "show"){?>
                                <ul class="y-blog_general_info">
                                    <li><span><i class="material-icons">access_time</i> <?php echo esc_html($date_value, 'yachtsailing'); ?></li>                                </ul> 
                                <?php } ?>
                                                                 
                              </div>  
                              <img class="img-responsive" alt="" src="<?php echo esc_url($post_image, 'yachtsailing');?>">
                              <div class="y-home_single_blog y-single_blog_text clearfix">
                                <div class="y-home_blog_content">
                                  <div class="y-home_blog_content_inner">
                                   <p>
                                     <?php 
									     $post_content = get_the_content($post_id);
										$post_content = substr($post_content,0,"400");
										$post_excerpt = get_the_excerpt($post_id);
										if($post_excerpt != "")
										{
											the_excerpt();
										}
										else
										{
											echo esc_html($post_content, 'yachtsailing');
										}
									 ?>
                                     
                                    </p>
                                    <a class="y-btn y-btn_bg" href="<?php echo esc_url($permalink, 'yachtsailing');?>">read more</a>
                                  </div>
                                </div>
                              </div>
                          </div> 
                          <?php endwhile; ?>
                                  <?php				
                $big = 999999999; // need an unlikely integer
                
  $translated = __( 'Page', 'yachtsailing' );
  echo paginate_links( array(
                    'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) , 'yachtsailing') ),
                    'format'  => '?paged=%#%',
                    'current' => $paged,
					          'show_all'=> true,
					          'type'    => 'plain',
                    'total'   => $post_loop->max_num_pages,
         'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',         
				 'prev_next'          => true,
				 'prev_text'          => __('Previous','yachtsailing'),
         'next_text'          => __('Next','yachtsailing')
  ) );
                ?>    
                                          
                         </div> 
                        </div>
                        <?php get_sidebar("blog"); ?>
                    </div>
                  </div>
    
    <?php 
	}?>
<?php echo $this->endBlockComment('bloglisting'); 
?>	