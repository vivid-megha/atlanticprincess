<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $values
 * @var $units
 * @var $bgcolor
 * @var $custombgcolor
 * @var $customtxtcolor
 * @var $options
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Progress_Bar
 */
$title = $values = $units = $bgcolor = $css = $custombgcolor = $customtxtcolor = $options = $el_class = '';
$output = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
$atts = $this->convertAttributesToNewProgressBar( $atts );
extract( $atts );
$font_size =$atts["font_size"];
$line_height =$atts["line_height"];
$padding_top =$atts["padding_top"];
$padding_bottom =$atts["padding_bottom"];
$padding_right =$atts["padding_right"];
$padding_left =$atts["padding_left"];
$letter_spacing =$atts["letter_spacing"];
$font_weight =$atts["font_weight"];
$font_color =$atts["font_color"];
$bar_background_color =$atts["bar_background_color"];
$styles = "";
$attributes = "";
$styles = array();
$attributes = array();
	if ( $font_size != '' ) {
		$styles[] = vc_get_css_color( 'font-size', $font_size."px" );
	}
	if ( $font_color != '' ) {
		$styles[] = vc_get_css_color( 'color', $font_color );
	}
	if ( $line_height != '' ) {
		$styles[] = vc_get_css_color( 'line-height', $line_height."px" );
	}
	if ( $letter_spacing != '' ) {
		$styles[] = vc_get_css_color( 'letter-spacing', $letter_spacing."px" );
	}
	if ( $font_weight != '' ) {
		$styles[] = vc_get_css_color( 'font-weight', $font_weight);
	}
	if ( $padding_bottom != '' ) {
		$styles[] = vc_get_css_color( 'padding-bottom', $padding_bottom."px" );
	}
	if ( $padding_top != '' ) {
		$styles[] = vc_get_css_color( 'padding-top', $padding_top."px" );
	}
	if ( $padding_right != '' ) {
		$styles[] = vc_get_css_color( 'padding-right', $padding_right."px" );
	}
	if ( $padding_left != '' ) {
		$styles[] = vc_get_css_color( 'padding-left', $padding_left."px" );
	}
	
if ( $styles != '' ) {
	$attributes[] = 'style="' . implode( ' ', $styles ) . '"';
}
$attributes = implode( ' ', $attributes );
$styles_bar = "";
$attributes_bar = "";
$styles_bar = array();
$attributes_bar = array();
	if ( $bar_background_color != '' ) {
		$styles_bar[] = vc_get_css_color( 'background', $bar_background_color );
	}
if ( $styles_bar != '' ) {
	$attributes_bar[] = 'style="' . implode( ' ', $styles_bar ) . '"';
}
	
$attributes_bar = implode( ' ', $attributes_bar );
wp_enqueue_script( 'waypoints' );
$el_class = $this->getExtraClass( $el_class );
$bar_options = array();
$options = explode( ',', $options );
if ( in_array( 'animated', $options ) ) {
	$bar_options[] = 'animated';
}
if ( in_array( 'striped', $options ) ) {
	$bar_options[] = 'striped';
}
if ( 'custom' === $bgcolor && '' !== $custombgcolor ) {
	$custombgcolor = ' style="' . vc_get_css_color( 'background-color', $custombgcolor ) . '"';
	if ( '' !== $customtxtcolor ) {
		$customtxtcolor = ' style="' . vc_get_css_color( 'color', $customtxtcolor ) . '"';
	}
	$bgcolor = '';
} else {
	$custombgcolor = '';
	$customtxtcolor = '';
	$bgcolor = 'vc_progress-bar-color-' . esc_attr( $bgcolor , 'yachtsailing');
	$el_class .= ' ' . $bgcolor;
}
$class_to_filter = 'vc_progress_bar wpb_content_element';
$class_to_filter .= vc_shortcode_custom_css_class( $css, ' ' ) . $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts );
$output = '<div class="' . esc_attr( $css_class , 'yachtsailing') . '">';
$output .= wpb_widget_title( array( 'title' => $title, 'extraclass' => 'wpb_progress_bar_heading' ) );
$values = (array) vc_param_group_parse_atts( $values );
$max_value = 0.0;
$graph_lines_data = array();
foreach ( $values as $data ) {
	$new_line = $data;
	$new_line['value'] = isset( $data['value'] ) ? $data['value'] : 0;
	$new_line['label'] = isset( $data['label'] ) ? $data['label'] : '';
	$new_line['bgcolor'] = isset( $data['color'] ) && 'custom' !== $data['color'] ? '' : $custombgcolor;
	$new_line['txtcolor'] = isset( $data['color'] ) && 'custom' !== $data['color'] ? '' : $customtxtcolor;
	if ( isset( $data['customcolor'] ) && ( ! isset( $data['color'] ) || 'custom' === $data['color'] ) ) {
		$new_line['bgcolor'] = ' style="background-color: ' . esc_attr( $data['customcolor'] , 'yachtsailing') . ';"';
	}
	if ( isset( $data['customtxtcolor'] ) && ( ! isset( $data['color'] ) || 'custom' === $data['color'] ) ) {
		$new_line['txtcolor'] = ' style="color: ' . esc_attr( $data['customtxtcolor'] , 'yachtsailing') . ';"';
	}
	if ( $max_value < (float) $new_line['value'] ) {
		$max_value = $new_line['value'];
	}
	$graph_lines_data[] = $new_line;
}
foreach ( $graph_lines_data as $line ) {
	$unit = ( '' !== $units ) ? ' <span class="vc_label_units">' . $line['value'] . $units . '</span>' : '';
	$output .= '<div class="vc_general vc_single_bar' . ( ( isset( $line['color'] ) && 'custom' !== $line['color'] ) ?
			' vc_progress-bar-color-' . $line['color'] : '' )
		. '">';
	$output .= '<small '.$attributes.' class="vc_label"' . $line['txtcolor'] . '>' . $line['label'] . $unit . '</small>';
	if ( $max_value > 100.00 ) {
		$percentage_value = (float) $line['value'] > 0 && $max_value > 100.00 ? round( (float) $line['value'] / $max_value * 100, 4 ) : 0;
	} else {
		$percentage_value = $line['value'];
	}
	$output .= '<span '.$attributes_bar.' class="vc_bar ' . esc_attr( implode( ' ', $bar_options ) , 'yachtsailing') . '" data-percentage-value="' . esc_attr( $percentage_value , 'yachtsailing') . '" data-value="' . esc_attr( $line['value'] , 'yachtsailing') . '"' . esc_attr($line['bgcolor'], 'yachtsailing') . '></span>';
	$output .= '</div>';
}
$output .= '</div>';
echo $output;
