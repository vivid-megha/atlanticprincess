<?php
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => '' ,
  'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );  
  $orderby = $atts["orderby"];
  $order = $atts["order"];
  $post_type = $atts["yachtsailing_charter_post_type"];
  $number_posts = $atts["number_posts"];
  $orderby = $atts["orderby"];
  $order = $atts["order"];
  $charter_styling = $atts["charter_styling"];
  
  if($number_posts == "" )
  {
    $number_posts = "-1";
  }
  
if($charter_styling == "default_style") 
{
?>
<div class="y-product_listing y-product_listing_side clearfix">
<?php 
  $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
  
  $charter_args = array("post_type" => $post_type, "posts_per_page" => $number_posts, "offset" => $paged, "order" => $order, "orderby" => $orderby );
  $charter_loop =  new wp_query($charter_args);
    while($charter_loop->have_posts()):$charter_loop->the_post();
    $post_id = get_the_id();
    $charter_title = get_the_title($post_id);
          $charter_type = get_post_meta($post_id,"_cmb_charter_type",true);
          $charter_berths = get_post_meta($post_id,"_cmb_charter_berths",true);
          $price_from = get_post_meta($post_id,"_cmb_price_from",true);
          $charter_image = get_post_meta($post_id,"_cmb_charter_image",true);
      if(empty($charter_image))
      {
        $charter_image = get_template_directory_uri()."/images/product_1.jpg";
      }
      
  ?>
                          
             <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
               <div class="y-yacht_intro_img">
                 <a href="<?php echo esc_url(the_permalink(), 'yachtsailing'); ?>">
                  <img src="<?php echo esc_url($charter_image, 'yachtsailing'); ?>" class="img-responsive" alt=""></a>
               </div>
                 
               <div class="y-yacht_intro">
                 <a href="<?php echo esc_url(the_permalink(), 'yachtsailing'); ?>">
				    <?php echo esc_html($charter_title, 'yachtsailing'); ?>
                  </a>
                   
                 <ul>
                    <li><i class="material-icons">room</i><?php echo esc_html($charter_type, 'yachtsailing'); ?></li>
                    <li><img src="<?php echo esc_url(get_template_directory_uri()."/images/bed.png", 'yachtsailing');?>" alt=""> Berths: <?php echo esc_html($charter_berths, 'yachtsailing');?> </li>
                 </ul>                    
                 <span>Price from <strong>$<?php echo esc_html($price_from, 'yachtsailing');?></strong></span>
                 <a class="y-button" href="<?php echo esc_url(get_permalink($post_id), 'yachtsailing'); ?>">Book Now</a>
               </div> 
             </div>
                                  
  <?php  
    endwhile;
  ?>
<?php if(!is_front_page()){ ?>                        
                <?php       
                $big = 999999999; // need an unlikely integer
                
  $translated = __( 'Page', 'yachtsailing' );
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) , 'yachtsailing') ),
                    'format' => '?paged=%#%',
                    'current' => $paged,
                    'show_all'  => true,
                    'type'  => 'plain',
                    'total' => $charter_loop->max_num_pages,
                    'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',
            'prev_next'          => true,
          'prev_text'          => __('Previous', 'yachtsailing'),
                    'next_text'          => __('Next', 'yachtsailing')
                ) );
                ?>    
<?php } ?>                        
                     
</div>
<?php } 
else { ?>
<div class="y-boat_carousel col-lg-12 clearfix" id="y-boat_carousel">
<?php $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
    $charter_args = array("post_type" => $post_type, "posts_per_page" => $number_posts, "offset" => $paged, "order" => $order, "orderby" => $orderby );
     $charter_loop =  new wp_query($charter_args);
    while($charter_loop->have_posts()):$charter_loop->the_post();
    
      $post_id = get_the_id();
    $charter_title = get_the_title($post_id);
          $charter_type = get_post_meta($post_id,"_cmb_charter_type",true);
          $charter_berths = get_post_meta($post_id,"_cmb_charter_berths",true);
          $price_from = get_post_meta($post_id,"_cmb_price_from",true);
          $charter_image = get_post_meta($post_id,"_cmb_charter_image",true);
      if(empty($charter_image))
      {
        $charter_image = get_template_directory_uri()."/images/product_1.jpg";
      }
?>
                      <div class="item">
                        <img src="<?php echo esc_url($charter_image, 'yachtsailing'); ?>" alt="" class="img-responsive">
                        <div class="y-boat_info clearfix">
                          <div class="col-sm-12 col-xs-12">
                            <p><a href="<?php echo esc_url(the_permalink($post_id), 'yachtsailing'); ?>"><?php echo esc_html($charter_title, 'yachtsailing'); ?></a></p>
                            <i>Mati martin</i>
                          </div>  
                        </div> 
                      </div>
      <?php endwhile; ?>  
<?php if(!is_front_page()){ ?>                        
          <?php       
                $big = 999999999;
                
  $translated = __( 'Page', 'yachtsailing' );
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) , 'yachtsailing') ),
                    'format' => '?paged=%#%',
                    'current' => $paged,
                    'show_all'  => true,
                    'type' => 'plain',
                    'total' => $charter_loop->max_num_pages,
                    'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',
                    'prev_next'       => true,
                    'prev_text' => __('Previous','yachtsailing'),
                    'next_text'  => __('Next','yachtsailing')
                ) );
                ?>    
<?php } ?>                        
</div>
<?php } ?>
<?php echo $this->endBlockComment('charter'); ?>