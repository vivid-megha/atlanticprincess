<?php
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''	,
	'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
	$post_type = $atts["testimonials_post_type"];
	$number_posts = $atts["number_posts"];
	$orderby = $atts["orderby"];
	$order = $atts["order"];
	$content_styling = $atts["content_styling"];
	$rating_visibility = $atts["rating_visibility"];
    	
	
/* ctitle variables starts here */
    $orderby = $atts["orderby"];
	$order = $atts["order"];
	$testimonials = wp_get_recent_posts( array( 'numberposts' => $number_posts, 'orderby' => $orderby, 'order' => $order, 'post_type' => $post_type ) );
	
	if($number_posts == 1)
	{
           $testimonial_id = "y-client_testimonial";
	}
	else
	{
	       $testimonial_id = "y-client_testimonial_carousel";
	}
?>
<?php if($content_styling == "slider"){?>
<section id="y-client_says_section">  
      <div class="y-client_says_section">
        <div id="y-client_says_text" class="container">
              <div class="row"> 
                    <h2 class="text-center">WHAT CLIENTS SAY</h2>  
                    <div id="<?php echo $testimonial_id; ?>">
                    <?php 
					  foreach( $testimonials as $testimonial ) { 
					  $desi = get_post_meta($testimonial['ID'],"_cmb_testimonials_designation",true); 
					  $rating = get_post_meta($testimonial['ID'],"_cmb_rating_number",true);
					?>
                      <div class="y-client_testimonials">
                            <p class="y-client_testimonial_text">
                                  <?php echo $testimonial['post_content']; ?>
                            </p>
                            <div class="y-client_testimonial_user">
                                  <span><?php echo $testimonial['post_title']; ?> <em>(<?php echo $desi; ?>)</em></span>
                                 
                                  
                                 <?php 
								  if($rating_visibility == "yes")
								  {
								  if($rating != "")
								  {
									  echo ' <div class="y-client_testimonial_rating">';
									  $zero = 1; 
									  for($zero; $zero <= $rating; $zero++) 
									  {?>
										<i class="fa fa-star"></i>
								<?php
							     	  }
									  echo "</div>";
							    }
							} ?>
                            </div>
                      </div>
                      <?php } ?>
                      
                    </div> 
              </div>
        </div>
      </div>    
    </section>
    
<?php 
    } else if($content_styling == "listing")
	{
?>
<div class="responsive-tabs">
<div class="y-tab_review">
<?php 
  foreach( $testimonials as $testimonial ) { 
  $desi = get_post_meta($testimonial['ID'],"_cmb_testimonials_designation",true); 
  $post_image = get_post_meta($testimonial["ID"],"_cmb_featured_image",true);	
  $rating = get_post_meta($testimonial['ID'],"_cmb_rating_number",true);
 ?>
                      <div class="y-client_testimonials">
                            <img alt="" src="<?php echo esc_url($post_image, 'yachtsailing');?>">
                            <p class="y-client_testimonial_text">
                                <?php echo $testimonial['post_content']; ?>
                            </p> 
                            <div class="y-client_testimonial_user">
                                  <span><?php echo $testimonial['post_title']; ?> 
                                  <?php $pfx_date = get_the_date( "d M Y", $testimonial['ID'] ); ?>
                                  <em> (<?php echo $pfx_date;?>)</em></span>
                            </div>
                                 <?php 
								  if($rating_visibility == "yes")
								  {
								  if($rating != "")
								  {
									  echo ' <div class="y-client_testimonial_rating">';
									  $zero = 1; 
									  for($zero; $zero <= $rating; $zero++) 
									  {?>
										<i class="fa fa-star"></i>
								<?php
							     	  }
									  echo "</div>";
							    }
							} ?>
                      </div>
                      <?php } ?>
                    </div>
                    </div>
                    
<?php 
	}
?>
<?php echo $this->endBlockComment('testimonialowlCarousel'); ?>