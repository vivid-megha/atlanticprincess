<?php
$counter_title = $counter_text = $counter_subtext ="";
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''	,
	'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* ctitle variables starts here */
    $title = $atts["title"];
    $count = $atts["count"];
    $orderby = $atts["orderby"];
	$order = $atts["order"];
    $el_class	= $atts["el_class"];
	$css_editor = $atts["css_editor"];
	
?>
<div class="services__info <?php echo $el_class; ?>">
	<?php 
		$services = wp_get_recent_posts( array( 'numberposts' => $count, 'category' => 0, 'orderby' => $orderby, 'order' => $order, 'post_type' => 'yachtsailing_services','suppress_filters' => false ) );
		foreach( $services as $service ) {
    ?>
			<div class="services__info-block">
				<h5 class="clearfix services__title"><a class="pull-left no-decoration js-toggle" href="javascript:void(0)"><?php echo $service['post_title']; ?></a>
                <a class="square square--toggle pull-right js-toggle"><span class="fa fa-plus"></span></a></h5>
                <div class="services__text triangle triangle--services"><?php echo $service['post_content']; ?></div>
			</div>
		<?php } ?>
</div>
<?php echo $this->endBlockComment('services'); ?>	