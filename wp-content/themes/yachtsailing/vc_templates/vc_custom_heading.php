<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $source
 * @var $text
 * @var $link
 * @var $google_fonts
 * @var $font_container
 * @var $el_class
 * @var $css
 * @var $font_container_data - returned from $this->getAttributes
 * @var $google_fonts_data - returned from $this->getAttributes
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Custom_heading
 */
$source = $text = $link = $google_fonts = $font_container = $el_class = $css = $font_container_data = $google_fonts_data = '';
// This is needed to extract $font_container_data and $google_fonts_data
extract( $this->getAttributes( $atts ) );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
/* Heading icon variables starts here */
$material_icon_shortcode = $atts["material_icon_shortcode"];
$icon_position = $atts["icon_position"];
$iconpicker= $atts["iconpicker"];
if($material_icon_shortcode != '')
{
	$subtext = '<i class="material-icons">'.$material_icon_shortcode.'</i> ';	
}
else
{
	$subtext = '<i class="'.$iconpicker.'"></i> ';		
}
if($icon_position == "after_heading")
{
    $after_text = $subtext;	
    $before_text = "";	
}
else
{
    $before_text = $subtext;	
    $after_text = "";	
}
/* Heading icon variables starts here */
$letter_spacing = $atts["letter_spacing"];
$text_transform = $atts["text_transform"];
$font_weight= $atts["font_weight"];
extract( $this->getStyles( $el_class, $css, $google_fonts_data, $font_container_data, $atts ) );
$settings = get_option( 'wpb_js_google_fonts_subsets' );
if ( is_array( $settings ) && ! empty( $settings ) ) {
	$subsets = '&subset=' . implode( ',', $settings );
} else {
	$subsets = '';
}
if ( isset( $google_fonts_data['values']['font_family'] ) ) {
	wp_enqueue_style( 'vc_google_fonts_' . vc_build_safe_css_class( $google_fonts_data['values']['font_family'] ), '//fonts.googleapis.com/css?family=' . $google_fonts_data['values']['font_family'] . $subsets );
}
if ( ! empty( $styles ) ) {
	$style = 'style="' . esc_attr( implode( ';', $styles ) , 'yachtsailing') . ' ;letter-spacing:'.$letter_spacing.'px; text-transform:'.$text_transform.';font-weight:'.$font_weight.';"';
} else {
	$style = '';
}
if ( 'post_title' === $source ) {
	$text = get_the_title( get_the_ID() );
}
if ( ! empty( $link ) ) {
	$link = vc_build_link( $link );
	$text = '<a href="' . esc_attr( $link['url'] , 'yachtsailing') . '"'
		. ( $link['target'] ? ' target="' . esc_attr( $link['target'] , 'yachtsailing') . '"' : '' )
		. ( $link['title'] ? ' title="' . esc_attr( $link['title'] , 'yachtsailing') . '"' : '' )
		. '>' . $text . '</a>';
}
$output = '';
if ( apply_filters( 'vc_custom_heading_template_use_wrapper', false ) ) {
	$output .= '<div class="' . esc_attr( $css_class , 'yachtsailing') . '" >';
	$output .= '<' . $font_container_data['values']['tag'] . ' ' . $style . ' >';
	$output .= $text;
	$output .= '</' . $font_container_data['values']['tag'] . '>';
	$output .= '</div>';
} else {
	$output .= '<' . $font_container_data['values']['tag'] . ' ' . $style . ' class="' . esc_attr( $css_class , 'yachtsailing') . '">';
	$output .= $before_text;	
	$output .= $text;
	$output .= $after_text;
	$output .= '</' . $font_container_data['values']['tag'] . '>';
}
echo $output;
