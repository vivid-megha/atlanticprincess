<?php 
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* ctitle variables starts here */
    $title = $atts["title"]; 
    $short_content = $atts["short_content"];
	if(isset($title) && $title != ''){
?>
			<header class="tab-header clearfix">
			<h2 class="title title--main pull-left"><?php echo $title; ?><span class="line line--title"><span class="line__first"></span>
            <span class="line__second"></span></span></h2>
			<div class="tab-toggles pull-right js-isotope-btns">
            <a href="#" class="button button--grey button--main btn js-isotope-btn" data-sort-by="name">BY TYPE</a>
            <a href="#" class="button button--grey button--main btn button--active js-isotope-btn" data-sort-by="original-order">BY NEWEST</a>
            </div>
            </header>
            <?php } if(isset($short_content) && $short_content != '') { ?>
            <p class="text text--anons"><?php echo $short_content;?></p>
            <?php } ?>
            <div class="row isotope">
            <?php $products = wp_get_recent_posts( array( 'numberposts' => '4', 'category' => 0, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => 'product','suppress_filters' => false ) );
			foreach( $products as $product ) {
			 ?>
			<div class="col-md-3 col-xs-6 isotope-item">
        	<a href="<?php echo $product['guid']; ?>" class="listing-anons equal-height-item listing-anons--home triangle triangle--big line-down no-decoration">
			<?php if($image !=''){ ?>
			<img src="<?php echo $image[0]; ?>" class="img-responsive" alt="" />
            <?php } ?>
			<div class="listing-anons__title">
            <h4 class="name"><?php echo $product['post_title']; ?></h4>
            </div>
            <div class="listing-anons__hidden">
            <h3> <?php echo $product['post_title']; ?></h3>
            <p> <?php echo $product['post_excerpt']; ?></p>
            </div>
            </a></div>
			<?php } ?>
</div>
<?php echo $this->endBlockComment('productlisting'); ?>	