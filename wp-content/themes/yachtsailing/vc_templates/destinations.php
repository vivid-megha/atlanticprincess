<?php
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''	,
	'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
    $orderby = $atts["orderby"];
	$order = $atts["order"];
	$post_type = $atts["destinations_post_type"];
	$number_posts = $atts["number_posts"];
	$orderby = $atts["orderby"];
	$order = $atts["order"];
	$content_position = $atts["content_position"];
	
	if($number_posts == "" )
	{
		$number_posts = "-1";
	}
?>
 <div class="y-dest_list">
     <div class="row">
<?php	
     wp_reset_query();
     wp_reset_postdata();
    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;	
	$destination_args = array("post_type" => $post_type, "posts_per_page" => $number_posts, "offset" => $paged, "order" => $order, "orderby" => $orderby );
	$destination_loop =  new wp_query($destination_args);
	$count = 0;
	  while($destination_loop->have_posts()):$destination_loop->the_post();
			   $post_id = get_the_id();
		  $count++;	     
   	      $destination_short_content = get_post_meta($post_id,"_cmb_destination_short_content",true);
   	        $destination_image = get_post_meta($post_id,"_cmb_destination_image",true);
     
		    $attachment_id = attachment_url_to_postid($destination_image);
			 $src = wp_get_attachment_image_src($attachment_id,'yachtsailing-destination-thumb');
			 $destination_image = $src[0];	
		  if($destination_image == "")
		  {
			  $destination_image = get_template_directory_uri()."/images/destinations_4.jpg";
		  }
		  $image_pull_right = "";
		  $content_pull_right = "";		  
		
		  if($content_position == "images_left")
		  {
		     $image_pull_right = "";			  
		     $content_pull_right = "pull-right";		  			 
		  }
		  else if($content_position == "images_right")
		  {
		     $image_pull_right = "pull-right";			  
		     $content_pull_right = "";		  			 
		  }
		  else
		  {
			  if($count % 2 != 0)
			  {
				  $image_pull_right = "pull-right";
				  $content_pull_right = "";
			  }
			  else
			  {
				  $content_pull_right = "pull-right";
				  $image_pull_right = "";
			  }
		  }
		  
		  ?>
                          <div class="y-dest_list_single clearfix">
                            <div data-wow-duration="1s" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 <?php echo sanitize_html_class($image_pull_right); ?>">
                   <a href="<?php echo esc_url(get_permalink($post_id), 'yachtsailing'); ?>">
                   <img alt="" class="img-responsive" src="<?php echo esc_url($destination_image, 'yachtsailing');?>"></a>
                            </div>
                            <div data-wow-duration="1s" class="col-sm-8 <?php echo sanitize_html_class($content_pull_right); ?> wow fadeInRight">
                                <h3 class="y-heading"><a href="<?php echo get_permalink($post_id); ?>"><?php echo esc_html(get_the_title($post_id), 'yachtsailing'); ?></a></h3> 
                                   <?php echo $destination_short_content;?>
                                <a class="y-button" href="<?php echo get_permalink($post_id); ?>">Read More</a>
                            </div> 
                          </div>
<?php  endwhile;  ?>  
         </div>
        <?php				
                $big = 999999999; // need an unlikely integer
                $translated = __( 'Page', 'yachtsailing' );   
                echo paginate_links( array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) , 'yachtsailing') ),
                    'format' => '?paged=%#%',
                    'current' => $paged,
					'show_all'  => true,
					'type'               => 'plain',
                    'total' => $destination_loop->max_num_pages,
'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',
				    'prev_next'          => true,
					'prev_text'          => __('Previous','yachtsailing'),
                   	'next_text'          => __('Next','yachtsailing')
                ) );
                ?>    
                
                        </div>
<?php echo $this->endBlockComment('destinations'); ?>