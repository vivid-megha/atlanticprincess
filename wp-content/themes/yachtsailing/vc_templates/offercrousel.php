<?php 
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''	,
	'icon_counter' => '' 
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* ctitle variables starts here */
    $title = $atts["title"]; 
    $count = $atts["count"];
    $orderby = $atts["orderby"];
	$order = $atts["order"];
    $el_class	= $atts["el_class"];
	
?>
<h2 class="title title--main"><span class="title__bold"><?php echo $title ?></span><span class="line line--title line--title--main"><span class="line__first"></span><span class="line__second"></span></span></h2>
<div id="carousel-small" class="owl-carousel enable-owl-carousel <?php echo $el_class;?>" data-auto-play="5000" data-stop-on-hover="true" data-items="2" data-pagination="true" data-navigation="false" data-items-desktop="2" data-items-desktop-small="2" data-items-tablet="1" data-items-tablet-small="1" >
	<?php 
	$offers = wp_get_recent_posts( array( 'numberposts' => $count, 'category' => 0, 'orderby' => $orderby, 'order' => $order, 'post_type' => 'yachtsailing_offer','suppress_filters' => false ) );
			foreach( $offers as $offer ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id($offer['ID']),'yachtsailing-offer');
			$url = get_post_meta($offer['ID'],"_cmb_product_link",true);
			$offer_text = get_post_meta($offer['ID'],"_cmb_product_link_text",true); ?>
			<div class="item">
		<div class="offer">
        <div class="row row--no-padding">
        <div class="col-xs-6"><div class="offer__img">
		<?php if($image !=''){ ?>
		<img src="<?php echo $image[0]; ?>" class="img-responsive" alt="" />
		<?php } ?>
</div></div><div class="col-xs-6"><div class="offer__info">
<h4 class="title"><?php echo $offer['post_title']; ?></h4>
<?php echo $offer['post_content']; ?>
<a href="<?php echo $url; ?>" class="btn button button--main button--red pull-right">get offer</a>
</div></div>
</div>
</div>
</div>
		<?php } ?>
</div>
<?php echo $this->endBlockComment('offercrousel'); ?>	