<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $content - shortcode content
 * @var $this WPBakeryShortCode_VC_Tta_Accordion|WPBakeryShortCode_VC_Tta_Tabs|WPBakeryShortCode_VC_Tta_Tour|WPBakeryShortCode_VC_Tta_Pageable
 */
$el_class = $css = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
$this->resetVariables( $atts, $content );
extract( $atts );
/* List of Custom Variables ends here */
	$type = $atts["type"];
	$tab_background_color = $atts["tab_background_color"];
	$tab_border_color = $atts["tab_border_color"];
	$tab_border_size = $atts["tab_border_size"];
	$tab_border_style = $atts["tab_border_style"];
	$active_tab_background_color = $atts["active_tab_background_color"];
	$tab_font_color = $atts["tab_font_color"];
	$active_tab_font_color = $atts["active_tab_font_color"];
	
	$tabs_padding_top = $atts["tabs_padding_top"];
    $tabs_padding_bottom = $atts["tabs_padding_bottom"];
	$tabs_padding_left = $atts["tabs_padding_left"];
	$tabs_padding_right = $atts["tabs_padding_right"];
	$tabs_margin_top = $atts["tabs_margin_top"];
	$tabs_margin_bottom = $atts["tabs_margin_bottom"];
	$tabs_margin_left = $atts["tabs_margin_left"];
	$tabs_margin_right = $atts["tabs_margin_right"];
	$text_transform = $atts["text_transform"];
	$font_weight = $atts["font_weight"];
	$tab_letter_spacing = $atts["tab_letter_spacing"];
	$tab_line_height = $atts["tab_line_height"];
	
/* List of custom variables ends here */
/* Icon regarding variables starts here */
	$icon_position = $atts["icon_position"];
	$icon_padding_top = $atts["icon_padding_top"];
	$icon_padding_bottom = $atts["icon_padding_bottom"];
	$icon_padding_left = $atts["icon_padding_left"];
	$icon_padding_right = $atts["icon_padding_right"];
	$icon_size = $atts["icon_size"];
/* Icon regarding variables starts here */
$this->setGlobalTtaInfo();
$this->enqueueTtaStyles();
$this->enqueueTtaScript();
// It is required to be before tabs-list-top/left/bottom/right for tabs/tours
$prepareContent = $this->getTemplateVariable( 'content' );
$class_to_filter = $this->getTtaGeneralClasses();
$class_to_filter .= vc_shortcode_custom_css_class( $css, ' ' ) . $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts );
$output = '<div ' . $this->getWrapperAttributes() . '>';
$output .= $this->getTemplateVariable( 'title' );
$output .= '<div class="' . esc_attr( $css_class , 'yachtsailing') . '">';
$output .= $this->getTemplateVariable( 'tabs-list-top' );
$output .= $this->getTemplateVariable( 'tabs-list-left' );
$output .= '<div class="vc_tta-panels-container">';
$output .= $this->getTemplateVariable( 'pagination-top' );
$output .= '<div class="vc_tta-panels">';
$output .= $prepareContent;
$output .= '</div>';
$output .= $this->getTemplateVariable( 'pagination-bottom' );
$output .= '</div>';
$output .= $this->getTemplateVariable( 'tabs-list-bottom' );
$output .= $this->getTemplateVariable( 'tabs-list-right' );
$output .= '</div>';
$output .= '</div>';
if($type == "container"){ echo '<div class="container">'; echo $output; echo "</div>"; }else { echo $output; }
?>
<style type="text/css">
.vc_tta-tabs-list{ 
  margin-top:<?php echo $tabs_margin_top."px" ?> !important; 
  margin-bottom:<?php echo $tabs_margin_bottom."px" ?> !important; 
  margin-left:<?php echo $tabs_margin_left."px" ?> !important; 
  margin-right:<?php echo $tabs_margin_right."px" ?> !important;
}
.vc_tta-tabs-list .vc_tta-tab > a{
	background:<?php echo $tab_background_color; ?> !important;
	 color:<?php echo $tab_font_color;?> !important;
	 padding-top:<?php echo $tabs_padding_top."px";?> !important;
	 padding-bottom:<?php echo $tabs_padding_bottom."px";?> !important;
	 padding-left:<?php echo $tabs_padding_left."px";?> !important;
	 padding-right:<?php echo $tabs_padding_right."px";?> !important;
}  
.vc_tta-tabs-list .vc_tta-tab{ 
	border:<?php echo $tab_border_size."px "; 
	echo $tab_border_color." "; 
	echo $tab_border_style; ?>;
	box-sizing:border-box;
}
.vc_tta-tabs-list .vc_tta-tab.vc_active > a{ 
     background:<?php echo $active_tab_background_color; ?> !important; 
	 padding-top:<?php echo $tabs_padding_top."px";?> ;
	 padding-bottom:<?php echo $tabs_padding_bottom."px";?> ;
	 padding-left:<?php echo $tabs_padding_left."px";?>;
	 padding-right:<?php echo $tabs_padding_right."px";?> ;
	 color:<?php echo $active_tab_font_color;?> !important ;
}  
.vc_tta-title-text {
  text-transform: <?php echo $text_transform; ?>;
  font-weight: <?php echo $font_weight; ?>;
  letter-spacing:<?php echo $tab_letter_spacing."px";?>;
  line-height:<?php echo $tab_line_height."px";?>
}
</style>
<?php 
if($icon_position == "top")
{
?>
<style type="text/css">
.vc_tta.vc_general .vc_tta-icon {
  display: block !important;
  padding-top: <?php echo $icon_padding_top."px";?>;
  padding-bottom: <?php echo $icon_padding_bottom."px";?>;
  padding-left: <?php echo $icon_padding_left."px";?>;
  padding-right: <?php echo $icon_padding_right."px";?>;
  font-size: <?php echo $icon_size."px"; ?> !important;
}
</style>
<?php 
}
?>