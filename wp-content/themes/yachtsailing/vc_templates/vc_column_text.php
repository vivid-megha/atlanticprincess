<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $css_animation
 * @var $css
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Column_text
 */
$el_class = $css = $css_animation = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$class_to_filter = 'wpb_text_column wpb_content_element ' . $this->getCSSAnimation( $css_animation );
$class_to_filter .= vc_shortcode_custom_css_class( $css, ' ' ) . $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts );
$font_color = $atts["font_color"];
$font_size = $atts["font_size"];
$letter_spacing = $atts["letter_spacing"];
$line_height = $atts["line_height"];
$text_align = $atts["text_align"];
$output = '
	<div class="'.$css_class.'">
		<div class="wpb_wrapper" style="color:'.$font_color.'; font-size:'.$font_size.'px;letter-spacing:'.$letter_spacing.'px;text-align:'.$text_align.';line-height:'.$line_height.'px;">
			' . wpb_js_remove_wpautop( $content, true ) . '
		</div>
	</div>
';
echo $output;
