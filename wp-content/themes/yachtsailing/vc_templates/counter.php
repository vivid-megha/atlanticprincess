<?php
$counter_title = $counter_text = $counter_subtext ="";
$css = '';
extract(shortcode_atts(array(
    'css' => '',
    'counter_title' => '',
    'counter_text' => '',
    'counter_subtext' => ''	,
	'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$material_icon_code =  $atts["material_icon_code"];
$icon_font_size =  $atts["icon_font_size"];
$cicon_styles = "";
$cicon_attributes = "";
$cicon_styles = array();
$cicon_attributes = array();
if ( $icon_font_size ) {
	$cicon_styles[] = vc_get_css_color( 'font-size', $icon_font_size."px" );
}
	
if ( $cicon_styles ) {
	$cicon_attributes[] = 'style="' . implode( ' ', $cicon_styles ) . '"';
}
$cicon_attributes = implode( ' ', $cicon_attributes );
$iconpicker_code =  $atts["iconpicker_code"];
$counter_number =  $atts["counter_number"];
/* Counter title variables starts here */
$counter_title =  $atts["counter_title"];
	$ctitle_font_color = $atts["ctitle_font_color"];
	$ctitle_font_size = $atts["ctitle_font_size"];
	$ctitle_line_height = $atts["ctitle_line_height"];
	$ctitle_letter_spacing = $atts["ctitle_letter_spacing"];
	$ctitle_font_weight = $atts["ctitle_font_weight"];
	$ctitle_text_transform = $atts["ctitle_text_transform"];
/* Counter title variables ends here */
/* Counter number variables starts here */
	$counter_after_text = $atts["counter_after_text"];
	$ctext_font_color = $atts["ctext_font_color"];
	$ctext_font_size = $atts["ctext_font_size"];
	$ctext_line_height = $atts["ctext_line_height"];
	$ctext_letter_spacing = $atts["ctext_letter_spacing"];
	$ctext_font_weight = $atts["ctext_font_weight"];
/* Counter number  variables ends here */
/* Counter number Text variables starts here */
	$cntext_font_color = $atts["counter_after_text"];
	$cntext_font_color = $atts["cntext_font_color"];
	$cntext_font_size = $atts["cntext_font_size"];
	$cntext_line_height = $atts["cntext_line_height"];
	$cntext_letter_spacing = $atts["cntext_letter_spacing"];
	$cntext_font_weight = $atts["cntext_font_weight"];
/* Counter number text variables ends here */
/* Counter title Styles starts here */
	$cntext_styles = "";
	$cntext_attributes = "";
	
	$cntext_styles = array();
	$cntext_attributes = array();
	
	if ( $cntext_font_color ) {
		$cntext_styles[] = vc_get_css_color( 'color', $cntext_font_color );
	}
	
	if ( $cntext_font_size ) {
		$cntext_styles[] = vc_get_css_color( 'font-size', $cntext_font_size."px" );
	}
	
	if ( $cntext_line_height ) {
		$cntext_styles[] = vc_get_css_color( 'line-height', $cntext_line_height."px" );
	}
	
	if ( $cntext_letter_spacing ) {
		$cntext_styles[] = vc_get_css_color( 'letter-spacing', $cntext_letter_spacing."px" );
	}
	
	if ( $cntext_font_weight ) {
		$cntext_styles[] = vc_get_css_color( 'font-weight', $cntext_font_weight );
	}
	
	if ( $cntext_text_transform ) {
		$cntext_styles[] = vc_get_css_color( 'text-transform', $cntext_text_transform );
	}
	
	if ( $cntext_styles ) {
		$cntext_attributes[] = 'style="' . implode( ' ', $cntext_styles ) . '"';
	}
	
	$cntext_attributes = implode( ' ', $cntext_attributes );
/* Counter title Styles ends here */
/* Counter title Styles starts here */
	$ctitle_styles = "";
	$ctitle_attributes = "";
	
	$ctitle_styles = array();
	$ctitle_attributes = array();
	
	if ( $ctitle_font_color ) {
		$ctitle_styles[] = vc_get_css_color( 'color', $ctitle_font_color );
	}
	
	if ( $ctitle_font_size ) {
		$ctitle_styles[] = vc_get_css_color( 'font-size', $ctitle_font_size."px" );
	}
	
	if ( $ctitle_line_height ) {
		$ctitle_styles[] = vc_get_css_color( 'line-height', $ctitle_line_height."px" );
	}
	
	if ( $ctitle_letter_spacing ) {
		$ctitle_styles[] = vc_get_css_color( 'letter-spacing', $ctitle_letter_spacing."px" );
	}
	
	if ( $ctitle_font_weight ) {
		$ctitle_styles[] = vc_get_css_color( 'font-weight', $ctitle_font_weight );
	}
	
	if ( $ctitle_text_transform ) {
		$ctitle_styles[] = vc_get_css_color( 'text-transform', $ctitle_text_transform );
	}
	
	if ( $ctitle_styles ) {
		$ctitle_attributes[] = 'style="' . implode( ' ', $ctitle_styles ) . '"';
	}
	
	$ctitle_attributes = implode( ' ', $ctitle_attributes );
/* Counter title Styles ends here */
/* Counter number starts here */
	$ctext_styles = "";
	$ctext_attributes = "";
	
	$ctext_styles = array();
	$ctext_attributes = array();
	
	if ( $ctext_font_color ) {
		$ctext_styles[] = vc_get_css_color( 'color', $ctext_font_color );
	}
	
	if ( $ctext_font_size ) {
		$ctext_styles[] = vc_get_css_color( 'font-size', $ctext_font_size."px" );
	}
	
	if ( $ctext_line_height ) {
		$ctext_styles[] = vc_get_css_color( 'line-height', $ctext_line_height."px" );
	}
	
	if ( $ctext_letter_spacing ) {
		$ctext_styles[] = vc_get_css_color( 'letter-spacing', $ctext_letter_spacing."px" );
	}
	
	if ( $ctext_font_weight ) {
		$ctext_styles[] = vc_get_css_color( 'font-weight', $ctext_font_weight );
	}
	
	if ( $ctext_styles ) {
		$ctext_attributes[] = 'style="' . implode( ' ', $ctext_styles ) . '"';
	     }
	$ctext_attributes = implode( ' ', $ctext_attributes );
/* Counter Number ends here */
?> 
   <div class="y-fact_single wow fadeIn" data-wow-duration=".5s">
     <div>
     <?php 
		if($material_icon_code != '')
		{
     ?>
     <i class="material-icons" <?php echo esc_html($cicon_attributes, 'yachtsailing');?>>
	    <?php echo esc_html($material_icon_code, 'yachtsailing');?>
     </i>
     <?php 
		}	 
		else if($iconpicker_code != "")
		{
     ?>
     <i class="<?php echo esc_html($iconpicker_code, 'yachtsailing'); ?>" <?php echo esc_html($cicon_attributes, 'yachtsailing');?>>
     </i>
     <?php 
		}
		else
		{?>
     <i class="material-icons" <?php echo esc_html($cicon_attributes, 'yachtsailing');?>>
      insert_emoticon
     </i>        
        <?php			
		}
	 ?>
        
        <?php 
		if($counter_number != '')
		{
		?>
        <span class="y-fact_count y-counter"  <?php echo esc_html($ctext_attributes, 'yachtsailing');?>>
		   <?php echo esc_html($counter_number, 'yachtsailing');?>
        </span>
        
        <?php if($counter_after_text != ""){?>
        <span class="y-fact_count"  <?php echo esc_html($cntext_attributes, 'yachtsailing');?>>
		   <?php echo esc_html($counter_after_text, 'yachtsailing');?>
        </span>
        
        <?php } ?>
        <?php 
		}
		else
		{
	    ?>
      <span class="y-fact_count y-counter"  <?php echo esc_html($ctext_attributes, 'yachtsailing');?>>
        285K
      </span>
        <?php 
		}
	    ?>
        <?php if($counter_title != '')
		{
		?>
        <span class="y-fact_title" <?php echo esc_html($ctitle_attributes, 'yachtsailing'); ?>>
		   <?php echo esc_html($counter_title, 'yachtsailing');?>
         </span>
        <?php } 
		else
		{
	    ?>
        <span class="y-fact_title <?php echo esc_html($ctitle_attributes, 'yachtsailing'); ?>">
           happy clients
         </span>        	
   <?php }?>
    </div>
   </div>
    
<?php echo $this->endBlockComment('counter'); ?>	