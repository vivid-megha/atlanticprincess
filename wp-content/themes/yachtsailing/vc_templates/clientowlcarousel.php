<?php
$css = '';
extract(shortcode_atts(array(
    'css' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
$post_type = "law_clients";
?>
<div class="container">
<div class="row">
  <?php
     $post_args = array("post_type" => $post_type, "posts_per_page" => -1,'suppress_filters' => false);
	 $post_loop = new wp_query($post_args);
  ?>
	 <div class="client-carousel">  
		  <?php
             while($post_loop->have_posts()):$post_loop->the_post();
                 $client_image = get_post_meta(get_the_ID(),"_cmb_client_image",true);
                 $client_link = get_post_meta(get_the_ID(),"_cmb_client_link",true); ?>
                            <div class="client-box">
                            <?php if(isset($client_link) && $client_link != '') { ?>
                                <a href="<?php echo esc_url($client_link, 'yachtsailing'); ?>">
                                     <img src="<?php echo esc_url($client_image, 'yachtsailing');?>" alt=""/></a>
                            <?php } else { ?>
                            	<img src="<?php echo esc_url($client_image, 'yachtsailing');?>" alt=""/>
                            <?php } ?>
                            </div>
         <?php endwhile; ?>
	</div>	
    </div>   			
</div><?php echo $this->endBlockComment('clientowlCarousel'); 
?>	