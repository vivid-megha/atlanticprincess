<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $link
 * @var $size
 * @var $el_class
 * @var $css
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Gmaps
 */
$title = $link = $size = $el_class = $css = '';
$output = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
/* Extra design option variables starts here */
$hide_map = $atts["hide_map"];
$latitude_data = $atts["latitude_data"];
$longitude_data = $atts["longitude_data"];
$map_height = $atts["map_height"];
$map_id = $atts["map_id"];
if(empty($map_id))
{
	$map_id = "map";
}
/* Extra design option variables ends here */
/* Extra design option Styling code starts here */
$cmap_styles = "";
$cmap_attributes = "";
$cmap_styles = array();
$cmap_attributes = array();
if ( $map_height ) {
	$cmap_styles[] = vc_get_css_color( 'height', $map_height."px" );
}
if ( $cmap_styles ) {
	$cmap_attributes[] = 'style="' . implode( ' ', $cmap_styles ) . '"';
}
$cmap_attributes = implode( ' ', $cmap_attributes );
/* Extra design option Styling code ends here */
$zoom = 14; // deprecated 4.0.2. In 4.6 was moved outside from shortcode_atts
$type = 'm'; // deprecated 4.0.2
$bubble = ''; // deprecated 4.0.2
if ( '' === $link ) {
	return null;
}
$link = trim( vc_value_from_safe( $link ) );
$bubble = ( '' !== $bubble && '0' !== $bubble ) ? '&amp;iwloc=near' : '';
$size = str_replace( array( 'px', ' ' ), array( '', '' ), $size );
if ( is_numeric( $size ) ) {
	$link = preg_replace( '/height="[0-9]*"/', 'height="' . $size . '"', $link );
}
$class_to_filter = 'wpb_gmaps_widget wpb_content_element' . ( '' === $size ? ' vc_map_responsive' : '' );
$class_to_filter .= vc_shortcode_custom_css_class( $css, ' ' ) . $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class_to_filter, $this->settings['base'], $atts );
?>
<div class="<?php echo esc_attr( $css_class , 'yachtsailing'); ?>">
	<?php echo wpb_widget_title( array( 'title' => $title, 'extraclass' => 'wpb_map_heading' ) ); ?>
	<div class="wpb_wrapper">
		<div class="wpb_map_wraper">
		<?php
			if($hide_map == "yes")
			{
		?>
          <div id="<?php echo esc_attr($map_id, 'yachtsailing');?>" class="y-map" data-latt="<?php echo $latitude_data; ?>" data-long="<?php echo $longitude_data?>" <?php echo $cmap_attributes;?>></div>
        <?php
			}
			else
			{
				if ( preg_match( '/^\<ifram/', $link ) ) {
					echo $link;
				} 
			}
			?>
		</div>
	</div>
</div>
