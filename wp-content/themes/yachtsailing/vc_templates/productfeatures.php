<?php
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''	,
	'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* ctitle variables starts here */
    $title = $atts["title"]; 
    $count = $atts["count"];
    $orderby = $atts["orderby"];
	$order = $atts["order"];
    $el_class	= $atts["el_class"];
	
?>
<h2 class="title title--main"><span class="title__bold"><?php echo $title; ?></span><span class="line line--title line--title--main"><span class="line__first"></span><span class="line__second"></span></span></h2>
<ul class="js-featured-slider enable-bx-slider" data-auto="false" data-auto-hover="true" data-mode="vertical" data-pager="false" data-pager-custom="null" data-prev-selector=".featured .technical" data-next-selector=".featured .technical">
	<?php 
	$featured = wp_get_recent_posts( array( 'numberposts' => $count, 'meta_key' => '_featured', 'meta_value' => 'yes', 'category' => 0, 'orderby' => '$orderby', 'order' => '$order', 'post_type' => 'product','suppress_filters' => false ) );
			foreach($featured as $feature){
			$image = wp_get_attachment_image_src( get_post_thumbnail_id($feature['ID']),'yachtsailing-product-featured');
			$design = get_post_meta($feature['ID'],"_cmb_product_design",true);
			$displacement = get_post_meta($feature['ID'],"_cmb_product_displacement",true);
			$bore = get_post_meta($feature['ID'],"_cmb_product_bore",true);
			$stroke = get_post_meta($feature['ID'],"_cmb_product_stroke",true);
			$performance = get_post_meta($feature['ID'],"_cmb_product_performance",true);
			$cold_start_device = get_post_meta($feature['ID'],"_cmb_product_cold_start_device",true); ?>
			<li><div class="row">
			<div class="col-xs-5">
			<a href="<?php echo get_permalink($feature['ID']); ?>"><img src="<?php echo $image[0]; ?>" alt="" class="img-responsive" /></a>
			</div>
			<div class="col-xs-7">
			<div class="featured__info">
			<h4><a href="<?php echo get_permalink($feature['ID']); ?>" class="no-decoration"><?php echo $feature['post_title']; ?></a></h4>
			<p class="text"><?php echo $feature['post_excerpt']; ?></p>
			<div class="technical">
			<h5><?php echo __('Technical Specifications:','yachtsailing'); ?></h5>			
			<?php 
			if($design!=''){ ?>
			<div class="row">
			<div class="col-xs-4"><h6 class="technical__title"><?php echo __('Design','yachtsailing'); ?></h6></div>
			<div class="col-xs-8"><p class="technical__val"><?php echo $design; ?></p></div>
			</div>
			<?php }
			if($displacement!=''){ ?>
			<div class="row">
			<div class="col-xs-4"><h6 class="technical__title"><?php echo __('Displacement','yachtsailing');?></h6></div>
			<div class="col-xs-8"><p class="technical__val"><?php echo $displacement; ?></p></div>
			</div>
			<?php }
			if($bore!=''){ ?>
			<div class="row">
			<div class="col-xs-4"><h6 class="technical__title"><?php echo __('Bore','yachtsailing'); ?></h6></div>
			<div class="col-xs-8"><p class="technical__val"><?php echo $bore; ?></p></div>
			</div>
			<?php }
			if($stroke!=''){ ?>
			<div class="row">
			<div class="col-xs-4"><h6 class="technical__title"><?php echo __('Stroke','yachtsailing'); ?></h6></div>
			<div class="col-xs-8"><p class="technical__val"><?php echo $stroke;?></p></div>
			</div>
			<?php }
			if($performance!=''){ ?>
			<div class="row">
			<div class="col-xs-4"><h6 class="technical__title"><?php echo __('Performance','yachtsailing'); ?></h6></div>
			<div class="col-xs-8"><p class="technical__val"><?php echo $performance; ?></p></div>
			</div>
			<?php } if($cold_start_device!=''){ ?>
			<div class="row">
			<div class="col-xs-4"><h6 class="technical__title"><?php echo __('Cold Start Device','yachtsailing'); ?></h6></div>
			<div class="col-xs-8"><p class="technical__val"><?php echo $cold_start_device; ?></p></div>
			</div>
			<?php } ?>
			</div>
			</div>
			</div>
			</div>
			</li>
		<?php } ?>
</ul>
<?php echo $this->endBlockComment('productfeatures'); ?>	