<?php
/*
* Yacht Sailing functions and definitions
* Set up the theme and provides some helper functions, which are used in the
* theme as custom template tags. Others are attached to action and filter
* hooks in WordPress to change core functionality.
*/
// Enable featured image
add_theme_support('post-thumbnails');
// $content_width
if (!isset($content_width))
	{
	$content_width = 770;
	}
// Add default posts and comments RSS feed links to head
add_theme_support('automatic-feed-links');
// Load theme languages
load_theme_textdomain('yachtsailing', get_template_directory() . '/languages');
require_once get_template_directory() . '/includes/redux/sample-config.php';
// yachtsailing widget
require_once get_template_directory() . '/includes/yachtsailing-widget.php';
// feature product widget
require_once get_template_directory() . '/includes/get-feature-product-widget.php';
// add extra feils in autor profile
require_once get_template_directory() . '/includes/get-authos-fields.php';
// custom post filed
if (isset($theme_option))
	{
	require_once get_template_directory() . '/includes/custom-metafield/metabox-functions.php';
	}
// Add new image sizes
add_action('after_setup_theme', 'yachtsailing_theme_setup');
function yachtsailing_theme_setup()
	{
	add_image_size('yachtsailing-portfolio', 370, 370, true);
	add_image_size('yachtsailing-offer', 270, 270, true);
	add_image_size('yachtsailing-blog-listing-thumb', 270, 270, true);
	add_image_size('yachtsailing-blog-listing-medium', 540, 360, true);
	add_image_size('yachtsailing-destination-thumb', 324, 324, true);
	add_image_size('yachtsailing-sevices', 653, 602, true);
	add_image_size('yachtsailing-blog-sidebar', 72, 72, true);
	add_image_size('yachtsailing-blog-thumb', 575, 250, true);
	add_image_size('yachtsailing-blog-big', 850, 450, true);
	add_image_size('yachtsailing-post-latest', 370, 400, true);
	add_image_size('yachtsailing-post-single', 850, 350, true);
	add_image_size('yachtsailing-author', 153, 153, true);
	add_image_size('yachtsailing-post-latest', 370, 400, true);
	add_image_size('yachtsailing-product-featured', 470, 270, true);
	add_image_size('yachtsailing-feature-product', 60, 60, true);
	add_image_size('yachtsailing-product-shop', 123, 123, true);
	add_image_size('yachtsailing-product-single', 650, 650, true);
	add_image_size('yachtsailing-product-listing', 259, 259, true);
	add_theme_support('title-tag');
	add_theme_support('woocommerce');
/**
 * Custom template tags for this theme.
 *
 * @since Yachtsailing 1.0
 */
require get_template_directory() . '/inc/template-tags.php';
/**
 * Customizer additions.
 *
 * @since Yachtsailing 1.0
 */
require get_template_directory() . '/inc/customizer.php';
    
$color_scheme  = yachtsailing_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );
	// Setup the WordPress core custom background feature.
	/**
	 * Filter Yachtsailing custom-header support arguments.
	 *
	 * @since Yachtsailing 1.0
	 *
	 * @param array $args {
	 *     An array of custom-header support arguments.
	 *
	 *     @type string $default-color     		Default color of the header.
	 *     @type string $default-attachment     Default attachment of the header.
	 * }
	 */
	add_theme_support( 'custom-background', apply_filters( 'yachtsailing_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => __( 'fixed', 'yachtsailing'),
	) ) );
    
    
if ( ! function_exists( 'yachtsailing_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Fifteen.
 *
 * @since Twenty Fifteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function yachtsailing_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = __( 'latin,latin-ext', 'yachtsailing');
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'yachtsailing' ) ) {
		$fonts[] = __( 'Noto Sans:400italic,700italic,400,700', 'yachtsailing') ;
	}
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'yachtsailing' ) ) {
		$fonts[] = __( 'Noto Serif:400italic,700italic,400,700', 'yachtsailing') ;
	}
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'yachtsailing' ) ) {
		$fonts[] = __( 'Inconsolata:400,700', 'yachtsailing');
	}
	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'yachtsailing' );
	if ( 'cyrillic' == $subset ) {
		$subsets .= __( ',cyrillic,cyrillic-ext', 'yachtsailing');
	} elseif ( 'greek' == $subset ) {
		$subsets .= __( ',greek,greek-ext', 'yachtsailing' );
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= __( ',devanagari', 'yachtsailing') ;
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= __( ',vietnamese', 'yachtsailing');
	}
	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), __( 'https://fonts.googleapis.com/css', 'yachtsailing') );
	}
	return $fonts_url;
}
endif;
	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( __( 'css/editor-style.css','yachtsailing'), __( 'genericons/genericons.css','yachtsailing' ), yachtsailing_fonts_url() ) );
    
	}
function yachtsailing_global_var($keyOne = null, $keyTwo = null)
	{
	global $theme_option;
	if ($keyTwo != null)
		{
		if (isset($theme_option[$keyOne][$keyTwo])) return $theme_option[$keyOne][$keyTwo];
		}
	  else
		{
		if (isset($theme_option[$keyOne])) return $theme_option[$keyOne];
		}
	}
// Register Custom Menu Function
if (function_exists('register_nav_menus'))
	{
	register_nav_menus(array(
		'primary' => ('Yacht Sailing Main Menu') ,
	));
	}
// add_sidebar
add_action('widgets_init', 'yachtsailing_slug_widgets_init');
function yachtsailing_slug_widgets_init()
	{
	add_filter('widget_title', 'yachtsailing_remove_widget_title');
	function yachtsailing_remove_widget_title($widget_title)
		{
		if (substr($widget_title, 0, 1) == '!') return;
		  else return ($widget_title);
		}
	global $theme_option;
	$custom_sidebars = isset($theme_option['sidebars_list']) ? $theme_option['sidebars_list'] : null;
	if ($custom_sidebars && is_array($custom_sidebars))
		{
		foreach($custom_sidebars as $sidebar)
			{
			if ($sidebar)
				{
				if (function_exists('register_sidebar'))
					{
					register_sidebar(array(
						'name' => $sidebar,
						'description' => __('This is your custom sidebar.', "yachtsailing") ,
						'id' => strtolower($sidebar) ,
						'before_widget' => __( '<div id="%1$s" class="%2$s widget">', 'yachtsailing' ),
						'after_widget' => __( '</div>', 'yachtsailing'),
						'before_title' => __( '<h4 class="widget-title">', 'yachtsailing' ),
						'after_title' => __( '</h4>', 'yachtsailing')
					));
					}
				}
			}
		}
	register_sidebar(array(
		'name' => esc_html('Sidebar Blog', 'yachtsailing') ,
		'id' => 'sidebar-1',
		'description' => esc_html('Widgets in this area will be shown on blog page. To Hide Title values, just use "!" at first position.', 'yachtsailing') ,
		'before_widget' => __( '<div class="blog-aside__block %2$s y-side_bar_single">', 'yachtsailing' ),
		'after_widget' => '</div>',
		'before_title' => '<header class="y-section_subheader"><h3 class="y-heading">',
		'after_title' => '</h3></header>',
	));
	register_sidebar(array(
		'name' => esc_html('Tags Widget', 'yachtsailing') ,
		'id' => 'tags_widtet',
		'description' => esc_html('Widgets in this area will be shown in footer.', 'yachtsailing') ,
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h5 class="hide_widget_title">',
		'after_title' => '</h5>',
	));
	if (class_exists('Woocommerce'))
		{
		register_sidebar(array(
			'name' => esc_html('Shop Widget', 'yachtsailing') ,
			'id' => 'shop',
			'description' => esc_html('Widgets in this area will be shown in shop page. To Hide Title values, just use "!" at first position.', 'yachtsailing') ,
			'before_widget' => '<div class="blog-aside__block %2$s y-side_bar_single">',
			'after_widget' => '</div>',
			'before_title' => '<header class="y-section_subheader"><h3 class="y-heading">',
			'after_title' => '</h3></header>',
		));
		}
	register_sidebar(array(
		'name' => esc_html('Footer widget', 'yachtsailing') ,
		'id' => 'footer_widget',
		'description' => esc_html('Widgets in this area will be shown in footer. To Hide Title values, just use "!" at first position.', 'yachtsailing') ,
		'before_widget' => '<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">',
		'after_widget' => '</div>',
		'before_title' => '<h5>',
		'after_title' => '</h5>',
	));
	}
// Add html5 suppost for search form and comments list
add_theme_support('html5', array(
	'search-form',
	'comment-form',
	'comment-list'
));
// TGM class 2.5.0 - neccessary plugins
require_once get_template_directory() . '/includes/class-tgm-plugin-activation.php';
add_action('tgmpa_register', 'yachtsailing_theme_register_required_plugins');
function yachtsailing_theme_register_required_plugins()
	{
	$plugins = array(
		array(
			'name' => __('Redux Framework', 'yachtsailing') ,
			'slug' => 'redux-framework',
			'required' => true,
		) ,
		array(
			'name' => __('Woocommerce', 'yachtsailing') ,
			'slug' => 'woocommerce',
			'required' => true,
		) ,
		array(
			'name' => __('Contact Form 7', 'yachtsailing') ,
			'slug' => 'contact-form-7',
			'required' => true,
		) ,
		array(
			'name' => __('MailChimp Newsletters', 'yachtsailing') ,
			'slug' => 'mailchimp-for-wp',
			'required' => true,
		) ,
		array(
			'name' => __('Yacht Custom post type', 'yachtsailing') ,
			'slug' => 'yacht_cpt',
			'required' => true,
			'source' => get_template_directory() . '/includes/plugins/yacht_cpt.zip', // The plugin source
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => '', // If set, overrides default API URL and points to an external URL
		) ,
		array(
			'name' => __('Revolution Slider', 'yachtsailing') ,
			'slug' => 'revslider',
			'required' => true,
			'source' => get_template_directory() . '/includes/plugins/revslider.zip', // The plugin source
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => '', // If set, overrides default API URL and points to an external URL
		) ,
		array(
			'name' => __('WPBakery Visual Composer', 'yachtsailing') ,
			'slug' => 'js_composer',
			'required' => true,
			'source' => get_template_directory() . '/includes/plugins/js_composer.zip', // The plugin source
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => '', // If set, overrides default API URL and points to an external URL
		) ,
	);
	/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'default_path' => '', // Default absolute path to pre-packaged plugins.
		'menu' => 'tgmpa-install-plugins', // Menu slug.
		'has_notices' => true, // Show admin notices or not.
		'dismissable' => true, // If false, a user cannot dismiss the nag message.
		'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false, // Automatically activate plugins after installation or not.
		'message' => '', // Message to output right before the plugins table.
		'strings' => array(
			'page_title' => esc_html('Install Required Plugins', 'yachtsailing') ,
			'menu_title' => esc_html('Install Plugins', 'yachtsailing') ,
			'installing' => esc_html('Installing Plugin: %s', 'yachtsailing') , // %s = plugin name.
			'oops' => esc_html('Something went wrong with the plugin API.', 'yachtsailing') ,
			'notice_can_install_required' => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_cannot_install' => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_cannot_activate' => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'yachtsailing') , // %1$s = plugin name(s).
			'notice_cannot_update' => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'yachtsailing') , // %1$s = plugin name(s).
			'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins', 'yachtsailing') ,
			'activate_link' => _n_noop('Begin activating plugin', 'Begin activating plugins', 'yachtsailing') ,
			'return' => esc_html('Return to Required Plugins Installer', 'yachtsailing') ,
			'plugin_activated' => esc_html('Plugin activated successfully.', 'yachtsailing') ,
			'complete' => esc_html('All plugins installed and activated successfully. %s', 'yachtsailing') , // %s = dashboard link.
			'nag_type' => 'updated'
			// Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		)
	);
	tgmpa($plugins, $config);
	}
// Load necessary theme scripts and styles
function yachtsailing_theme_scripts_styles()
	{
	// Adds JavaScript to pages with the comment form to support sites with
	if (is_singular() && comments_open() && get_option('thread_comments')) wp_enqueue_script('comment-reply');
	wp_enqueue_script("modernizr", get_template_directory_uri() . "/js/modernizr.custom.js", array(
		'jquery'
	) , false, true);
    wp_enqueue_script("responsive-multimenu", get_template_directory_uri() . "/assets/js/responsivemultimenu.js", array(
		'jquery'
	) , true, false);    
	wp_enqueue_script("waypoints", get_template_directory_uri() . "/js/waypoints.min.js", array(
		'jquery'
	) , false, true);
	wp_enqueue_script("easypiechart", get_template_directory_uri() . "/js/jquery.easypiechart.min.js", array(
		'jquery'
	) , false, true);
	wp_enqueue_script("classie", get_template_directory_uri() . "/js/classie.js", array(
		'jquery'
	) , false, true);
	wp_enqueue_script("smooth-scroll", get_template_directory_uri() . "/js/jquery.smooth-scroll.js", array(
		'jquery'
	) , false, true);
	wp_enqueue_script("placeholder", get_template_directory_uri() . "/js/jquery.placeholder.min.js", array(
		'jquery'
	) , false, true);
	wp_enqueue_script("theme", get_template_directory_uri() . "/js/theme.js", array(
		'jquery'
	) , false, true);
	wp_enqueue_script("camera", get_template_directory_uri() . "/assets/js/camera.min.js", array(
		'jquery'
	) , false, true);	
	wp_enqueue_script("yachtsailing-custom-js", get_template_directory_uri() . "/js/yachtsailing-custom.js", array(
		'jquery'
	) , true, false);
	wp_enqueue_script("colpick", get_template_directory_uri() . "/js/colpick.js", array(
		'jquery'
	) , true, false);	
	wp_enqueue_script("wow-min", get_template_directory_uri() . "/assets/js/wow.min.js", array(
		'jquery'
	) , true, false);
	wp_enqueue_script("owl-carousel", get_template_directory_uri() . "/assets/js/owl.carousel.min.js", array(
		'jquery'
	) , true, false);
	wp_enqueue_script("ui", get_template_directory_uri() . "/assets/js/jquery.ui.js", array(
		'jquery'
	) , true, false);
	wp_enqueue_script("counterup", get_template_directory_uri() . "/assets/js/jquery.counterup.min.js", array(
		'jquery'
	) , true, false);
	wp_enqueue_script("yachtsailing-custom", get_template_directory_uri() . "/assets/js/yachtsailing-custom.min.js", array(
		'jquery'
	) , true, false);
	wp_enqueue_script("boot-js", get_template_directory_uri() . "/assets/js/bootstrap.min.js", array(
		'jquery'
	) , true, false);
	// Loads our main stylesheet.
	wp_enqueue_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');
	wp_enqueue_style('owltheme', get_template_directory_uri() . '/assets/owl-carousel/owl.theme.css');
	wp_enqueue_style('yachtsailing-page', get_template_directory_uri() . '/css/yachtsailing-page.css');
	wp_enqueue_style('yachtsailing-shop', get_template_directory_uri() . '/css/yachtsailing-shop.css');
	wp_enqueue_style('yachtsailing-responsive', get_template_directory_uri() . '/css/yachtsailing-responsive.css');
	wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('colpick', get_template_directory_uri() . '/css/colpick.css');
    
	// Loads current theme css
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('yachtsailing-reset', get_template_directory_uri() . '/assets/css/yachtsailing-reset.css');
	wp_enqueue_style('owl', get_template_directory_uri() . '/assets/css/owl.carousel.css');
	wp_enqueue_style('camera', get_template_directory_uri() . '/assets/css/camera.css');
	wp_enqueue_style('tabs', get_template_directory_uri() . '/assets/css/tabs.css');
	wp_enqueue_style('yachtsailing-main', get_template_directory_uri() . '/assets/css/yachtsailing-main.css');
	wp_enqueue_style('yachtsailing-custom', get_template_directory_uri() . '/assets/css/yachtsailing-custom.css');
    
    // Load our main stylesheet.
	wp_enqueue_style( 'yachtsailing-style', get_stylesheet_uri() );
   }
add_action('wp_enqueue_scripts', 'yachtsailing_theme_scripts_styles');
// PRETTY PHOTO //
function yachtsailing_plug_exists($name, $dir = null)
	{
	if (!$dir) $dir = $name;
	$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));
	if (in_array($dir . '/' . $name . '.php', $active_plugins)) return true;
	return false;
	}
add_theme_support('post-formats', array(
	'audio',
	'gallery',
	'image',
	'link',
	'video'
));
// post navigation
function yachtsailing_numeric_posts_nav()
	{
	if (is_singular()) return;
	global $wp_query;
	/** Stop execution if there's only 1 page */
	if ($wp_query->max_num_pages <= 1) return;
	$paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
	$max = intval($wp_query->max_num_pages);
	/** Add current page to the array */
	if ($paged >= 1) $links[] = $paged;
	/** Add the pages around the current page to the array */
	if ($paged >= 3)
		{
		$links[] = $paged - 1;
		$links[] = $paged - 2;
		}
	if (($paged + 2) <= $max)
		{
		$links[] = $paged + 2;
		$links[] = $paged + 1;
		}
	echo '<div class="pagination wow slideInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">' . "\n";
	echo '<span class="pagination__text">Page ' . $paged . ' of ' . $max . '</span>';
	echo '<span class="pagination__row">';
	/** Link to first page, plus ellipses if necessary */
	/** Previous Post Link */
	if (get_previous_posts_link()) echo '<a href="' . get_previous_posts_page_link() . '" class="pagination__cell pagination__cell--nav no-decoration">&laquo;</a>';
	if (!in_array(1, $links))
		{
		$class = 1 == $paged ? ' class="pagination__cell pagination__cell--active no-decoration"' : '';
		printf('<a%s href="%s">%s</a>' . "\n", $class, esc_url(get_pagenum_link(1)) , '1', 'yachtsailing');
		if (!in_array(2, $links)) echo '<a href="#" class="pagination__cell no-decoration">...</a>';
		}
	/* Link to current page, plus 2 pages in either direction if necessary */
	sort($links);
	foreach((array)$links as $link)
		{
		$class = $paged == $link ? ' class="pagination__cell pagination__cell--active no-decoration"' : '';
		printf('<a%s href="%s">%s</a>' . "\n", $class, esc_url(get_pagenum_link($link)) , $link, 'yachtsailing');
		}
	/** Link to last page, plus ellipses if necessary */
	if (!in_array($max, $links))
		{
		if (!in_array($max - 1, $links)) echo '<a href="#" class="pagination__cell no-decoration">...</a>' . "\n";
		$class = $paged == $max ? ' class="pagination__cell pagination__cell--active no-decoration"' : '';
		printf('<a%s href="%s">%s</a>' . "\n", $class, esc_url(get_pagenum_link($max)) , $max, 'yachtsailing');
		}
	/** Next Post Link */
	if (get_next_posts_link()) echo '<a href="' . get_next_posts_page_link() . '" class="pagination__cell pagination__cell--nav no-decoration">&raquo;</a>';
	echo '</span></div>' . "\n";
	}
class String_new
	{
	public static
	function truncate($s, $l, $e = '...', $isHTML = false)
		{
		$i = 0;
		$tags = array();
		if ($isHTML)
			{
			preg_match_all('/<[^>]+>([^<]*)/', $s, $m, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
			foreach($m as $o)
				{
				if ($o[0][1] - $i >= $l) break;
				$t = substr(strtok($o[0][0], " \t\n\r\0\x0B>") , 1);
				if ($t[0] != '/') $tags[] = $t;
				elseif (end($tags) == substr($t, 1)) array_pop($tags);
				$i+= $o[1][1] - $o[0][1];
				}
			}
		return substr($s, 0, $l = min(strlen($s) , $l + $i)) . (count($tags = array_reverse($tags)) ? '</' . implode('></', $tags) . '>' : '') . (strlen($s) > $l ? $e : '');
		}
	public static
	function content_limit($max_char, $allow_tags = '')
		{
		$content = get_the_content('');
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]&gt;', $content);
		$content = strip_tags($content, $allow_tags);
		return String::truncate($content, $max_char, '', true, false);
		}
	}
function yachtsailing_fonts_url()
	{
	$fonts_url = '';
	$fonts = array();
	$subsets = 'latin,latin-ext';
	/* Translators: If there are characters in your language that are not
	* supported by Lora, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$Montserrat = _x('on', 'Montserrat font: on or off', 'yachtsailing');
	$Material = _x('on', 'Material font: on or off', 'yachtsailing');
    
	/* Translators: If there are characters in your language that are not
	* supported by Open Sans, translate this to 'off'. Do not translate
	* into your own language.
	*/
	$Vollkorn = _x('on', 'Vollkorn font: on or off', 'yachtsailing');
	$Roboto = _x('on', 'Roboto font: on or off', 'yachtsailing');
	$font_families = array();
	if ('off' !== $Montserrat)
		{
		$fonts[] = 'Montserrat:400,700';
		}
	if ('off' !== $Material)
		{
		$fonts[] = 'Material Icons:400';
		}
	if ('off' !== $Vollkorn)
		{
		$fonts[] = 'Vollkorn:400italic';
		}
	if ('off' !== $Roboto)
		{
		$fonts[] = 'Roboto:';
		}
	if ($fonts)
		{
		$fonts_url = add_query_arg(array(
			'family' => urlencode(implode('|', $fonts)) ,
			'subset' => urlencode($subsets) ,
		) , '//fonts.googleapis.com/css');
		}
	return $fonts_url;
	}
/* Enqueue scripts and styles. */
function yachtsailing_scripts()
	{
	wp_enqueue_style('yachtsailing-google-fonts', yachtsailing_fonts_url() , array() , '1.0.0');
	}
add_action('wp_enqueue_scripts', 'yachtsailing_scripts');
class yachtsailing_menu_walker extends Walker_Nav_Menu
	{
	function start_lvl(&$output, $depth = 0, $args = array())
		{
		$indent = str_repeat("\t", $depth);
		$output.= "\n$indent<ul class=\"rmm-submenu\">\n";
		}
	function start_el(&$output, $item, $depth = 0, $args = array() , $id = 0)
		{
		global $wp_query;
		$indent = ($depth) ? str_repeat("\t", $depth) : '';
		$class_names = $value = '';
		$classes = empty($item->classes) ? array() : (array)$item->classes;
		$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes) , $item));
		$class_names = ' class="' . esc_attr($class_names, 'yachtsailing') . '"';
		$output.= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
		$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title, 'yachtsailing') . '"' : '';
		$attributes.= !empty($item->target) ? ' target="' . esc_attr($item->target, 'yachtsailing') . '"' : '';
		$attributes.= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn, 'yachtsailing') . '"' : '';
		$attributes.= !empty($item->url) ? ' href="' . esc_attr($item->url, 'yachtsailing') . '"' : '';
		if ($args->has_children)
			{
			$attributes.= ' class="rmm-dropdown"';
			$attributes.= ' data-toggle="dropdown"';
			}
		  else
			{
			$attributes.= ' class="no-decoration"';
			}
		$prepend = '';
		$append = '';
		$description = !empty($item->description) ? '<span>' . esc_attr($item->description, 'yachtsailing') . '</span>' : '';
		if ($depth != 0)
			{
			$description = $append = $prepend = "";
			}
		$item_output = $args->before;
		$item_output.= '<a' . $attributes . '>';
		$item_output.= $args->link_before . $prepend . apply_filters('the_title', $item->title, $item->ID) . $append;
		$item_output.= $description . $args->link_after;
		if ($args->has_children)
			{
			$item_output.= ' ';
			}
		$item_output.= '</a>';
		if ($depth == 0)
			{
			$item_output.= $args->after;
			}
		$output.= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
		if ($item->menu_order == 1)
			{
			$classes[] = 'first';
			}
		}
	function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
		{
		$id_field = $this->db_fields['id'];
		if (is_object($args[0]))
			{
			$args[0]->has_children = !empty($children_elements[$element->$id_field]);
			}
		return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
		}
	}
function yachtsailing_search_form($form)
	{
	$form = '<form method="get" id="y-side_search" action="' . home_url('/') . '" >
	<input type="text" class="search-input" placeholder="' . esc_html('Search...', 'yachtsailing') . '" value="' . get_search_query() . '" name="s" id="s" /><input type="submit" class="y-search-submit" value="Search" />
	</form>';
	return $form;
	}
add_filter('get_search_form', 'yachtsailing_search_form');
/*********Breadcrum*************/
function yachtsailing_breadcrumbs()
	{
	// Settings
	$separator = __( '&raquo;', 'yachtsailing');
	$breadcrums_id = __( 'breadcrumbs', 'yachtsailing');
	$breadcrums_class = __( 'breadcumb', 'yachtsailing');
	$home_title = __( 'Home', 'yachtsailing' );
	// If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
	$custom_taxonomy = __( 'product_cat', 'yachtsailing');
	// Get the query & post information
	global $post, $wp_query;
	// Do not display on the homepage
	// Build the breadcrums
	echo __( '<ul class="pull-right">', 'yachtsailing' );
	// Home page
	echo '<li><a class="breadcumb__page no-decoration" href="' . get_home_url() . '" title="' . esc_html($home_title, 'yachtsailing') . '">' . esc_html($home_title, 'yachtsailing') . '</a>
		</li>';
	if (is_archive() && !is_tax() && !is_category() && !is_tag())
		{
		echo '<li><a href="#" class="breadcumb__page no-decoration">' . post_type_archive_title($prefix = '', false) . '</a></li>';
		}
	  else
	if (is_archive() && is_tax() && !is_category() && !is_tag())
		{
		// If post is a custom post type
		$post_type = get_post_type();
		// If it is a custom post type display name and link
		if ($post_type != 'post')
			{
			$post_type_object = get_post_type_object($post_type);
			$post_type_archive = get_post_type_archive_link($post_type);
			echo '<li><a class="breadcumb__page no-decoration bread-custom-post-type-' . esc_html($post_type, 'yachtsailing') . '" href="' . esc_url($post_type_archive, 'yachtsailing') . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
			}
		$custom_tax_name = get_queried_object()->name;
		echo '<li><a href="#" class="breadcumb__page no-decoration">' . esc_html($custom_tax_name, 'yachtsailing') . '</a></li>';
		}
	  else
	if (is_single())
		{
		// If post is a custom post type
		$post_type = get_post_type();
		// If it is a custom post type display name and link
		if ($post_type != 'post')
			{
			$post_type_object = get_post_type_object($post_type);
			$post_type_archive = get_post_type_archive_link($post_type);
			echo '<li><a class="breadcumb__page no-decoration bread-custom-post-type-' . esc_html($post_type, 'yachtsailing') . '" href="' . esc_url($post_type_archive, 'yachtsailing') . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
			}
		// Get post category info
		$category = get_the_category();
		if (!empty($category))
			{
			// Get last category post is in
			$last_category = end($category);
			// Get parent any categories and create array
			$get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ',') , ',');
			$cat_parents = explode(',', $get_cat_parents);
			// Loop through parent categories and store in variable $cat_display
			$cat_display = '';
			foreach($cat_parents as $parents)
				{
				preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', $parents, $link);
				$cat_display.= '<li><a href="' . esc_url($link[1], 'yachtsailing') . '" class="breadcumb__page no-decoration">' . esc_html($link[2], 'yachtsailing') . '</a></li>';
				}
			}
		// Check if the post is in a category
		if (!empty($last_category))
			{
			echo $cat_display;
			echo '<li><a href="#" class="breadcumb__page no-decoration" title="' . get_the_title() . '">' . get_the_title() . '</a></li>';
			}
		  else
		if (!empty($cat_id))
			{
			echo '<li><a class="breadcumb__page no-decoration bread-cat-' . $cat_id . ' bread-cat-' . esc_html($cat_nicename, 'yachtsailing') . '" href="' . esc_url($cat_link, 'yachtsailing') . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
			echo '<li><a href="' . get_permalink($post->ID) . '" class="breadcumb__page no-decoration" title="' . get_the_title() . '">' . get_the_title() . '</a></li>';
			}
		  else
			{
			echo '<li><a href="' . get_permalink($post->ID) . '" class="breadcumb__page no-decoration" title="' . get_the_title() . '">' . get_the_title() . '</a></li>';
			}
		}
	  else
	if (is_category())
		{
		// Category page
		echo '<li><a href="#" class="breadcumb__page no-decoration">' . single_cat_title('', false) . '</a></li>';
		}
	  else
	if (is_page())
		{
		// Standard page
		if ($post->post_parent)
			{
			// If child page, get parents
			$anc = get_post_ancestors($post->ID);
			// Get parents in the right order
			$anc = array_reverse($anc);
			// Parent page loop
			foreach($anc as $ancestor)
				{
				$parents = '';
				$parents.= '<li><a class="breadcumb__page no-decoration bread-parent-' . esc_html($ancestor, 'yachtsailing') . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
				}
			// Display parent pages
			echo $parents;
			// Current page
			echo '<li><a href="' . get_permalink($post->ID) . '" class="breadcumb__page no-decoration" title="' . get_the_title() . '"> ' . get_the_title() . '</a></li>';
			}
		  else
			{
			// Just display current page if not parents
			echo '<li><a href="' . get_permalink($post->ID) . '" class="breadcumb__page no-decoration" title="' . get_the_title() . '"> ' . get_the_title() . '</a></li>';
			}
		}
	  else
	if (is_tag())
		{
		// Tag page
		// Get tag information
		$term_id = get_query_var('tag_id');
		$taxonomy = __( 'post_tag', 'yachtsailing');
		$args = 'include=' . $term_id;
		$terms = get_terms($taxonomy, $args);
		$get_term_id = $terms[0]->term_id;
		$get_term_slug = $terms[0]->slug;
		$get_term_name = $terms[0]->name;
		// Display the tag name
		echo '<li><a href="' . get_permalink($get_term_id) . '" class="breadcumb__page no-decoration">' . esc_html($get_term_name, 'yachtsailing') . '</a></li>';
		}
	elseif (is_day())
		{
		// Day archive
		// Year link
		echo '<li><a class="breadcumb__page no-decoration" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
		// Month link
		echo '<li><a class="breadcumb__page no-decoration" href="' . get_month_link(get_the_time('Y') , get_the_time('m')) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
		// Day display
		echo '<li><a href="' . get_day_link(get_the_time('Y') , get_the_time('m') , get_the_time('jS')) . '" class="breadcumb__page no-decoration"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</a></li>';
		}
	  else
	if (is_month())
		{
		// Month Archive
		// Year link
		echo '<li><a class="breadcumb__page no-decoration" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
		// Month display
		echo '<li><a href="#" class="breadcumb__page no-decoration" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
		}
	  else
	if (is_year())
		{
		// Display year archive
		echo '<li><a href="#" class="breadcumb__page no-decoration" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
		}
	  else
	if (is_author())
		{
		// Auhor archive
		// Get the author information
		global $author;
		$userdata = get_userdata($author);
		// Display author name
		echo '<li><a href="#" class="breadcumb__page no-decoration" title="' . esc_html($userdata->display_name, 'yachtsailing') . '">' . esc_html_e('Author:', 'yachtsailing') . esc_html($userdata->display_name, 'yachtsailing') . '</a></li>';
		}
	  else
	if (get_query_var('paged'))
		{
		// Paginated archives
		echo '<li><a href="#" class="breadcumb__page no-decoration" title="Page ' . get_query_var('paged') . '">' . esc_html('Page', 'yachtsailing') . ' ' . get_query_var('paged') . '</a></li>';
		}
	  else
	if (is_search())
		{
		// Search results page
		echo '<li><a href="#" class="breadcumb__page no-decoration" title="Search results for: ' . get_search_query() . '">' . esc_html('Search results for: ', 'yachtsailing') . '' . get_search_query() . '</a></li>';
		}
	elseif (is_404())
		{
		// 404 page
		echo '<li><a href="#" class="breadcumb__page no-decoration">' . esc_html_e('Error 404', 'yachtsailing') . '</a></li>';
		}
	echo '</ul>';
	}
function yachtsailing_getPostViews($postID)
	{
	$count_key = __( 'post_views_count', 'yachtsailing');
	$count = get_post_meta($postID, $count_key, true);
	if ($count == '')
		{
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
		}
	return $count . ' Views';
	}
function yachtsailing_setPostViews($postID)
	{
	$count_key = __( 'post_views_count', 'yachtsailing');
	$count = get_post_meta($postID, $count_key, true);
	if ($count == '')
		{
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		}
	  else
		{
		$count++;
		update_post_meta($postID, $count_key, $count);
		}
	}
function yachtsailing_comment($comment, $args, $depth)
	{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);
	if ($depth == '1'):
?>
	<div class="article-comment clearfix">
    <?php
	else: ?>
    <div class="article-comment article-comment--to clearfix wow slideInRight" data-wow-delay="0.7s" data-wow-duration="1.5s">
    <?php
	endif; ?>
    <?php
	yachtsailing_comment_reply_link(array_merge($args, array(
		'add_below' => $comment->comment_ID,
		'depth' => $depth,
		'max_depth' => $args['max_depth']
	))); ?>
	<div class="article-comment__author"><?php
	if ($args['avatar_size'] != 0) echo get_avatar($comment, 80); ?></div>
	<?php
	if ($comment->comment_approved == '0'): ?> 
		<em class="comment-awaiting-moderation"><?php
		esc_html_e('Your comment is awaiting moderation.', 'yachtsailing'); ?></em>
		<br />
	<?php
	endif; ?>
	<div class="article-comment__body">
    	<h2 class="article-comment__name"><?php
	echo comment_author(); ?></h2>
        <div class="article-comment__date"><?php
	echo get_comment_date(); ?></div>
	</div>
	<?php
	comment_text(); ?>
<?php
	}
function yachtsailing_get_comment_reply_link($args = array() , $comment = null, $post = null)
	{
	$defaults = array(
		'add_below' => __( 'comment', 'yachtsailing'),
		'respond_id' => __( 'respond', 'yachtsailing'),
		'reply_text' => esc_html('Reply', 'yachtsailing') ,
		'login_text' => esc_html('Log in to Reply', 'yachtsailing') ,
		'depth' => 0,
		'before' => '',
		'after' => ''
	);
	$args = wp_parse_args($args, $defaults);
	if (0 == $args['depth'] || $args['max_depth'] <= $args['depth'])
		{
		return;
		}
	$add_below = $args['add_below'];
	$respond_id = $args['respond_id'];
	$reply_text = $args['reply_text'];
	$comment = get_comment($comment);
	if (empty($post))
		{
		$post = $comment->comment_post_ID;
		}
	$post = get_post($post);
	if (!comments_open($post->ID))
		{
		return false;
		}
	if (get_option('comment_registration') && !is_user_logged_in())
		{
		$link = '<a rel="nofollow" class="article-comment__action triangle triangle--12" href="' . esc_url(wp_login_url(get_permalink()), 'yachtsailing') . '">' . $args['login_text'] . '</a>';
		}
	  else
		{
		$link = "<a class='article-comment__action triangle triangle--12' href='" . esc_url(add_query_arg('replytocom', $comment->comment_ID), 'yachtsailing') . "#" . $respond_id . "' onclick='return addComment.moveForm(\"$add_below-$comment->comment_ID\", \"$comment->comment_ID\", \"$respond_id\", \"$post->ID\")'><span class='fa fa-long-arrow-left'></span></a>";
		}
	/**
	 * Filter the comment reply link.
	 *
	 * @since 2.7.0
	 *
	 * @param string  $link    The HTML markup for the comment reply link.
	 * @param array   $args    An array of arguments overriding the defaults.
	 * @param object  $comment The object of the comment being replied.
	 * @param WP_Post $post    The WP_Post object.
	 */
	return apply_filters('yachtsailing_comment_reply_link', $args['before'] . $link . $args['after'], $args, $comment, $post);
	}
function yachtsailing_comment_reply_link($args = array() , $comment = null, $post = null)
	{
	echo yachtsailing_get_comment_reply_link($args, $comment, $post);
	}
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
function yachtsailing_move_comment_field_to_bottom($fields)
	{
	$comment_field = $fields['comment'];
	unset($fields['comment']);
	$fields['comment'] = $comment_field;
	return $fields;
	}
add_filter('comment_form_fields', 'yachtsailing_move_comment_field_to_bottom');
function yachtsailing_remove_loop_button()
	{
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
	}
add_action('init', 'yachtsailing_remove_loop_button');
/* Woocommerce starts */
add_action("woocommerce_output_related_products", "woocommerce_output_related_products");
add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action('woocommerce_after_shop_loop_related_item_title', 'woocommerce_template_loop_related_price', 5);
if (!function_exists('woocommerce_template_loop_related_price'))
	{
	/**
	 * Get the product price for the loop.
	 *
	 * @subpackage	Loop
	 */
	function woocommerce_template_loop_related_price()
		{
		wc_get_template('loop/related_price.php');
		}
	}
/* Woocommerce end */
include (get_template_directory() . '/yachtsailing-css.php');
include (get_template_directory() . '/yachtsailing-js.php');
include (get_template_directory() . '/includes/yachtsailing-vc.php');
include (get_template_directory() . '/includes/yachtsailing-woocommerce.php');
/* Remove notices */
add_action('admin_head', 'yachtsailing_notice_remover');
function yachtsailing_notice_remover()
	{
	echo '<style>
    .rs-update-notice-wrap, .redux-notice, .rAds
    {
     opacity:0;
     display:none !important;
    } 
  </style>';
	}
