<?php
 /* The template for displaying archive pages */
 get_header(); 
?>
<?php 
  echo get_template_part("header","title");
   $postid = $post->ID;   
   $post_image = get_post_meta($postid,"_cmb_featured_image",true);	
   $post_category = get_the_category();		   
?>    
   <section id="y-single_info">
          <div class="y-single_info">
            <div class="container">
                <div class="row y-single_info_inner y-section_content">                    
                    <div class="col-sm-12">
                        <div class="col-lg-8 col-md-8 col-sm-7">
								           <?php                             
                           if ( have_posts() ) :
                                    while ( have_posts() ) : the_post();
                                        get_template_part( 'template-parts/content', get_post_format() );
                                    endwhile;
                                else :
                                    get_template_part( 'template-parts/content', 'none' );
                                endif; 
                            ?>
                        </div>
                      <?php get_sidebar("blog"); ?>
                    </div>
                </div>
            </div>
          </div>
   </section>
<?php get_footer(); ?>