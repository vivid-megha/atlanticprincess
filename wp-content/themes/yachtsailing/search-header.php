<form role="search" method="get" class="navbar-form header_search_form" action="<?php echo esc_url( home_url( '/' ) , 'yachtsailing'); ?>">
	<i class="fa fa-times search-form_close"></i>
    <div class="form-group">
    	<input type="search" class="form-control" placeholder="<?php echo esc_html("Search", "yachtsailing"); ?>" value="<?php echo get_search_query(); ?>" name="s" id="s" />
    </div>
    <button type="submit" class="btn btn_search customBgColor"><?php echo esc_html( 'Search','yachtsailing' ); ?></button>
</form>