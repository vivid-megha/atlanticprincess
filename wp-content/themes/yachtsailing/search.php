<?php
  /* The template for displaying search results pages */
  get_header(); 
?>
<?php echo get_template_part("header","title");?>
<section id="y-single_info">
   <div class="y-single_info">
          <div class="row y-section_content">
              <div class="clearfix">
                <div class="y-corporate_block clearfix">
                <div class="container search_page">
									<?php if ( have_posts() ) : ?>
                                        <?php
                                        while ( have_posts() ) : the_post();
                                            get_template_part( 'template-parts/content', 'search' );
                                        endwhile;
                                    else :
                                        get_template_part( 'template-parts/content', 'none' );
                                    endif;
                                    ?>
                                    <br><br><br>
                                </div>
                                </div>
                            </div>
    </div>
 </div>
</section>
<?php get_footer(); ?>