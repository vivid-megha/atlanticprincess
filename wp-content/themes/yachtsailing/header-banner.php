<?php 
   $header_slider_image_video = get_post_meta($post->ID, "_cmb_header-slider_image_video", true); 
   $header_image = get_post_meta($post->ID, "_cmb_header_image", true); 
   $header_slider_num = get_post_meta($post->ID, "_cmb_header-slider", true); 
   $header_video = get_post_meta($post->ID, "_cmb_header-video", true);  
   
		if($header_slider_num == "")
		{
			$header_slider_num = 0; 
		}
     $rev_slider = new RevSlider();
     $sliders = $rev_slider->getAllSliderAliases();   
	 $header_slider = $sliders[$header_slider_num];
	 
	 
/* Header image starts here */	 
   if($header_slider_image_video != "")
   {
	   if($header_slider_image_video == "image")
	   {
          if($header_image != "")
		  {
	?>
               <div class="gap gap-100">
                  <img src="<?php echo $header_image; ?>" alt="" class="img-responsive" />
               </div>    
    <?php 
	      } 
		  else
		  {
	?>
              <div class="gap gap-100">
                  <img src="<?php  echo get_template_directory_uri()?>/test_images/header_bg_06.jpg" alt="" class="img-responsive" />              
              </div>     
              
    <?php
			  
		  }
	   }	 
   }
/* Header image ends here */	    
/* Header slider starts here */	    
   if($header_slider_image_video != "")
   {
	   if($header_slider_image_video == "slider")
	   {
		   
			  if($header_slider != "")
			  {
				   echo do_shortcode('[rev_slider alias='.$header_slider.']');  
			  } 
			  else
			  {
				 echo do_shortcode('[rev_slider alias="Homepage1"]');
			  }		   
		   
	   }	 
   }
/* Header slider ends here */	    
/* Header slider starts here */	    
   if($header_slider_image_video != "")
   {
	   if($header_slider_image_video == "video")
	   {
		   echo '    <div class="video-container"> ';
			  if($header_video != "")
			  {
				  echo $header_video;
			  } 
			  else
			  {
				?>
  <iframe  height="720" src="https://www.youtube.com/embed/eUHVA2fMFSg?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>                 
                <?php
			  }		   
		   echo "</div>";
	   }	 
   }
/* Header slider ends here */