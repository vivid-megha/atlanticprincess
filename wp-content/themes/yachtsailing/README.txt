=== Yachtsailing ===
Contributors: the Yachtsailing team
Requires at least: WordPress 4
Tested up to: WordPress 4.9-trunk
Version: 1.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: one-column, two-columns, right-sidebar, custom-background, custom-header, custom-menu, editor-style, featured-images, flexible-header, full-width-template, post-formats, sticky-post, theme-options
== Description ==
Yacht Sailing WordPress theme is specially crafted  for  Yacht, Sailing, Marine business, charter booking,  charter selling companies. we spent good time to understand the industry and in result we designed the elements to give you perfect theme to build your awesome website with ease.
For more information about Yachtsailing please go to https://codex.wordpress.org/Twenty_Seventeen.
== Installation ==
1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Upload yachtsailing.zip file.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to Documentation directory attached with theme files for a guide on how to customize this theme.
5. Navigate to Appearance > Customize or Theme options in your admin panel and customize to taste.
== Copyright ==
Yachtsailing WordPress Theme, Copyright 2017 veepixel.com/
Yachtsailing is distributed under the terms of the GNU GPL
This program is premium software.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
Yachtsailing bundles the following third-party resources:
CSS
        style.css
        bootstrap.min.css
        camera.css
        owl.carousel.css
        tabs.css
        yachtsailing-custom.css
        yachtsailing-main.css
        yachtsailing-reset.css
        owl.theme.css
        animate.css
        colpick.css
        font-awesome.min.css
        theme.css
        yachtsailing-page.css
        yachtsailing-responsive.css
        yachtsailing-shop.css
        genericons.css
        font-awesome.min.css
        fontawesome-iconpicker.min.css
        simple-line-icons.css
        style.css
        style.min.css
PHP
        functions.php
        yachtsailing_fonts_url
        yachtsailing_breadcrumbs
        woocommerce_template_loop_related_price
404.php
archive.php
comments.php
footer.php
header-banner.php
header-fifth.php
header-first.php
header-fourth.php
header-inner.php
header-second.php
header-sixth.php
header-third.php
header-title.php
header.php
back-compat.php
        yachtsailing_switch_theme
        yachtsailing_upgrade_notice
        yachtsailing_customize
        yachtsailing_preview
      
      
custom-header.php
        yachtsailing_custom_header_setup
        yachtsailing_hex2rgb
        yachtsailing_header_style
        yachtsailing_header_background_color_css
        yachtsailing_sidebar_text_color_css
        
customizer.php
        yachtsailing_customize_register
        yachtsailing_customize_partial_blogname
        yachtsailing_customize_partial_blogdescription
        yachtsailing_get_color_schemes
        yachtsailing_get_color_scheme
        yachtsailing_get_color_scheme_choices
        yachtsailing_sanitize_color_scheme
        yachtsailing_color_scheme_css
        yachtsailing_customize_control_js
        yachtsailing_customize_preview_js
        yachtsailing_get_color_scheme_css
        yachtsailing_color_scheme_css_template
template-tags.php
        yachtsailing_comment_nav
        yachtsailing_entry_meta
        yachtsailing_categorized_blog
        yachtsailing_category_transient_flusher
        yachtsailing_post_thumbnail
        yachtsailing_get_link_url
        yachtsailing_excerpt_more
        yachtsailing_the_custom_logo
        
        
class-tgm-plugin-activation.php
        tgmpa( $plugins, $config = array
        tgmpa_load_bulk_installer
cmb_Meta_Box_Sanitize.php
cmb_Meta_Box_Show_Filters.php
cmb_Meta_Box_ajax.php
cmb_Meta_Box_field.php
cmb_Meta_Box_types.php
init.php
        __construct
        cmb_get_option
        cmb_get_field
        cmb_get_field_value
        cmb_print_metaboxes
        cmb_print_metabox
        cmb_save_metabox_fields
        cmb_metabox_form
metabox-functions.php
        cmb_sample_metaboxes
        cmb_initialize_cmb_meta_boxes
        
        
get-authos-fields.php
        yachtsailing_get_meta
        
get-feature-product-widget.php
sample-config.php
adding_charters.php
adding_custom_blog_listing.php
adding_custom_carousel.php
adding_custom_clients_owl_carousel.php
adding_custom_counter.php
adding_custom_features.php
adding_custom_offers_crousel.php
adding_custom_owl_carousel.php
adding_custom_product_listing.php
adding_custom_services.php
adding_custom_team_owl_carousel.php
adding_custom_testimonial_owl_carousel.php
adding_destinations.php
adding_yachtcarousel.php
acp_charters.php
acp_destinations.php
acp_teamowlcarousel.php
acp_testimonialowlcarousel.php
acp_vc_btn.php
acp_vc_column_inner.php
acp_vc_column_text.php
acp_vc_custom_heading.php
acp_vc_gmaps.php
acp_vc_inner_row.php
acp_vc_progress_bar.php
acp_vc_row.php
acp_vc_single_image.php
acp_vc_tta_section.php
acp_vc_tta_tabs.php
yachtsailing-vc.php
yachtsailing-widget.php
yachtsailing-woocommerce.php
woocommerce_template_loop_product_title
index.php
page-color_customizer.php
template-home.php
template-vc.php
page.php
search-header.php
search.php
sidebar-blog.php
sidebar-shop.php
sidebar.php
single-yachtsailing_charter.php
single-yachtsailing_desti.php
single.php
social-icons.php
content-none.php
content-page.php
content-search.php
content-single.php
content.php
bloglisting.php
charters.php
clientowlcarousel.php
counter.php
customowlcarousel.php
destinations.php
offercrousel.php
productfeatures.php
productlisting.php
services.php
teamowlcarousel.php
testimonialowlcarousel.php
vc-tta-tabs.php
constructIcon
vc_btn.php
vc_column_inner.php
vc_column_text.php
vc_custom_heading.php
vc_gmaps.php
vc_progress_bar.php
vc_row.php
vc_row_inner.php
vc_single_image.php
vc_tabs.php
vc_tta_global.php
vc_tta_section.php
yachtcarousel.php
archive-product.php
breadcrumb.php
content-product.php
content-single-product.php
sidebar.php
wrapper-end.php
wrapper-start.php
loop-end.php
loop-start.php
price.php
related-product.php
price.php
product-image.php
related.php
tabs.php
tabs.php
title.php
yachtsailing-css.php
yachtsailing-js.php
JS
    bootstrap.min.js
    camera.min.js
    jquery.counterup.min.js
    jquery.ui.js
    nouislider.min.js
    
    
owl.carousel.min.js
    Owl
    getTouches
    isStyleSupported
    isTransition
    isTransform
    isPerspective
    isTouchSupport
    isTouchSupportIE
responsivemultimenu.js
wow.min.js
yachtsailing-custom.min.js
owl.carousel.min.js
cmb.js
cmb.min.js
fontawesome-iconpicker.js
jquery.datePicker.min.js
jquery.timePicker.min.js
classie.js
colpick.js
jquery.easypiechart.min.js
jquery.placeholder.min.js
jquery.smooth-scroll.js
modernizr.custom.js
theme.js
waypoints.min.js
wow.min.js
yachtsailing-custom.js
== Changelog ==
= 1.0 =
* Released: June 30, 2017
http://veepixel.com/demo/yacht/
