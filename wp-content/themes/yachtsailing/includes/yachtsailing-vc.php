<?php
function yachtsailing_enqueue_styles()
 {    
 
 wp_enqueue_style( 'yachtsailing-parent-stylesheet', get_template_directory_uri() . '/style.css' );
 
 }
 add_action( 'wp_enqueue_scripts', 'yachtsailing_enqueue_styles' );
 
 function yachtsailing_lang_setup() {	
	 $lang = get_stylesheet_directory() . '/languages';	
	 load_child_theme_textdomain( 'yachtsailing', $lang );
 }
 add_action( 'after_setup_theme', 'yachtsailing_lang_setup' );
 
 
 if(class_exists('WPBakeryShortCode')){
/* Visual composer includes starts here */
    /* Adding custom elements files starts here */
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_testimonial_owl_carousel.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_team_owl_carousel.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_destinations.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_charters.php";
		
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_counter.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_blog_listing.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_yachtcarousel.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_offers_crousel.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_services.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_features.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_product_listing.php";
		include get_template_directory() . "/includes/vc_custom_code/adding_custom_elements/adding_custom_owl_carousel.php";		
    /* Adding custom elements files ends here */
	
    /* Adding custom parameters files starts here */
	
	/* vc_row_inner */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_inner_row.php";
	
	/* vc_btn */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_btn.php";
	
	/* vc_column_text */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_column_text.php";
	
	/* vc_custom_heading */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_custom_heading.php";
	
	/* teamowlcarousel */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_teamowlcarousel.php";
	
	/* destinations */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_destinations.php";
	
	/* charters */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_charters.php";
	
	/* testimonialowlcarousel */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_testimonialowlcarousel.php";
	
	/* vc_tta_tabs */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_tta_tabs.php";
	
	/* vc_row */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_row.php";
	
	/* vc_single_image */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_single_image.php";
	
	/* vc_column_inner */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_column_inner.php";
	
	/* vc_column_inner */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_progress_bar.php";
	/* vc_gmaps */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_gmaps.php";
	
	/* vc_tta_section */
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_tta_section.php";
	include get_template_directory() . "/includes/vc_custom_code/adding_custom_params/acp_vc_tta_tabs.php";
    /* Adding custom parameters files ends here */
  /* Visual composer includes ends here */
}