<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */
add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb_';
if ( class_exists( 'RevSlider' ) )
{
	 $rev_slider = new RevSlider();
     $sliders = $rev_slider->getAllSliderAliases();
     if($sliders == "")
     {
     	$sliders[] = "No slider";
     }
}
else
{
 	$sliders[] = "No slider";	
}
	
	$meta_boxes[] = array(
		'id'         => 'services_fields',
		'title'      => esc_html('Services Field', 'yachtsailing'),
		'pages'      => array( 'yachtsailing_services'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		
		'fields' => array(
			array(
                'name' => esc_html('Secondary Title', 'yachtsailing'),
				'desc' => esc_html('Secondary Title', 'yachtsailing'),
				'id'   => $prefix . 'services_sub_title',
				'type' => 'text',
            ), 			
        )
	);
	
	$meta_boxes[] = array(
		'id'         => 'event_fields',
		'title'      => esc_html('Event Fields', 'yachtsailing'),
		'pages'      => array( 'yachtsailing_event'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
            array(
                'name' => esc_html('Event City', 'yachtsailing'),
				'desc' => esc_html('Enter Event City', 'yachtsailing'),
				'id'   => $prefix . 'event_city',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Event Date', 'yachtsailing'),
				'desc' => esc_html('Enter Event Date', 'yachtsailing'),
				'id'   => $prefix . 'event_date',
				'type' => 'text_datetime_timestamp',
            ),
			array(
                'name' => esc_html('Event Duration', 'yachtsailing'),
				'desc' => esc_html('Enter Event Duration', 'yachtsailing'),
				'id'   => $prefix . 'event_duration',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Event Address', 'yachtsailing'),
				'desc' => esc_html('Enter Event Address', 'yachtsailing'),
				'id'   => $prefix . 'event_address',
				'type' => 'textarea',
            ),
        )
	);
	$meta_boxes[] = array(
		'id'         => 'featured_image_container',
		'title'      => esc_html('Featured Image', 'yachtsailing'),
		'pages'      => array('post','yachtsailing_team','yachtsailing_testi'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => esc_html('Featured Image', 'yachtsailing'),
				'desc' => esc_html('Featured Image', 'yachtsailing'),
				'id'   => $prefix . 'featured_image',
				'type' => 'file'            ),                 
        )
	);
	$meta_boxes[] = array(
		'id'         => 'header_images',
		'title'      => esc_html('Header Image, Slider  Selection', 'yachtsailing'),
		'pages'      => array('page'), // Post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/template-home.php' ),		
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => esc_html('Slider, Image ?', 'yachtsailing'),
				'desc' => esc_html('Choose Slider or Image to Show as Main banner', 'yachtsailing'),
				'id'   => $prefix . 'header-slider_image_video',
				'type'    => 'select',
				'options' => array(
					'image' => __( 'Image', 'yachtsailing' ),
					'slider'   => __( 'Slider', 'yachtsailing' )
                ),
				'default' => 'slider',
            ),  
  	        
			array(
                'name' => esc_html('Header Image', 'yachtsailing'),
				'desc' => esc_html('Header Image', 'yachtsailing'),
				'id'   => $prefix . 'header_image',
				'type' => 'file'
            ),                 
						 
			array(
                'name' => esc_html('Select Slider', 'yachtsailing'),
				'desc' => esc_html('Choose Revolution Slider to Show as Main banner', 'yachtsailing'),
				'id'   => $prefix . 'header-slider',
				'type'    => 'select',
				'options' => $sliders,
				'default' => 'None',
            )                 
			
        )
	);
	
	$meta_boxes[] = array(
		'id'         => 'header_footer',
		'title'      => esc_html('Header  Setting', 'yachtsailing'),
		'pages'      => array('page','post','yachtsailing_testi','yachtsailing_event','yachtsailing_services','yachtsailing_team','yachtsailing_offer','product'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => esc_html('Select Header Type', 'yachtsailing'),
				'desc' => esc_html('Select Header Type. For each page this effect would only work if [Home Header Type] in Theme options is selected to [Theme defaults]. ', 'yachtsailing'),
				'id'   => $prefix . 'header_type',
				'type'    => 'select',
				'options' => array(
					'header_default' => __( 'Header Default', 'yachtsailing' ),
					'header_first' => __( 'Header First', 'yachtsailing' ),
					'header_second'   => __( 'Header Second', 'yachtsailing' ),
					'header_third'   => __( 'Header Third', 'yachtsailing' ),
					'header_fourth'   => __( 'Header Fourth', 'yachtsailing' ),
					'header_fifth'   => __( 'Header Fifth', 'yachtsailing' ),
					'header_sixth'   => __( 'Header Sixth', 'yachtsailing' ),
					'header_inner'   => __( 'Header Inner', 'yachtsailing' ),
				),
				'default' => 'header_inner',
            ),   
        )
	);
		
	$meta_boxes[] = array(
		'id'         => 'page_layout_settings',
		'title'      => esc_html('Page Layout Setting', 'yachtsailing'),
		'pages'      => array('page','post','yachtsailing_testi','yachtsailing_event','yachtsailing_services','yachtsailing_team','yachtsailing_offer','product'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => esc_html('Page Layout Setting', 'yachtsailing'),
				'desc' => esc_html('Page Layout Setting', 'yachtsailing'),
				'id'   => $prefix . 'page_layout',
				'type'    => 'select',
				'options' => array(
					'page_fullwidth' => __( 'Fullwidth Layout', 'yachtsailing' ),
					'page_boxed'   => __( 'Boxed Layout', 'yachtsailing' )
				),
            )
        )
	);
		
		
	$meta_boxes[] = array(
		'id'         => 'destination_content',
		'title'      => esc_html('Destination Listing page Settings', 'yachtsailing'),
		'pages'      => array('yachtsailing_desti'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(		  
			array(
                'name' => esc_html('Featured Image', 'yachtsailing'),
				'desc' => esc_html('Listing Page Featured Image', 'yachtsailing'),
				'id'   => $prefix . 'destination_image',
				'type'    => 'file'
            ),   
	array(
                'name' => esc_html__('Short Content', 'yachtsailing'),
				'desc' => esc_html__('Listing Page Short Content', 'yachtsailing'),
				'id'   => $prefix . 'destination_short_content',
				'type'    => 'wysiwyg'
            ),              
        )
	);
	
		
	$meta_boxes[] = array(
		'id'         => 'charter_fields',
		'title'      => esc_html('Charter Listing page Settings', 'yachtsailing'),
		'pages'      => array('yachtsailing_charter'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
            array(
                'name' => esc_html('Charter type', 'yachtsailing'),
				'desc' => esc_html('Charter type', 'yachtsailing'),
				'id'   => $prefix . 'charter_type',
				'type' => 'text',
            ),   
            array(
                'name' => esc_html('Charter subtitle', 'yachtsailing'),
				'desc' => esc_html('Charter subtitle', 'yachtsailing'),
				'id'   => $prefix . 'charter_subtitle',
				'type' => 'text',
            ),   
            array(
                'name' => esc_html('Berths', 'yachtsailing'),
				'desc' => esc_html('Berths', 'yachtsailing'),
				'id'   => $prefix . 'charter_berths',
				'type' => 'text',
            ),   
            array(
                'name' => esc_html('Price from', 'yachtsailing'),
				'desc' => esc_html('Price from in Doller ($)', 'yachtsailing'),
				'id'   => $prefix . 'price_from',
				'type' => 'text',
            ),   
			array(
                'name' => esc_html('Featured Image', 'yachtsailing'),
				'desc' => esc_html('Listing Page Featured Image', 'yachtsailing'),
				'id'   => $prefix . 'charter_image',
				'type'    => 'file'
            ),   			
        )
	);
	
	$meta_boxes[] = array(
		'id'         => 'testimonials_fields',
		'title'      => esc_html('Testimonials Field', 'yachtsailing'),
		'pages'      => array( 'yachtsailing_testi'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
         
            array(
                'name' => esc_html('Put Designation', 'yachtsailing'),
				'desc' => esc_html('Put Designation', 'yachtsailing'),
				'id'   => $prefix . 'testimonials_designation',
				'type' => 'text',
            ),   
			array(
                'name' => esc_html('Rating', 'yachtsailing'),
				'desc' => esc_html('Select Rating for current Testimonial.', 'yachtsailing'),
				'id'   => $prefix . 'rating_number',
				'type'    => 'select',
				'options' => array(
					'select_rating' => __( 'Select Rating', 'yachtsailing' ),
					'1' => __( '1', 'yachtsailing' ),
					'2'   => __( '2', 'yachtsailing' ),
					'3'   => __( '3', 'yachtsailing' ),					
					'4'   => __( '4', 'yachtsailing' ),
					'5'   => __( '5', 'yachtsailing' )
				),
				'default' => '',
            ),  
			
			    
        ) 
	);
	
	$meta_boxes[] = array(
		'id'         => 'team_fields',
		'title'      => esc_html('Team Field', 'yachtsailing'),
		'pages'      => array( 'yachtsailing_team'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
         
            array(
                'name' => esc_html('Designation', 'yachtsailing'),
				'desc' => esc_html('Designation', 'yachtsailing'),
				'id'   => $prefix . 'team_designation',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Facebook Link', 'yachtsailing'),
				'desc' => esc_html('Facebook Link', 'yachtsailing'),
				'id'   => $prefix . 'team_fbook',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Twitter Link', 'yachtsailing'),
				'desc' => esc_html('Twitter Link', 'yachtsailing'),
				'id'   => $prefix . 'team_twitter',
				'type' => 'text',
            ),
			
			array(
                'name' => esc_html('LinkedIn Link', 'yachtsailing'),
				'desc' => esc_html('LinkedIn Link', 'yachtsailing'),
				'id'   => $prefix . 'team_linkedlink',
				'type' => 'text',
            ),
			    
        ) 
	);
	$meta_boxes[] = array(
		'id'         => 'product_link',
		'title'      => esc_html('Product Link', 'yachtsailing'),
		'pages'      => array( 'yachtsailing_offer'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => esc_html('Product Link', 'yachtsailing'),
				'desc' => esc_html('Product Link', 'yachtsailing'),
				'id'   => $prefix . 'product_link',
				'type' => 'text',
            ),
        )
	);
	$meta_boxes[] = array(
		'id'         => 'technical_specification',
		'title'      => esc_html('Technical Specifications', 'yachtsailing'),
		'pages'      => array( 'product'), // Post type
		'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true, // Show field names on the left
		//'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
                'name' => esc_html('Design', 'yachtsailing'),
				'desc' => esc_html('Design', 'yachtsailing'),
				'id'   => $prefix . 'product_design',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Displacement', 'yachtsailing'),
				'desc' => esc_html('Displacement', 'yachtsailing'),
				'id'   => $prefix . 'product_displacement',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Bore', 'yachtsailing'),
				'desc' => esc_html('Bore', 'yachtsailing'),
				'id'   => $prefix . 'product_bore',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Stroke', 'yachtsailing'),
				'desc' => esc_html('Stroke', 'yachtsailing'),
				'id'   => $prefix . 'product_stroke',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Performance', 'yachtsailing'),
				'desc' => esc_html('Performance', 'yachtsailing'),
				'id'   => $prefix . 'product_performance',
				'type' => 'text',
            ),
			array(
                'name' => esc_html('Cold Start Device', 'yachtsailing'),
				'desc' => esc_html('Cold Start Device', 'yachtsailing'),
				'id'   => $prefix . 'product_cold_start_device',
				'type' => 'text',
            ),
        )
	);
	// Add other metaboxes as needed
	return $meta_boxes;
}
add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {
	if ( ! class_exists( 'cmb_Meta_Box' ) )
		include get_template_directory() . "/includes/custom-metafield/init.php";
}