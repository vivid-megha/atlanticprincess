<?php 
/***Post Tab widget**/
class yachtsailing_post_Widget extends WP_Widget {
    /** constructor */
    function __construct() {
        parent::__construct( false, 'Yacht sailing Post Tab Widget' );
    }
    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        global $posttypes;
        $title = apply_filters('widget_title', $instance['title']);
		if($instance['popular']!=''){
			$popular = apply_filters('widget_title', $instance['popular']);
		}else{
			$popular = 'Popular';
		}
		if($instance['recent']!=''){
			$recent = apply_filters('widget_title', $instance['recent']);
		}else{
			$recent = 'Recent';
		}
		if($instance['number']!=''){
			$number = apply_filters('widget_title', $instance['number']);
		}else{
			$number = '4';
		}
		echo $before_widget;
        if ( $title ) ?>
            <div class="aside-tabs">
                <div class="aside-tabs__links">
                    <a href="#" class="no-decoration aside-tabs__active-link js-tab-link" data-for='#block1'><?php echo $popular; ?></a>
                    <a href="#" class="no-decoration js-tab-link" data-for='#block2'><?php echo $recent; ?></a>
                </div>
                <div class="aside-tabs__blocks js-tab-block" id="block1">
                    <?php
                    $popular = new WP_Query( array( 'posts_per_page' => $number, 'meta_key' => 'post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
                    while ( $popular->have_posts() ) : $popular->the_post();
                    $thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id($popular->ID), 'yachtsailing-blog-sidebar'); 
                    ?>
                        <div class="aside-tabs__block">
                            <div class="row row--no-padding">
                                <div class="col-xs-4"><img src="<?php echo esc_url($thumbnail_url[0], 'yachtsailing'); ?>" alt="<?php the_title(); ?>" /></div>
                                <div class="col-xs-7">
                                    <div class="aside-tabs__anons">
                                    <p><a href="<?php the_permalink(); ?>" class="no-decoration"><?php the_title(); ?></a></p>
                                    <div class="aside-tabs__date"><?php echo get_the_date('F j, Y', $popular->ID); ?></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="aside-tabs__blocks js-tab-block" id="block2">
                    <?php
                    global $post;
                    $tmp_post = $post;
                    // get the category IDs and place them in an array
                    $args = array( 'numberposts' =>$number, 'orderby' => 'post_date', 'order' => 'DESC', 'post_type' => 'post' );
                    $recents = wp_get_recent_posts( $args );
                    foreach( $recents as $recent ) :
                    $thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id($recent['ID']),'yachtsailing-blog-sidebar'); 
                    ?>
                        <div class="aside-tabs__block">
                            <div class="row row--no-padding">
                                <div class="col-xs-4"><img src="<?php echo esc_url($thumbnail_url[0], 'yachtsailing'); ?>" alt="<?php echo $recent['post_title']; ?>" /></div>
                                <div class="col-xs-7">
                                    <div class="aside-tabs__anons">
                                    <p><a href="<?php the_permalink($recent['ID']); ?>" class="no-decoration"><?php echo $recent['post_title']; ?></a></p>
                                    <div class="aside-tabs__date"><?php echo get_the_date('F j, Y', $recent['ID']); ?></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php $post = $tmp_post; ?>
                </div>
           </div>
        <?php echo $after_widget;
    }
    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
        global $posttypes;
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
		$instance['popular'] = strip_tags($new_instance['popular']);
		$instance['recent'] = strip_tags($new_instance['recent']);
        $instance['number'] = strip_tags($new_instance['number']);
         $instance['posttype'] = $new_instance['posttype'];
        return $instance;
    }
    /** @see WP_Widget::form */
    function form($instance) {
        $posttypes = get_post_types('', 'objects');
        $title =  esc_attr($instance['title'], 'yachtsailing');
		$popular =  esc_attr($instance['popular'], 'yachtsailing');
		$recent =  esc_attr($instance['recent'], 'yachtsailing');
        $number = esc_attr($instance['number'], 'yachtsailing');
       	$posttype   = esc_attr($instance['posttype'], 'yachtsailing');
        ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo sanitize_text_field( $title );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('popular'); ?>"><?php esc_html_e('Popular:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('popular'); ?>" name="<?php echo $this->get_field_name('popular'); ?>" type="text" value="<?php echo sanitize_text_field( $popular );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('recent'); ?>"><?php esc_html_e('Recent:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('recent'); ?>" name="<?php echo $this->get_field_name('recent'); ?>" type="text" value="<?php echo sanitize_text_field( $recent );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php esc_html_e('Number to Show Post:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo sanitize_text_field( $number ); ?>" />
        </p>
    
         <input  type="hidden" name="<?php echo $this->get_field_name('posttype'); ?>" value="<?php esc_html_e('post','yachtsailing')?>"/>
       
        <?php
    }
} 
function yachtsailing_recent_post_thumb_widget() {
    register_widget( 'yachtsailing_post_Widget' );
} 
add_action( 'widgets_init', 'yachtsailing_recent_post_thumb_widget' );
/***About Club widget**/
class about_club_Widget extends WP_Widget {
    /** constructor */
    function __construct() {
        parent::__construct( false, 'About Club Widget' );
    }
    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        global $posttypes;
        $title = apply_filters('widget_title', $instance['title']);
		$content = apply_filters('widget_title', $instance['content']);
		$fbook_link = apply_filters('widget_title', $instance['fbook_link']);
		$twitter_link = apply_filters('widget_title', $instance['twitter_link']);
		$gplus_link = apply_filters('widget_title', $instance['gplus_link']);
		echo $before_widget;
        if ( $title )
			echo $before_title . $title . $after_title; ?>
            <div class="blog-aside__about">
            	<p class="blog-text"><?php echo $content; ?></p>
                <?php if($fbook_link != '' || $twitter_link != '' || $gplus_link != '' ) : ?>
                <div class="social-blocks">
                	<div class="row row--no-padding">
                    	<div class="col-xs-4">
                        	<div class="social-blocks__one social-blocks__one--fb">
                            	<div class="social-blocks__logo">
                                	<span class="fa fa-facebook"></span>
                                </div>
                                <a href="<?php echo $fbook_link; ?>" class="social-blocks__action no-decoration"><?php esc_html_e('Like','yachtsailing'); ?></a>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="social-blocks__one social-blocks__one--twitter">
                                <div class="social-blocks__logo">
                                    <span class="fa fa-twitter"></span>
                                </div>
                                <a href="<?php echo $twitter_link; ?>" class="social-blocks__action no-decoration"><?php esc_html_e('Follow','yachtsailing'); ?></a>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="social-blocks__one social-blocks__one--google">
                                <div class="social-blocks__logo">
                                    <span class="fa fa-google-plus"></span>
                                </div>
                                <a href="<?php echo $gplus_link; ?>" class="social-blocks__action no-decoration"><?php esc_html_e('ADD','yachtsailing'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
			</div>
        <?php echo $after_widget;
    }
    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
        global $posttypes;
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
		$instance['content'] = strip_tags($new_instance['content']);
		$instance['fbook_link'] = strip_tags($new_instance['fbook_link']);
        $instance['twitter_link'] = strip_tags($new_instance['twitter_link']);
		$instance['gplus_link'] = strip_tags($new_instance['gplus_link']);
        $instance['posttype'] = $new_instance['posttype'];
        return $instance;
    }
    /** @see WP_Widget::form */
    function form($instance) {
        $posttypes = get_post_types('', 'objects');
        $title =  esc_attr($instance['title'], 'yachtsailing');
		$content =  esc_attr($instance['content'], 'yachtsailing');
		$fbook_link =  esc_attr($instance['fbook_link'], 'yachtsailing');
		$twitter_link =  esc_attr($instance['twitter_link'], 'yachtsailing');
        $gplus_link = esc_attr($instance['gplus_link'], 'yachtsailing');
       	$posttype   = esc_attr($instance['posttype'], 'yachtsailing');
        ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo sanitize_text_field( $title );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('content'); ?>"><?php esc_html_e('Content:','yachtsailing'); ?></label>
          <textarea class="widefat" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name('content'); ?>"><?php echo sanitize_text_field( $content );?></textarea>
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('fbook_link'); ?>"><?php esc_html_e('Facebook Link:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('fbook_link'); ?>" name="<?php echo $this->get_field_name('fbook_link'); ?>" type="text" value="<?php echo sanitize_text_field( $fbook_link );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('twitter_link'); ?>"><?php esc_html_e('Twitter Link:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('twitter_link'); ?>" name="<?php echo $this->get_field_name('twitter_link'); ?>" type="text" value="<?php echo sanitize_text_field( $twitter_link );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('gplus_link'); ?>"><?php esc_html_e('Google+ Link:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('gplus_link'); ?>" name="<?php echo $this->get_field_name('gplus_link'); ?>" type="text" value="<?php echo sanitize_text_field( $gplus_link ); ?>" />
        </p>
    
         <input  type="hidden" name="<?php echo $this->get_field_name('posttype'); ?>" value="<?php esc_html_e('post','yachtsailing')?>"/>
       
        <?php
    }
} 
function yachtsailing_about_club_widget() {
    register_widget( 'about_club_Widget' );
} 
add_action( 'widgets_init', 'yachtsailing_about_club_widget' );
/***Upcoming Event widget**/
class yachtsailing_event_Widget extends WP_Widget { 
    /** constructor */
    function __construct() {
        parent::__construct( false, 'Yacht sailing Upcoming Event Widget' );
    }
    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        global $posttypes;
        $title = apply_filters('widget_title', $instance['title']);
		if($instance['number']!=''){
			$number = apply_filters('widget_title', $instance['number']);
		}else{
			$number = '3';
		}
		echo $before_widget;
        if ( $title ) ?>
			<div class="footer-main-home__block equal-height-item footer-main-home__block--margin">
				<?php
				echo $before_title . $title . $after_title;
                $events = wp_get_recent_posts( array( 'numberposts' => $number, 'category' => 0, 'meta_query' => array( array( 'key' => '_cmb_event_date', 'value' => current_time('timestamp'), 'compare' => '>=', 'type' => 'numeric' ) ), 'orderby' => 'meta_value_num', 'order' => 'ASC', 'post_type' => 'yachtsailing_event' ) );
                foreach( $events as $event ) :
				$date = get_post_meta($event['ID'],"_cmb_event_date",true);
				$duration = get_post_meta($event['ID'],"_cmb_event_duration",true);
                ?>
                    <div class="footer-main-home__event clearfix">
                    	<div class="event-num triangle pull-right"><?php echo date('j', $date); ?></div>
                        <div class="footer-main-home__event-text">
                            <h3><a class="no-decoration" href="<?php echo esc_url(get_permalink($event['ID']), 'yachtsailing');?>"><?php echo $event['post_title']; ?></a></h3>
                            <p class="text text--footer-main-home"><?php echo $duration . '. Starting ' . date('F d, Y', $date); ?></p> 
                        </div>
                    </div>
                <?php endforeach; ?>
			</div>
        <?php echo $after_widget;
    }
    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
        global $posttypes;
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
         $instance['posttype'] = $new_instance['posttype'];
        return $instance;
    }
    /** @see WP_Widget::form */
    function form($instance) {
        $posttypes = get_post_types('', 'objects');
        $title =  esc_attr($instance['title'], 'yachtsailing');
        $number = esc_attr($instance['number'], 'yachtsailing');
       	$posttype   = esc_attr($instance['posttype'], 'yachtsailing');
        ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo sanitize_text_field( $title );?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php esc_html_e('Number to Show Post:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo sanitize_text_field( $number ); ?>" />
        </p>
    
         <input  type="hidden" name="<?php echo $this->get_field_name('posttype'); ?>" value="<?php esc_html_e('post','yachtsailing'); ?>"/>
       
        <?php
    }
} 
function yachtsailing_upcoming_event_widget() {
    register_widget( 'yachtsailing_event_Widget' );
} 
add_action( 'widgets_init', 'yachtsailing_upcoming_event_widget' );