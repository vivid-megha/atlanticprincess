<?php
/* 
  destinations adding parameters starts here
*/  
	$attributes = array(
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Content Position",
								"param_name" => "content_position",
								"value" => array(
								                  "Default" => "default_styling",
								                  "All Images in Left side" => "images_left",
								                  "All Images in Right side" => "images_right",
											     )
                             ),
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Custom Post type 'Destinations' selected automatically",
								"param_name" => "destinations_post_type",
								"value" => array(
								                  "Destinations" => "yachtsailing_desti",
											     )
                             ),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Number of Destinations to Show",
							'param_name' => 'number_posts',
							'value' => "",
                            'description' => esc_html( "Enter number of Destinations to show for ex (1) or (10) or Enter value (-1) to show all Destinations", "yachtsailing" )							
						),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting of Destinations by",
								"param_name" => "orderby",
								"value" => array(
								                "Published Date" => "date",
								                "Charter Title" => "title",
								                "Modified Date" => "modified",
								                "Random" => "rand",
											   )
                                  ),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting order",
								"param_name" => "order",
								"value" => array(
								                "Descending" => "DESC",
								                "Ascending" => "ASC",
											   )
                                  ),
					);
	vc_add_params( 'destinations', $attributes ); 
	
/* destinations adding parameters ends here  */
?>