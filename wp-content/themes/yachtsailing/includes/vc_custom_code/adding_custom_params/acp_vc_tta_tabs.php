<?php
/* 
  vc_tta_tabs adding parameters starts here
  Dropdown for container div
  Tabs background color
  Active tab background color
  Tabs border color
  Tabs border size
  Tabs border style
*/  
	$attributes = array(
						array(
							     "type" => "dropdown",
							     "group" => "Extra Tab Options",
							     "heading" => "Container Type",
								 "param_name" => "type",
								 "value" => array(
													 "In Container" => "container",
													 "Full Width" => "container-fluid"
												  )
						      ),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'colorpicker',
							'heading' => "Tabs Font Color",
							'param_name' => 'tab_font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color for Tabs", "yachtsailing" )
						),						
						array(
							"group" => "Extra Tab Options",	
							'type' => 'colorpicker',
							'heading' => "Active Tabs Font Color",
							'param_name' => 'active_tab_font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color for Tabs", "yachtsailing" )
						),								
						array(
							"group" => "Extra Tab Options",	
							'type' => 'colorpicker',
							'heading' => "Tab Background Color",
							'param_name' => 'tab_background_color',
							'value' => "",
                            'description' => esc_html( "Select Background Color for Tabs", "yachtsailing" )
						),							   							
						array(
							"group" => "Extra Tab Options",	
							'type' => 'colorpicker',
							'heading' => "Active Tab Background Color",
							'param_name' => 'active_tab_background_color',
							'value' => "",
                            'description' => esc_html( "Select Background Color for Active Tabs", "yachtsailing" )
						),							   							
						array(
							"group" => "Extra Tab Options",	
							'type' => 'colorpicker',
							'heading' => "Tabs Border Color",
							'param_name' => 'tab_border_color',
							'value' => "",
                            'description' => esc_html( "Select Border Color for Tabs", "yachtsailing" )
						),							   							
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Border Size in px",
							'param_name' => 'tab_border_size',
							'value' => "",
                            'description' => esc_html( "Enter Border Size in pixel for ex. 15 or 3.2 (but not include px) for Tabs", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
								"group" => "Extra Tab Options",
								"heading" => "Tab Border Style",
								"param_name" => "tab_border_style",
								"value" => array(
													 "Solid" => "solid",
													 "None" => "none",
													 "Dotted" => "dotted",
													 "Dashed" => "dashed",
													 "Inset" => "inset",
													 "Outset" => "outset",
													 "Dashed" => "dashed",
													 "Inset" => "inset",
													 "Double" => "double",
													 "Hidden" => "hidden",
													 "Ridge" => "ridge",
													 "Hidden" => "hidden",
													 "Groove" => "groove"
												)
                                  ),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Padding Top",
							'param_name' => 'tabs_padding_top',
							'value' => "",
                            'description' => esc_html( "Enter Padding Top Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Padding Bottom",
							'param_name' => 'tabs_padding_bottom',
							'value' => "",
                            'description' => esc_html( "Enter Padding Bottom Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Padding Left",
							'param_name' => 'tabs_padding_left',
							'value' => "",
                            'description' => esc_html( "Enter Padding Left Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Padding Right",
							'param_name' => 'tabs_padding_right',
							'value' => "",
                            'description' => esc_html( "Enter Padding Right Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Margin Top",
							'param_name' => 'tabs_margin_top',
							'value' => "",
                            'description' => esc_html( "Enter Margin Top Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Margin Bottom",
							'param_name' => 'tabs_margin_bottom',
							'value' => "",
                            'description' => esc_html( "Enter Margin Bottom Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Margin Left",
							'param_name' => 'tabs_margin_left',
							'value' => "",
                            'description' => esc_html( "Enter Margin Left Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tabs Padding Right",
							'param_name' => 'tabs_margin_right',
							'value' => "",
                            'description' => esc_html( "Enter Padding Right Size arround tab text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tab Text Font Size in px",
							'param_name' => 'tab_font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
							),								  							
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tab Text Letter spacing in px",
							'param_name' => 'tab_letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
						array(
							"group" => "Extra Tab Options",	
							'type' => 'textfield',
							'heading' => "Tab Text Line Height",
							'param_name' => 'tab_line_height',
							'value' => "",
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
/* Icon options starts here */						
						array(
							     "type" => "dropdown",
							     "group" => "Icon Options",
							     "heading" => "Icon Font Size",
								 "param_name" => "icon_position",
								 "value" => array(
													 "Left" => "left",
													 "Right" => "right",
													 "Top" => "top",
													 "Bottom" => "bottom",
												  ),
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )																			  						      ),
						array(
							"group" => "Icon Options",	
							'type' => 'iconpicker',
							'heading' => "Icon Picker",
							'param_name' => 'iconpicker',
							'value' => "",
                'description' => esc_html( "Select icon. Note: If Material icon is empty then Icon picker will automatically Disabled.", "yachtsailing" )
							),
							
						array(
							"group" => "Icon Options",	
							'type' => 'textfield',
							'heading' => "Material Icon",
							'param_name' => 'material_icon_shortcode',
							'value' => "",
               'description' => esc_html( "Enter Material icon shortcode for ex(perm_phone_msg). See https://design.google.com/icons/", "yachtsailing" )							
							),
					    array(
							"group" => "Icon Options",	
							'type' => 'textfield',
							'heading' => "Padding Top",
							'param_name' => 'icon_padding_top',
							'value' => "",
             'description' => esc_html( "Enter Padding Top Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
					 array(
							"group" => "Icon Options",	
							'type' => 'textfield',
							'heading' => "Padding bottom",
							'param_name' => 'icon_padding_bottom',
							'value' => "",
           'description' => esc_html( "Enter Padding bottom Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
				     array(
							"group" => "Icon Options",	
							'type' => 'textfield',
							'heading' => "Padding left",
							'param_name' => 'icon_padding_left',
							'value' => "",
              'description' => esc_html( "Enter Padding left Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
					array(
					        "group" => "Icon Options",	
							'type' => 'textfield',
							'heading' => "Padding right",
							'param_name' => 'icon_padding_right',
							'value' => "",
                            'description' => esc_html( "Enter Padding right Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
					array(
					        "group" => "Icon Options",	
							'type' => 'textfield',
							'heading' => "Icon Size",
							'param_name' => 'icon_size',
							'value' => "",
                            'description' => esc_html( "Enter Icon Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
/* Icon options ends here */						
/* Tab title options starts here */
          array(
								"type" => "dropdown",
								"group" => "Extra Tab Options",
								"heading" => "Text Transform",
								"param_name" => "text_transform",
								"value" => array(
													 "Unset" => "unset",
													 "Upper Case" => "uppercase",
													 "Lower Case" => "lowercase",
													 "Capitalize" => "capitalize",
													 "Inherit" => "inherit",
													 "Initial" => "initial",
													 "Fullwidth" => "full-width",
													 "None" => "none"
												)
                             ),
                        array(
								"type" => "dropdown",
								"group" => "Extra Tab Options",
								"heading" => "Font weight",
								"param_name" => "font_weight",
								"value" => array(
													 "normal" => "normal",
													 "100" => "100",
													 "200" => "200",
													 "300" => "300",
													 "400" => "400",
													 "500" => "500",
													 "600" => "600",
													 "700" => "700",
													 "800" => "800",
													 "900" => "900",
													 "bold" => "bold",
													 "bolder" => "bolder",
													 "lighter" => "lighter",													
													 "inherit" => "inherit",
													 "initial" => "initial",
													 "unset" => "unset"
												)
                                  )							
/* Tab title options ends here */
						
						); 
	vc_add_params( 'vc_tta_tabs', $attributes ); 
/* vc_tta_tabs adding parameters ends here  */
?>