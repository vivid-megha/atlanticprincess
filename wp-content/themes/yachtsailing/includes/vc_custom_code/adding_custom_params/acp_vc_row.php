<?php
/* 
 vc_row adding parameters starts here  
 Background overlay
 Overlay opacity	
*/
	$attributes = array(
	 array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Container Type",
								"param_name" => "type",
								"value" => array(
													 "In Container" => "container",
													 "Full Width" => "container-fluid"
												)
                                  ),						
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Overlay Opacity",
							'param_name' => 'overlay_opacity',
							'value' => ".85",
                            'description' => esc_html( "Select Background Overlay Opacity Color", "yachtsailing" )
						     ),
						 array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Background Attachment",
								"param_name" => "background_attachment",
								"value" => array(
													 "Theme Default" => "",								
													 "Scroll" => "scroll",								
													 "Fixed" => "fixed",
													 "Local" => "local",
													 "Inherit" => "inherit"													 
												)
                             )
					   );
	vc_add_params( 'vc_row', $attributes ); 
/* vc_btn adding parameters ends here */
?>