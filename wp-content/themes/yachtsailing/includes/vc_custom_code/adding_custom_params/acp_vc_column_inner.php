<?php
/* 
 vc_btn adding parameters starts here  
	Border color
	Border size
	Border style
*/
	$attributes = array(
						array(
							"group" => "Extra Design Options",	
							'type' => 'colorpicker',
							'heading' => "Border Color",
							'param_name' => 'border_color',
							'value' => "",
                            'description' => esc_html( "Select Border Color", "yachtsailing" )
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Border Size in px",
							'param_name' => 'border_size',
							'value' => "",
                            'description' => esc_html( "Enter Border Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Border Style",
								"param_name" => "border_style",
								"value" => array(
													 "None" => "none",
													 "Solid" => "solid",
													 "Dotted" => "dotted",
													 "Dashed" => "dashed",
													 "Inset" => "inset",
													 "Outset" => "outset",
													 "Dashed" => "dashed",
													 "Inset" => "inset",
													 "Double" => "double",
													 "Hidden" => "hidden",
													 "Ridge" => "ridge",
													 "Hidden" => "hidden",
													 "Groove" => "groove"
												)
                                  ),
					);
	vc_add_params( 'vc_column_inner', $attributes ); 
/* vc_btn adding parameters ends here */
?>