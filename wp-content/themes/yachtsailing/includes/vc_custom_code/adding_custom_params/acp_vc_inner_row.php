<?php
/* 
 vc_row_inner adding parameters starts here 
  Dropdown for container div
*/
vc_add_param("vc_row_inner", array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Container Type",
								"param_name" => "type",
								"value" => array(
													 "In Container" => "container",
													 "Full Width" => "container-fluid"
												)
                                  )
		  );
/* vc_row_inner adding parameters ends here */
?>