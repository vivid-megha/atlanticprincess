<?php
/* 
  charters adding parameters starts here
*/  
	$attributes = array(
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Custom Post type 'Charters' selected automatically",
								"param_name" => "yachtsailing_charter_post_type",
								"value" => array(
								                  "Charters" => "yachtsailing_charter",
											     )
                             ),
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Charters List style",
								"param_name" => "charter_styling",
								"value" => array(
								                  "Default" => "default_style",
								                  "Carousel" => "yachtsailing_charter",
											     )
                             ),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Number of Charters to Show",
							'param_name' => 'number_posts',
							'value' => "",
                            'description' => esc_html( "Enter number of Charters to show for ex (1) or (10) or Enter value (-1) to show all Charters", "yachtsailing" )							
						),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting of Charters by",
								"param_name" => "orderby",
								"value" => array(
								                "Published Date" => "date",
								                "Charter Title" => "title",
								                "Modified Date" => "modified",
								                "Random" => "rand",
											   )
                                  ),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting order",
								"param_name" => "order",
								"value" => array(
								                "Descending" => "DESC",
								                "Ascending" => "ASC",
											   )
                                  ),
					);
	vc_add_params( 'charters', $attributes ); 
	
/* charters adding parameters ends here  */
?>