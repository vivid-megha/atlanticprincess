<?php
/* 
  testimonialowlcarousel adding parameters starts here
*/  
	$attributes = array(
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Custom Post type 'Testimonials' selected automatically",
								"param_name" => "testimonials_post_type",
								"value" => array(
								                  "Testimonial" => "yachtsailing_testi",
											     )
                             ),
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Content Styling",
								"param_name" => "content_styling",
								"value" => array(
								                  "Slider" => "slider",
								                  "Listing" => "listing",
											     )
                             ),
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Rating Star Visibility",
								"param_name" => "rating_visibility",
								"value" => array(
								                  "Yes" => "yes",
								                  "No" => "no",
											     )
                             ),
							 
							  array(
								"type" => "...",
								"group" => "Extra Design Options",
								"heading" => "QUERY",
								"param_name" => "post_query",
								"value" => ""
                             ),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Number of Testimonials to Show",
							'param_name' => 'number_posts',
							'value' => "",
                            'description' => esc_html( "Enter number of Testimonials to show for ex (1) or (10) or Enter value (-1) to show all Testimonials", "yachtsailing" )							
						),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting of Testimonials by",
								"param_name" => "orderby",
								"value" => array(
								                "Published Date" => "date",
								                "Member Title" => "title",
								                "Modified Date" => "modified",
								                "Random" => "rand",
											   )
                                  ),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting order",
								"param_name" => "order",
								"value" => array(
								                "Descending" => "DESC",
								                "Ascending" => "ASC",
											   )
                                  ),
					);
	vc_add_params( 'testimonialowlcarousel', $attributes ); 
	
/* testimonialowlcarousel adding parameters ends here  */
?>