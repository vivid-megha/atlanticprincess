<?php
/* 
 vc_btn adding parameters starts here  
	Border color
	Border size
	Border style
*/
	$attributes = array(
                        array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Type of Image",
								"param_name" => "type_of_image",
								"value" => array(
													 "Default" => "default",
													 "Background image" => "section_background_image"
												)
                                  )
								 );
	vc_add_params( 'vc_single_image', $attributes ); 
/* vc_btn adding parameters ends here */
?>