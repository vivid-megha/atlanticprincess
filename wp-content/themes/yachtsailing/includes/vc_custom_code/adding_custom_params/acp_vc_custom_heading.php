<?php
/* 
  vc_custom_heading adding parameters starts here
  Letter spacing
  Text transform
  Font weight
*/  
	$attributes = array(
	/* Heading Icon code starts here */
						array(
							"group" => "Heading Icon",	
							'type' => 'textfield',
							'heading' => "Material Icon",
							'param_name' => 'material_icon_shortcode',
							'value' => "",
                            'description' => esc_html( "Enter Material icon shortcode for ex(perm_phone_msg). See https://design.google.com/icons/", "yachtsailing" )							
							),
			array(
							"group" => "Heading Icon",	
							'type' => 'dropdown',
							'heading' => "Select position of Icon to Show",
							'param_name' => 'icon_position',
							"value" => array(
											 "Before Heading" => "before_heading",
											 "After Heading" => "after_heading",
			                                ),
                            'description' => esc_html( "", "yachtsailing" )							
							),
						array(
							"group" => "Heading Icon",	
							'type' => 'iconpicker',
							'heading' => "Icon Picker",
							'param_name' => 'iconpicker',
							'value' => "",
                            'description' => esc_html( "Select icon. Note: If Material icon is empty then Icon picker will automatically Disabled.", "yachtsailing" )							
							),
     /* Heading icon coede ends here */
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Letter spacing in px",
							'param_name' => 'letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Text Transform",
								"param_name" => "text_transform",
								"value" => array(
													 "Unset" => "unset",
													 "Upper Case" => "uppercase",
													 "Lower Case" => "lowercase",
													 "Capitalize" => "capitalize",
													 "Inherit" => "inherit",
													 "Initial" => "initial",
													 "Fullwidth" => "full-width",
													 "None" => "none"
												)
                             ),
                        array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Font weight",
								"param_name" => "font_weight",
								"value" => array(
													 "normal" => "normal",
													 "100" => "100",
													 "200" => "200",
													 "300" => "300",
													 "400" => "400",
													 "500" => "500",
													 "600" => "600",
													 "700" => "700",
													 "800" => "800",
													 "900" => "900",
													 
													 "bold" => "bold",
													 "bolder" => "bolder",
													 "lighter" => "lighter",													
													 "inherit" => "inherit",
													 "initial" => "initial",
													 "unset" => "unset"
													 
													 
												)
                                  )							
						);
	vc_add_params( 'vc_custom_heading', $attributes ); 
		
/* vc_custom_heading adding parameters ends here  */
?>