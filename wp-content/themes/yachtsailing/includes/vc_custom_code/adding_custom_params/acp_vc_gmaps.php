<?php
/* vc_gmaps adding parameters starts here */  
	$attributes = array(
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "MAP ID",
							'param_name' => 'map_id',
							'value' => "",
                            'description' => esc_html( "Enter ID for current map. Note: ID name should UNIQUE, that was never used on current page.", "yachtsailing" )							
							),
	
                        array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Hide Iframe Code Effect",
								"param_name" => "hide_map",
                            'description' => esc_html( "Hide Iframe Code and show MAP Based on Latitude-Longitude data Entered Below. ", "yachtsailing" ),
								"value" => array(
												 "Select" => "",
												 "Yes" => "yes",
												 "No" => "no",
												)
                                  ),	
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Latitude Data",
							'param_name' => 'latitude_data',
							'value' => "",
                            'description' => esc_html( "Enter Latitude data to show Map.", "yachtsailing" )							
							),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Longitude Data",
							'param_name' => 'longitude_data',
							'value' => "",
                            'description' => esc_html( "Enter Longitude data to show Map.", "yachtsailing" )							
							),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Map Height",
							'param_name' => 'map_height',
							'value' => "",
                            'description' => esc_html( "Enter Height to show for MAP for ex 300, (But not embed 'px' with 300).", "yachtsailing" )							
							),
						);
	vc_add_params( 'vc_gmaps', $attributes ); 
	
/* vc_gmaps adding parameters ends here  */
?>