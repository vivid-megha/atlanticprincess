<?php
/* 
  teamowlcarousel adding parameters starts here
*/  
	$attributes = array(
                        array(
						        "type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Custom Post type 'Team' selected automatically",
								"param_name" => "team_post_type",
								"value" => array(
								                  "Team" => "yachtsailing_team",
											     )
                             ),
							 
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Number of Members to Show",
							'param_name' => 'number_posts',
							'value' => "",
                            'description' => esc_html( "Enter number of Members to show for ex (1) or (10) or Enter value (-1) to show all Team members", "yachtsailing" )							
						),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting of Members by",
								"param_name" => "orderby",
								"value" => array(
								                "Published Date" => "date",
								                "Title" => "title",
								                "Modified Date" => "modified",
								                "Random" => "rand",
											   )
                                  ),
						
                           array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Select sorting order",
								"param_name" => "order",
								"value" => array(
								                "Descending" => "DESC",
								                "Ascending" => "ASC",
											   )
                                  ),
					);
	vc_add_params( 'teamowlcarousel', $attributes ); 
	
/* teamowlcarousel adding parameters ends here  */
?>