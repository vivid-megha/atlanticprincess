<?php
/* 
  vc_tta_section adding parameters starts here  
  Background color
*/
/* vc_tta_section adding parameters starts here */
	$attributes = array(
						array(
							"group" => "Extra Design Options",	
							'type' => 'colorpicker',
							'heading' => "Section Background Color",
							'param_name' => 'background_color',
							'value' => "",
                            'description' => esc_html( "Background Color For Tab Content Section Here", "yachtsailing" )							
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'colorpicker',
							'heading' => "Border Color",
							'param_name' => 'border_color',
							'value' => "",
                            'description' => esc_html( "Select Border Color", "yachtsailing" )
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Border Size in px",
							'param_name' => 'border_size',
							'value' => "",
                            'description' => esc_html( "Enter Border Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
							),
							array(
                                        "group" => "Extra Design Options",	
										'type' => 'google_fonts',
										'param_name' => 'google_fonts',
										'value' => '',										
										'dependency' => array(
											'element' => 'use_theme_fonts',
											'value_not_equal_to' => 'yes',
										),
                            'description' => esc_html( "Select Font Family", "yachtsailing" )										
									),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Top",
							'param_name' => 'padding_top',
							'value' => "",
                            'description' => esc_html( "Enter Padding Top Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Bottom",
							'param_name' => 'padding_bottom',
							'value' => "",
                            'description' => esc_html( "Enter Padding Bottom Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Left",
							'param_name' => 'padding_left',
							'value' => "",
                            'description' => esc_html( "Enter Padding Left Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Right",
							'param_name' => 'padding_right',
							'value' => "",
                            'description' => esc_html( "Enter Padding Right Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						
                   );
	vc_add_params( 'vc_tta_section', $attributes ); 
/* vc_tta_section adding parameters ends here */
?>