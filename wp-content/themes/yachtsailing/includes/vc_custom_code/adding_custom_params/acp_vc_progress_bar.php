<?php
/* 
 vc_progress_bar adding parameters starts here  
    Font size
	Line height
	Border color
	Border size
	Google fonts
	Padding arround link
	Letter spacing
	Font weight
*/
	$attributes = array(
						array(
							"group" => "Bar Text Design Options",	
							'type' => 'textfield',
							'heading' => "Font Size",
							'param_name' => 'font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Bar Text Design Options",	
							'type' => 'colorpicker',
							'heading' => "Font Color",
							'param_name' => 'font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color from Here.", "yachtsailing" )							
						),
						array(
							"group" => "Bar Text Design Options",	
							'type' => 'textfield',
							'heading' => "Line Height",
							'param_name' => 'line_height',
							'value' => "",
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Bar Text Design Options",	
							'type' => 'textfield',
							'heading' => "Letter spacing in px",
							'param_name' => 'letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
								"group" => "Bar Text Design Options",
								"heading" => "Font weight",
								"param_name" => "font_weight",
								"value" => array(
													 "normal" => "normal",
													 "100" => "100",
													 "200" => "200",
													 "300" => "300",
													 "400" => "400",
													 "500" => "500",
													 "600" => "600",
													 "700" => "700",
													 "800" => "800",
													 "900" => "900",
													 
													 "bold" => "bold",
													 "bolder" => "bolder",
													 "lighter" => "lighter",													
													 "inherit" => "inherit",
													 "initial" => "initial",
													 "unset" => "unset"
													 
													 
												)
                                  ),						
						array(
							"group" => "Bar Design Options",	
							'type' => 'colorpicker',
							'heading' => "Bar Background Color",
							'param_name' => 'bar_background_color',
							'value' => "",
                            'description' => esc_html( "Select Bar Background Color Here", "yachtsailing" )							
							),
						array(
							"group" => "Bar Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Top",
							'param_name' => 'padding_top',
							'value' => "",
                            'description' => esc_html( "Enter Padding Top Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Bar Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Bottom",
							'param_name' => 'padding_bottom',
							'value' => "",
                            'description' => esc_html( "Enter Padding Bottom Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Bar Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Left",
							'param_name' => 'padding_left',
							'value' => "",
                            'description' => esc_html( "Enter Padding Left Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
						array(
							"group" => "Bar Design Options",	
							'type' => 'textfield',
							'heading' => "Padding Right",
							'param_name' => 'padding_right',
							'value' => "",
                            'description' => esc_html( "Enter Padding Right Size arround  text or link text in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )
						),
				); 
	vc_add_params( 'vc_progress_bar', $attributes ); 
/* vc_btn adding parameters ends here */
?>