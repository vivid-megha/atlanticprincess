<?php
/* 
  vc_column_text adding parameters starts here
  Font color
  Fon size
  Letter spacing
  Line height
  Text align
*/
	$attributes = array(
						array(
							"group" => "Extra Design Options",	
							'type' => 'colorpicker',
							'heading' => "Font Color",
							'param_name' => 'font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color", "yachtsailing" )														
						),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Font Size in px",
							'param_name' => 'font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
							),								  							
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Letter spacing in px",
							'param_name' => 'letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
						array(
							"group" => "Extra Design Options",	
							'type' => 'textfield',
							'heading' => "Line Height",
							'param_name' => 'line_height',
							'value' => "",
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
                        array(
								"type" => "dropdown",
								"group" => "Extra Design Options",
								"heading" => "Text Align",
								"param_name" => "text_align",
								"value" => array(
													 "Left" => "left",
													 "Center" => "center",
													 "Right" => "right",
													 "Justify" => "justify",
													 "Inherit" => "inherit",
													 "Initial" => "initial",
													 "unset" => "unset",
													 "End" => "end",
													 "Start" => "start"
												)
                             )
						);
	vc_add_params( 'vc_column_text', $attributes ); 
/* vc_column_text adding parameters ends here  */
?>