<?php
add_action( 'vc_before_init', 'blog_listing_integrateWithVC' );
function blog_listing_integrateWithVC() {
   vc_map( array(
      "name" => esc_html( "Blog Posts Listing", "yachtsailing" ),
      "base" => "bloglisting",
      "params" => array(
                        array(
						        "type" => "dropdown",
								"group" => "Design Options",
								"heading" => "Posts display style",
								"param_name" => "post_display_style",
								"value" => array(
								                  "Style 1" => "style_one",
								                  "Style 2" => "style_two",
											     )
                             ),
                        array(
						        "type" => "dropdown",
								"group" => "Design Options",
								"heading" => "Default Post type 'Post' selected automatically",
								"param_name" => "default_post_type",
								"value" => array(
								                  "Post" => "post",
											     )
                             ),
							 
						array(
							"group" => "Design Options",	
							'type' => 'textfield',
							'heading' => "Number of Posts to Show",
							'param_name' => 'number_posts',
							'value' => "",
                            'description' => esc_html( "Enter number of Posts to show for ex (1) or (10) or Enter value (-1) to show all Team posts", "yachtsailing" )							
						),
						
                           array(
								"type" => "dropdown",
								"group" => "Design Options",
								"heading" => "Select sorting of Posts by",
								"param_name" => "orderby",
								"value" => array(
								                "Published Date" => "date",
								                "Title" => "title",
								                "Modified Date" => "modified",
								                "Random" => "rand",
											   )
                                  ),
						
                           array(
								"type" => "dropdown",
								"group" => "Design Options",
								"heading" => "Select sorting order",
								"param_name" => "order",
								"value" => array(
								                "Descending" => "DESC",
								                "Ascending" => "ASC",
											   )
                                  ),
						
                           array(
								"type" => "dropdown",
								"group" => "Blog listing options",
								"heading" => "Select show if you want to show dates.",
								"param_name" => "listing_date_visibility",
								"value" => array(
								                "Show" => "show",
								                "Hide" => "hide",
											   )
                                  ),
                           array(
								"type" => "dropdown",
								"group" => "Blog listing options",
								"heading" => "Select show if you want to show information slide.",
								"param_name" => "listing_info_visibility",
								"value" => array(
								                "Show" => "show",
								                "Hide" => "hide",
											   )
                                  ),
	  ),
   ) );
}
class WPBakeryShortCode_bloglisting extends WPBakeryShortCode {}
?>