<?php
add_action( 'vc_before_init', 'yacht_carousel_integrateWithVC' );
function yacht_carousel_integrateWithVC() {
   vc_map( 
      array(
        'base' => 'yachtcarousel',
         "name" => esc_html( "Yacht Carousel", "yachtsailing" ),		
		 'params' => array(
		 array(
                'type' => 'textfield',
                'value' => '',
                'heading' => 'Title',
                'param_name' => 'simple_textfield',
            ),
         array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'titles',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Enter slide title',
                        'param_name' => 'slide_title',
	                    'description' => esc_html( "Enter title for current slide.", "yachtsailing")
                    ),
                    array(
                        'type' => 'attach_image',
                        'value' => '',
                        'heading' => 'Upload image',
                        'param_name' => 'slide_image',
	                    'description' => esc_html( "Upload image for current slide.", "yachtsailing")
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => 'Enter Video Iframe Code',
                        'param_name' => 'slide_video',
	                    'description' => esc_html( "Enter Iframe code to show video", "yachtsailing")
                    ),
                )
           )
 	 )
	)
);
}
class WPBakeryShortCode_yachtcarousel extends WPBakeryShortCode {
}
?>