<?php
add_action( 'vc_before_init', 'client_owl_carousel_integrateWithVC' );
function client_owl_carousel_integrateWithVC() {
   vc_map( array(
      "name" => esc_html( "Clients Slider", "yachtsailing" ),
      "base" => "clientowlcarousel",
      "params" => array(
        array(
            'type' => 'css_editor',
            'heading' => esc_html( 'Css', 'yachtsailing' ),
            'param_name' => 'css',
            'group' => esc_html( 'Design options', 'yachtsailing' ),
        ),
      ),
   ) );
}
class WPBakeryShortCode_clientowlcarousel extends WPBakeryShortCode {
}
?>