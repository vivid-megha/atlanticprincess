<?php
add_action( 'vc_before_init', 'productlisting_integrateWithVC' );
function productlisting_integrateWithVC() {
   vc_map( array(
    'name' => __( 'Product Listing', 'yachtsailing' ),
	'base' => 'productlisting', 
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Widget title', 'yachtsailing' ),
			'param_name' => 'title',
			'description' => __( 'Enter text used as widget title (Note: located above content element).', 'yachtsailing' ),
		),
		array(
			'type' => 'textarea',
			'heading' => __( 'Short Content', 'yachtsailing' ),
			'param_name' => 'short_content',
			'description' => __( 'Content Here', 'yachtsailing' ),
		),
	),
   ) ); 
}
class WPBakeryShortCode_productlisting extends WPBakeryShortCode {
}
?>