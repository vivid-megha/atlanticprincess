<?php
add_action( 'vc_before_init', 'productfeatures_integrateWithVC' );
function productfeatures_integrateWithVC() {
   vc_map( array(
    'name' => __( 'Feature Section', 'yachtsailing' ),
	'base' => 'productfeatures',
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Widget title', 'yachtsailing' ),
			'param_name' => 'title',
			'description' => __( 'Enter text used as widget title (Note: located above content element).', 'yachtsailing' ),
		),
		
		array(
			'type' => 'textfield',
			'heading' => __( 'Slider count', 'yachtsailing' ),
			'param_name' => 'count',
			'value' => 3,
			'description' => __( 'Enter number of slides to display (Note: Enter "All" to display all slides).', 'yachtsailing' ),
		),
		
		
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order by', 'yachtsailing' ),
			'param_name' => 'orderby',
			'value' => array(
				'',
				__( 'Date', 'yachtsailing' ) => 'date',
				__( 'ID', 'yachtsailing' ) => 'ID',
				__( 'Author', 'yachtsailing' ) => 'author',
				__( 'Title', 'yachtsailing' ) => 'title',
				__( 'Modified', 'yachtsailing' ) => 'modified',
				__( 'Random', 'yachtsailing' ) => 'rand',
				__( 'Comment count', 'yachtsailing' ) => 'comment_count',
				__( 'Menu order', 'yachtsailing' ) => 'menu_order',
			),
			'description' => sprintf( __( 'Select how to sort retrieved posts. More at %s.', 'yachtsailing' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Sort order', 'yachtsailing' ),
			'param_name' => 'order',
			'value' => array(
				__( 'Descending', 'yachtsailing' ) => 'DESC',
				__( 'Ascending', 'yachtsailing' ) => 'ASC',
			),
			'description' => sprintf( __( 'Select ascending or descending order. More at %s.', 'yachtsailing' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' ),
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'yachtsailing' ),
			'param_name' => 'el_class',
			'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'yachtsailing' ),
		),
		array(
			'type' => 'css_editor',
			'heading' => __( 'CSS box', 'yachtsailing' ),
			'param_name' => 'css',
			'group' => __( 'Design Options', 'yachtsailing' ),
		),
	),
   ) ); 
}
class WPBakeryShortCode_productfeatures extends WPBakeryShortCode {
}
?>