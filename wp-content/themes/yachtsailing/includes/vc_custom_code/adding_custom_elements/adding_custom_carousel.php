<?php
$css = '';
extract(shortcode_atts(array(
    'title' => '',
    'count' => '',
    'orderby' => '',
    'order' => ''	,
	'icon_counter' => ''
), $atts));
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
/* ctitle variables starts here */
    $count = $atts["count"];
    $orderby = $atts["orderby"];
	$order = $atts["order"];
	$showtype = $atts["showtype"];
	
	$testimonials = wp_get_recent_posts( array( 'numberposts' => $count, 'category' => 0, 'orderby' => $orderby, 'order' => $order, 'post_type' => 'testimonials','suppress_filters' => false ) );
if($showtype == 'slider'){
?>
<ul class="reviews-slider enable-bx-slider" data-auto="true" data-auto-hover="true" data-mode="horizontal" data-pager="false" data-pager-custom="null" data-prev-selector="null" data-next-selector="null">
		<?php foreach( $testimonials as $testimonial ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id($testimonial['ID']),'full');
			$firstname = get_the_author_meta( 'first_name', $testimonial['post_author'] );
			$lastname = get_the_author_meta( 'last_name', $testimonial['post_author'] );
			$desi = get_post_meta($testimonial['ID'],"_cmb_testimonials_designation",true); ?>
		 <li>
            <div class="review wow slideInUp" data-wow-delay="0.7s" data-wow-duration="1.5s"><div class="row row--no-padding">
            <div class="col-xs-3">
            <div class="review__img">
            <div class="review__quote triangle triangle--services">
            <span class="fa-quote-left fa"></span>
            </div>
            <img class="img-responsive" src="<?php echo esc_url($image[0], 'yachtsailing');?>" alt="<?php echo esc_html($testimonial['post_title'], 'yachtsailing');?>" />
            </div>
            </div>
            <div class="col-xs-9">
                <div class="review__text">
                    <div class="review__main-text">
                    <?php echo esc_html($testimonial['post_content'], 'yachtsailing'); ?>
                    <div class="line line--small"><div class="line__first"></div><div class="line__second"></div></div>
                    </div>
                    <div class="review__author">
                      <?php echo esc_html($testimonial['post_title'], 'yachtsailing'); ?>
                      <span><?php echo esc_html($desi, 'yachtsailing'); ?></span>
                    </div>
                </div>
			</div>
            </div></div>
			</li>
			<?php }  ?>
</ul>
<?php } else { ?>
<div class="home-reviews__quote triangle triangle--services">
  <span class="fa  fa-quote-right"></span>
</div>
<div class="owl-carousel js-home-reviews enable-owl-carousel" data-auto-play="5000" data-stop-on-hover="true" data-items="2" data-pagination="true" data-navigation="false" data-items-desktop="2" data-items-desktop-small="2" data-items-tablet="1" data-items-tablet-small="1" >
	<?php 
	
			foreach( $testimonials as $testimonial ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id($testimonial['ID']),'full');
			$firstname = get_the_author_meta( 'first_name', $testimonial['post_author'] );	
			$lastname = get_the_author_meta( 'last_name', $testimonial['post_author'] );
			$desi = get_post_meta($testimonial['ID'],"_cmb_testimonials_designation",true); ?>
			<div><div class="home-reviews__review wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
			<div class="home-reviews__person"><img src="<?php echo esc_url($image[0], 'yachtsailing'); ?>"  class="photo img-responsive"></div>
			<div class="home-reviews__text"><p><?php echo esc_html($testimonial['post_content'], 'yachtsailing'); ?></p></div>
			<div class="home-reviews__author"><?php echo esc_html($testimonial['post_title'], 'yachtsailing');?></div>
			<div class="home-reviews__position"><?php echo esc_html($desi, 'yachtsailing'); ?></div>
			</div></div>
		<?php } } ?>
</div>
<?php echo $this->endBlockComment('customowlCarousel'); ?>