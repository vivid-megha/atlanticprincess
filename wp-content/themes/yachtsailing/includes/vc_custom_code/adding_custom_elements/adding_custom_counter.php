<?php
add_action( 'vc_before_init', 'counter_integrateWithVC' );
function counter_integrateWithVC() {
	vc_map( array(
      "name" => esc_html( "Counter Block", "yachtsailing" ),
      "base" => "counter",
      "params" => array(
        array(
            'type' => 'css_editor',
            'heading' => esc_html( 'Css', 'yachtsailing' ),
            'param_name' => 'css',
            'group' => esc_html( 'Design options', 'yachtsailing' ),
        ),
		 
/* Extra Design Options starts here */	
/* Counter shortcodes starts here */				
						array(
							"group" => "Counter Shortcode",	
							'type' => 'textfield',
							'heading' => "Enter Material Icon Shortcode",
							'param_name' => 'material_icon_code',
							'value' => "",
                            'description' => esc_html( "Enter Material Icon Code here. For ex: (insert_emoticon for smiling face.) See Help URL: https://design.google.com/icons/ ", "yachtsailing" )
						     ),
						array(
							"group" => "Counter Shortcode",	
							'type' => 'textfield',
							'heading' => "Font Size",
							'param_name' => 'icon_font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),							 
						array(
							"group" => "Counter Shortcode",	
							'type' => 'iconpicker',
							'heading' => "Select Icon Shortcode",
							'param_name' => 'iconpicker_code',
							'value' => "",
                            'description' => esc_html( "If above Textbox is empty only then this Icon selector would work.", "yachtsailing" )
						     ),
/* Counter shortcodes ends here */						
/* Counter Numbers starts here */
						array(
							"group" => "Number",	
							'type' => 'textfield',
							'heading' => "Enter Counter Number",
							'param_name' => 'counter_number',
							'value' => "",
                            'description' => esc_html( "Enter Counter Number . For ex: (285)", "yachtsailing" )
						     ),
/* Counter Numbers ends here */			
/* Counter Number Text starts here */			
						array(
							"group" => "Counter Text",	
							'type' => 'textfield',
							'heading' => "Enter Counter Number Text",
							'param_name' => 'counter_after_text',
							'value' => "",
                            'description' => esc_html( "Enter to show junt after the Counter Number", "yachtsailing" )
						     ),
                        array(
								"type" => "dropdown",
							"group" => "Counter Text",	
								"heading" => "Text Transform",
								"param_name" => "cntext_text_transform",
								"value" => array(
													 "Unset" => "unset",
													 "Upper Case" => "uppercase",
													 "Lower Case" => "lowercase",
													 "Capitalize" => "capitalize",
													 "Inherit" => "inherit",
													 "Initial" => "initial",
													 "Fullwidth" => "full-width",
													 "None" => "none"
												)
                             ),
						array(
							"group" => "Counter Text",	
							'type' => 'colorpicker',
							'heading' => "Font Color",
							'param_name' => 'cntext_font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color For Counter Title", "yachtsailing" )
						     ),
						array(
							"group" => "Counter Text",	
							'type' => 'textfield',
							'heading' => "Font Size",
							'param_name' => 'cntext_font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Counter Text",	
							'type' => 'textfield',
							'heading' => "Line Height",
							'param_name' => 'cntext_line_height',
							'value' => "",
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Counter Text",	
							'type' => 'textfield',
							'heading' => "Letter spacing in px",
							'param_name' => 'cntext_letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
							"group" => "Counter Text",	
								"heading" => "Font weight",
								"param_name" => "cntext_font_weight",
								"value" => array(
													 "normal" => "normal",
													 "100" => "100",
													 "200" => "200",
													 "300" => "300",
													 "400" => "400",
													 "500" => "500",
													 "600" => "600",
													 "700" => "700",
													 "800" => "800",
													 "900" => "900",
													 
													 "bold" => "bold",
													 "bolder" => "bolder",
													 "lighter" => "lighter",													
													 "inherit" => "inherit",
													 "initial" => "initial",
													 "unset" => "unset"
													 
													 
												)
                                  ),		
							 
/* Counter Number Text Ends here */			
/* Counter Titles Starts here */				 
						array(
							"group" => "Counter Title",	
							'type' => 'textfield',
							'heading' => "Enter Counter Title",
							'param_name' => 'counter_title',
							'value' => "",
                            'description' => esc_html( "Enter Counter Title . For ex: (Happy Clients)", "yachtsailing" )
						     ),
                        array(
								"type" => "dropdown",
							"group" => "Counter Title",	
								"heading" => "Text Transform",
								"param_name" => "ctitle_text_transform",
								"value" => array(
													 "Unset" => "unset",
													 "Upper Case" => "uppercase",
													 "Lower Case" => "lowercase",
													 "Capitalize" => "capitalize",
													 "Inherit" => "inherit",
													 "Initial" => "initial",
													 "Fullwidth" => "full-width",
													 "None" => "none"
												)
                             ),
						array(
							"group" => "Counter Title",	
							'type' => 'colorpicker',
							'heading' => "Font Color",
							'param_name' => 'ctitle_font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color For Counter Title", "yachtsailing" )
						     ),
						array(
							"group" => "Counter Title",	
							'type' => 'textfield',
							'heading' => "Font Size",
							'param_name' => 'ctitle_font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Counter Title",	
							'type' => 'textfield',
							'heading' => "Line Height",
							'param_name' => 'ctitle_line_height',
							'value' => "",
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Counter Title",	
							'type' => 'textfield',
							'heading' => "Letter spacing in px",
							'param_name' => 'ctitle_letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
							"group" => "Counter Title",	
								"heading" => "Font weight",
								"param_name" => "ctitle_font_weight",
								"value" => array(
													 "normal" => "normal",
													 "100" => "100",
													 "200" => "200",
													 "300" => "300",
													 "400" => "400",
													 "500" => "500",
													 "600" => "600",
													 "700" => "700",
													 "800" => "800",
													 "900" => "900",
													 
													 "bold" => "bold",
													 "bolder" => "bolder",
													 "lighter" => "lighter",													
													 "inherit" => "inherit",
													 "initial" => "initial",
													 "unset" => "unset"
													 
													 
												)
                                  ),		
								  					 
/* Counter Titles Ends here */			
/* Counter Number Starts here */				 
						array(
							"group" => "Number",	
							'type' => 'colorpicker',
							'heading' => "Font Color",
							'param_name' => 'ctext_font_color',
							'value' => "",
                            'description' => esc_html( "Select Font Color For Counter Text", "yachtsailing" )
						     ),
						array(
							"group" => "Number",	
							'type' => 'textfield',
							'heading' => "Font Size",
							'param_name' => 'ctext_font_size',
							'value' => "",
                            'description' => esc_html( "Enter Font Size in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Number",	
							'type' => 'textfield',
							'heading' => "Line Height",
							'param_name' => 'ctext_line_height',
							'value' => "",
                            'description' => esc_html( "Enter Line Height in pixel for ex. 15 or 3.2 (but not include px)", "yachtsailing" )							
						),
						array(
							"group" => "Number",	
							'type' => 'textfield',
							'heading' => "Letter spacing in px",
							'param_name' => 'ctext_letter_spacing',
							'value' => "",
                            'description' => esc_html( "Enter Letter spacing in pixel for ex. 1 or 3.2 (but not include px)", "yachtsailing" )							
							),
                        array(
								"type" => "dropdown",
							"group" => "Number",	
								"heading" => "Font weight",
								"param_name" => "ctext_font_weight",
								"value" => array(
													 "normal" => "normal",
													 "100" => "100",
													 "200" => "200",
													 "300" => "300",
													 "400" => "400",
													 "500" => "500",
													 "600" => "600",
													 "700" => "700",
													 "800" => "800",
													 "900" => "900",
													 
													 "bold" => "bold",
													 "bolder" => "bolder",
													 "lighter" => "lighter",													
													 "inherit" => "inherit",
													 "initial" => "initial",
													 "unset" => "unset"
													 
													 
												)
                                  ),
/* Counter Number Ends here */
      ),
   ));
}
class WPBakeryShortCode_counter extends WPBakeryShortCode { }
?>