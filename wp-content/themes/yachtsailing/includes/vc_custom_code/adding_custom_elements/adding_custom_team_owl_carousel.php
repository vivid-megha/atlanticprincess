<?php
add_action( 'vc_before_init', 'team_owl_carousel_integrateWithVC' );
function team_owl_carousel_integrateWithVC() {
   vc_map( array(
      "name" => esc_html( "Team Slider", "yachtsailing" ),
      "base" => "teamowlcarousel",
	  "icon" =>get_stylesheet_directory_uri().'/vc_icons/team_icon.png',
      "params" => array(
        array(
            'type' => 'css_editor',
            'heading' => esc_html( 'Css', 'yachtsailing' ),
            'param_name' => 'css',
            'group' => esc_html( 'Design options', 'yachtsailing' ),
        ),
		
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order by', 'yachtsailing' ),
			'param_name' => 'orderby',
			'value' => array(
				'',
				__( 'Date', 'yachtsailing' ) => 'date',
				__( 'ID', 'yachtsailing' ) => 'ID',
				__( 'Author', 'yachtsailing' ) => 'author',
				__( 'Title', 'yachtsailing' ) => 'title',
				__( 'Modified', 'yachtsailing' ) => 'modified',
				__( 'Random', 'yachtsailing' ) => 'rand',
				__( 'Comment count', 'yachtsailing' ) => 'comment_count',
				__( 'Menu order', 'yachtsailing' ) => 'menu_order',
			),
			'description' => sprintf( __( 'Select how to sort retrieved posts. More at %s.', 'yachtsailing' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' ),
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Sort order', 'yachtsailing' ),
			'param_name' => 'order',
			'value' => array(
				__( 'Descending', 'yachtsailing' ) => 'DESC',
				__( 'Ascending', 'yachtsailing' ) => 'ASC',
			),
			'description' => sprintf( __( 'Select ascending or descending order. More at %s.', 'yachtsailing' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' ),
		)
      ),
   ) );
}
class WPBakeryShortCode_teamowlcarousel extends WPBakeryShortCode {
}
?>