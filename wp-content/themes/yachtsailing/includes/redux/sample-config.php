<?php
/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */
if (!class_exists('Redux_Framework_sample_config')) {
    class Redux_Framework_sample_config {
        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;
        public function __construct() {
            if (!class_exists('ReduxFramework')) {
                return;
            }
            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }
        }
        public function initSettings() {
            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();
            // Set the default arguments
            $this->setArguments();
            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();
            // Create the sections and fields
            $this->setSections();
            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }
        /**
          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.
         * */
        function compiler_action($options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
            
        }
        /**
          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.
          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => esc_html('Section via hook', 'yachtsailing'),
                'desc' => esc_html('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'yachtsailing'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );
            return $sections;
        }
        /**
          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        function change_arguments($args) {
            $args['dev_mode'] = false;
            return $args;
        }
        /**
          Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';
            return $defaults;
        }
        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);
                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }
        public function setSections() {
            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();
           
            ob_start();
            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';
            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'yachtsailing'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class, 'yachtsailing'); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title, 'yachtsailing'); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview','yachtsailing'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot, 'yachtsailing'); ?>" alt="<?php esc_attr_e('Current theme preview','yachtsailing'); ?>" />
                <?php endif; ?>
                <h4><?php echo $this->theme->display('Name'); ?></h4>
                <div>
                    <ul class="theme-info">
                        <li><?php printf(esc_html('By %s', 'yachtsailing'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(esc_html('Version %s', 'yachtsailing'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . esc_html('Tags', 'yachtsailing') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . esc_html('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.','yachtsailing') . '</p>', __('//codex.wordpress.org/Child_Themes', 'yachtsailing'), $this->theme->parent()->display('Name'));
            }
            ?>
                </div>
            </div>
            <?php
            $item_info = ob_get_contents();
            ob_end_clean();
            $sampleHTML = '';
           $plugin_path =  plugin_dir_url("redux-framework/sample"); 
            if (file_exists($plugin_path . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if (empty($wp_filesystem)) {
                    require_once ABSPATH . '/wp-admin/includes/file.php';
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents($plugin_path . '/info-html.html');
            }
			
			 $this->sections[] = array(
               'icon'      => 'el el-brush',
                'title'     => esc_html('Appearance ', 'yachtsailing'),                
                'fields'    => array(
array(
       'id' => 'section-appearance-whole-site-start',
       'type' => 'section',
       'title' => __('GLOBAL SITE APPEARANCE OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),                   
        array(
                        'id'        => 'load_icon_val',
                        'type'      => 'button_set',
                        'title'     => esc_html('Load Icon', 'yachtsailing'),
						'options'   => array(
							1        => 'Show',
							0       => 'Hide',
						),
						'default' => 1
                    ),                                 
							
					array(
					'id'=> 'load_icon_image',
					'type'     => 'media',
					'title'    => __('Upload Load icon', 'yachtsailing'),
					'subtitle' => __('Upload Load icon', 'yachtsailing'),
					'desc'     => __('Upload Icon image to show that would visible just before any page loads and shows white screen. ', 'yachtsailing'),
					'required' => array('load_icon_val','equals',1),
					'default'   => null
					),
				   array(
					'id'        => 'layout_swtich', 
					'type'      => 'button_set',
					'title'     => __('Layout Style', 'yachtsailing'),
					'subtitle'  => __('Choose boxed or full page mode', 'yachtsailing'),
					'options'   => array(
						"full"  => __("Full Width", 'yachtsailing'),
						"boxed" => __("Boxed", 'yachtsailing')
					),
					'default'   => 'full'
				),
	          array(
                'id'=> 'boxed_background',
                'type'     => 'background',
                'title'    => __('Body Background', 'yachtsailing'),
                'subtitle' => __('Background on "BOXED" pages.', 'yachtsailing'),
				'preview' => 'true',
				 'required' => array('layout_swtich','equals','boxed'),
				'preview_height' => '100px'
                ),
				array(
						'id'       => 'site_width',
						'type'     => 'dimensions',
						'units'    => array('em','px','%'),
						'title'    => __('Site width', 'yachtsailing'),
						'subtitle'    => __('Default width is 1170px. ', 'yachtsailing'),
						'width' => true,
						'height' => false,
						'default'  => array(
							'Width'   => '1170px'
						)
              ),
                    
				
array(
       'id' => 'section-appearance-whole-site-ends',
       'type' => 'section',
       'indent' => false 
   ),                   
     array(
          'id'        => 'back_to_top_switch', 
          'type'      => 'button_set',
          'title'     => __('Back to top icon', 'yachtsailing'),
          'subtitle'  => __('Select show if you want to show back to top icon.', 'yachtsailing'),
          'options'   => array(
            "show" => __("Show", 'yachtsailing'),
            "hide" => __("Hide", 'yachtsailing')
          ),
          'default'   => 'show'
        ),
        
        array(
                'id'=> 'back_to_top_icon',
                'type'     => 'media',
                'title'    => __('Upload Back to Top Icon', 'yachtsailing'),
                'subtitle' => __('', 'yachtsailing'),
                'desc'     => __('Upload back to top icon to show in footer-right side. Otherwise default icons would be visible.', 'yachtsailing'),
                'required' => array('back_to_top_switch','equals','show'),
                'default'   => null
                )                   
		     )
			);
            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'icon'      => 'el-icon-cogs',
                'title'     => esc_html('General Settings', 'yachtsailing'),                
                'fields'    => array(
array(
      'id'=>'sidebars_list',
      'type' => 'multi_text',
      'title' => __('Widget Generator', 'yachtsailing'),
      'validate' => 'Widget',
      'add_text' => __('Add Widget', 'yachtsailing'),
      'subtitle' => __('Add unlimited custom Widgets to you site.', 'yachtsailing'),
      'default'  => null
  ) ,	 		
		array(
			'id'        => 'logo_image',
			'type'      => 'media',
			'url'       => true,
			'title'     => esc_html('Logo Image', 'yachtsailing'),
			'subtitle'  => esc_html('Upload a  logo image here', 'yachtsailing'),
			'default'   => array('url' => get_template_directory_uri().'/assets/images/logo.png')
		),     
		array(
			'id'        => 'logo_text',
			'type'      => 'text',
			'url'       => true,
			'title'     => esc_html('Logo Text Here', 'yachtsailing'),
			'subtitle'  => esc_html('Logo Text Here', 'yachtsailing'),
			'default'   => 'Yacht<span>Sailing</span>'
		),     
		array(
			'id'        => 'favicon',
			'type'      => 'media',
			'url'       => true,
			'title'     => esc_html('Custom Favicon', 'yachtsailing'),
			'subtitle'  => esc_html('Upload your Favicon here,  preferred size is  16x16 or 32x32px', 'yachtsailing'), 
			'default'   => array('url' => get_template_directory_uri().'/images/backgrounds/favicon.png')
		),
   array(
		'id'        => 'show_top_details_bar', 
		'type'      => 'button_set',
		'title'     => __('Show top details bar ?', 'yachtsailing'),
		'subtitle'  => __('Select "YES" if you want to top details bar', 'yachtsailing'),
		'options'   => array(
		    "yes" => __("Yes", 'yachtsailing'),
			"no"  => __("No", 'yachtsailing')			
		),
		'default'   => 'yes'
	),
),
            );
			
            $this->sections[] = array(
                'icon'      => 'el el-home',
                'title'     => esc_html('Header', 'yachtsailing'),                
                'fields'    => array(
  array(
       'id' => 'header_starts',
       'type' => 'section',
       'title' => __('HOMEPAGE HEADER OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),
       array(
					'id'        => 'home_header', 
					'type'      => 'button_set',
					'title'     => __('Home Header Type', 'yachtsailing'),
					'subtitle'  => __('Choose Header type for only Homepage', 'yachtsailing'),
					'options'   => array(
						"theme_defaults"  => __("Theme defaults", 'yachtsailing'),					
						"header_first"  => __("One", 'yachtsailing'),
						"header_second" => __("Two", 'yachtsailing'),
						"header_third"  => __("Three", 'yachtsailing'),
						"header_fourth" => __("Four", 'yachtsailing'),
						"header_fifth" => __("Five", 'yachtsailing'),
						"header_sixth" => __("Six", 'yachtsailing'),
						"header_inner" => __("Inner default", 'yachtsailing')
						
					),
					'default'   => 'theme_defaults'
				),
array( 
    'id'       => 'header1img',
    'type'     => 'raw',
    'title'    => __('Header First', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_1.jpg">',
    'required' => array('home_header','equals','header_first')	
),				
array( 
    'id'       => 'header2img',
    'type'     => 'raw',
    'title'    => __('Header second', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_2.jpg">',
    'required' => array('home_header','equals','header_second')	
),				
array( 
    'id'       => 'header3img',
    'type'     => 'raw',
    'title'    => __('Header third', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_3.jpg">',
    'required' => array('home_header','equals','header_third')	
),				
array( 
    'id'       => 'header4img',
    'type'     => 'raw',
    'title'    => __('Header fourth', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_4.jpg">',
    'required' => array('home_header','equals','header_fourth')	
),				
array( 
    'id'       => 'header5img',
    'type'     => 'raw',
    'title'    => __('Header fifth', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_5.jpg">',
    'required' => array('home_header','equals','header_fifth')	
),				
array( 
    'id'       => 'header6img',
    'type'     => 'raw',
    'title'    => __('Header sixth', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_6.jpg">',
    'required' => array('home_header','equals','header_sixth')	
 ),
array( 
    'id'       => 'headerinnerimg',
    'type'     => 'raw',
    'title'    => __('Header inner', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_inner.jpg">',
    'required' => array('home_header','equals','header_inner')	
 ),
 
   array(
					'id'        => 'home_header_sticky', 
					'type'      => 'button_set',
					'title'     => __('Sticky Header ', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing'),
					'options'   => array(
						"yes"  => __("Yes", 'yachtsailing'),
						"no" => __("No", 'yachtsailing'),
						"default"  => __("Theme Default", 'yachtsailing')
					),
					'default'   => 'default'
				),
				
               array(
					'id'        => 'home_sticky_header_bg', 
					'type'      => 'background',
				    'preview_height' => '100px',
					'title'     => __('Header backgroud', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing'),					
                    'required' => array('home_header_sticky','equals','yes'),					
				),
				   
               array(
					'id'        => 'home_header_bg', 
					'type'      => 'background',
				    'preview_height' => '100px',
					'title'     => __('Header backgroud', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing'),					
                    'required' => array('home_header_sticky','equals','no'),					
				),
   array(
       'id' => 'header_ends',
       'type' => 'section',       
       'indent' => false 
   ),
  array(
       'id' => 'inner_header_starts',
       'type' => 'section',
       'title' => __('INNERPAGES HEADER OPTIONS', 'yachtsailing'),
       'subtitle' => __('NOTE: 1) For styling go to Style settings > INNERPAGE HEADER STYLING OPTIONS. 2) You can also select Header from each page, only if following header type is selected to "THEME DEFAULTS. "', 'yachtsailing'),
       'indent' => true 
   ),
                   array(
					'id'        => 'inner_header', 
					'type'      => 'button_set',
					'title'     => __('Header Type', 'yachtsailing'),
					'subtitle'  => __('Choose Header type for only Homepage', 'yachtsailing'),
					'options'   => array(
						"theme_defaults"  => __("Theme defaults", 'yachtsailing'),
						"header_first"  => __("One", 'yachtsailing'),
						"header_second" => __("Two", 'yachtsailing'),
						"header_third"  => __("Three", 'yachtsailing'),
						"header_fourth" => __("Four", 'yachtsailing'),
						"header_fifth" => __("Five", 'yachtsailing'),
						"header_sixth" => __("Six", 'yachtsailing'),
						"header_inner" => __("Inner default", 'yachtsailing')
						
					),
					'default'   => 'theme_defaults'
				),
array( 
    'id'       => 'inner_header1img',
    'type'     => 'raw',
    'title'    => __('Header First', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_1.jpg">',
    'required' => array('inner_header','equals','header_first')	
),				
array( 
    'id'       => 'inner_header2img',
    'type'     => 'raw',
    'title'    => __('Header second', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_2.jpg">',
    'required' => array('inner_header','equals','header_second')	
),				
array( 
    'id'       => 'inner_header3img',
    'type'     => 'raw',
    'title'    => __('Header third', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_3.jpg">',
    'required' => array('inner_header','equals','header_third')	
),				
array( 
    'id'       => 'inner_header4img',
    'type'     => 'raw',
    'title'    => __('Header fourth', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_4.jpg">',
    'required' => array('inner_header','equals','header_fourth')	
),				
array( 
    'id'       => 'inner_header5img',
    'type'     => 'raw',
    'title'    => __('Header fifth', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_5.jpg">',
    'required' => array('inner_header','equals','header_fifth')	
),				
array( 
    'id'       => 'inner_header6img',
    'type'     => 'raw',
    'title'    => __('Header sixth', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_6.jpg">',
    'required' => array('inner_header','equals','header_sixth')	
 ),
array( 
    'id'       => 'inner_headerinnerimg',
    'type'     => 'raw',
    'title'    => __('Header inner', 'yachtsailing'),
    'content'  =>   '<img src="'.get_template_directory_uri().'/assets/images/yatch_home_inner.jpg">',
    'required' => array('inner_header','equals','header_inner')	
 ),   
        array(
					'id'        => 'inner_header_sticky', 
					'type'      => 'button_set',
					'title'     => __('Sticky Header ', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing'),
					'options'   => array(
						"yes"  => __("Yes", 'yachtsailing'),
						"no" => __("No", 'yachtsailing'),
						"default"  => __("Theme Default", 'yachtsailing')
					),
					'default'   => 'default'
				),
				
               array(
					'id'        => 'inner_sticky_header_bg', 
					'type'      => 'background',
 			        'preview_height' => '100px',					
					'title'     => __('Header backgroud', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing'),					
                    'required' => array('inner_header_sticky','equals','yes'),					
				),
				   
               array(
					'id'        => 'inner_header_bg', 
					'type'      => 'background',
		            'preview_height' => '100px',					
					'title'     => __('Header backgroud', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing'),					
                    'required' => array('inner_header_sticky','equals','no'),					
				),
		   array(
			'id'        => 'header_featured_swtich', 
			'type'      => 'button_set',
			'title'     => __('Featured Image or Background', 'yachtsailing'),
			'options'   => array(
				"default"  => __("Theme defaults", 'yachtsailing'),
				"custom" => __("Custom", 'yachtsailing')
			),
            'required' => array('inner_header','equals','inner_header_inner'),				
			'default'   => 'default'
		),
	  array(
		'id'=> 'header_featured',
		'type'     => 'background',
		'title'    => __('Featured Image or Background', 'yachtsailing'),
		'preview' => 'true',
		 'required' => array('header_featured_swtich','equals','custom'),
		'preview_height' => '100px'
		),	
array(
    'id'             => 'gap_header_menu_content',
    'type'           => 'spacing',
	 'required' => array('inner_header','equals','inner_header_inner'),		
    'mode'           => 'padding',
    'units'          => array('em', 'px'),
    'units_extended' => 'false',
    'title'          => __('Gap between header Menu and Content section', 'yachtsailing'),
	'top' => false,
	'right' => false,
	'left' => false,
    'default'            => array(
	'padding-bottom'     => '200px'
  )),
  array(
       'id' => 'inner_header_ends',
       'type' => 'section',
       'indent' => false 
   )
),
            );        
	       $this->sections[] = array(
                'icon'      => 'el el-shopping-cart',
                'title'     => esc_html('Woocommerce', 'yachtsailing'),                
                'fields'    => array(
								array(
       'id' => 'header_woocommerce_starts',
       'type' => 'section',
       'title' => __('HEADER WOOCOMMERCE OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),
							array(
										'id'        => 'woo_icon_switch',
										'type'      => 'button_set',
										'title'     => __('Cart icon ?', 'yachtsailing'),
										'subtitle'  => __('Show woocommerce icon in header ?', 'yachtsailing'),
										'options'   => array(
											"show"        => 'Show',
											"hide"       => 'Hide',
										),
										'default' => "show"
									),
								array(
       'id' => 'header_woocommerce_ends',
       'type' => 'section',
       'indent' => false 
   ),
	array(
       'id' => 'shop_woocommerce_starts',
       'type' => 'section',
       'title' => __('SHOP PAGE WOOCOMMERCE OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),
				     array(
                        'id'        => 'shop_title',
                        'type'      => 'text',       
                        'title'     => __('Shop page title ', 'yachtsailing'),
                        'subtitle'  => __('Enter shop page title here.', 'yachtsailing'),                        
                        'default'   => 'shop'
                    ),
				   array(
					'id'        => 'shop_sidebar_swtich', 
					'type'      => 'button_set',
					'title'     => __('Shop page sidebar', 'yachtsailing'),
					'subtitle'  => __('Choose show if you want to show sidebar', 'yachtsailing'),
					'options'   => array(
						"show"  => __("Show", 'yachtsailing'),
						"hide" => __("Hide", 'yachtsailing')
					),
					'default'   => 'show'
				),
					array(
						'id'        => 'shop_sidebar',
						'type'      => 'image_select',
						'title'     => __('Shop page sidebar ?', "yachtsailing"),
						'subtitle'  => __('Select a sidebar position for pages.', "yachtsailing"),
						'options'   => array(
							'right' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
							'fullwidth' => array('alt' => 'No Sidebar',  'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
							'left' => array('alt' => 'Sidebar Left',  'img' => ReduxFramework::$_url . 'assets/img/2cl.png')
						),
                      'required' => array('shop_sidebar_swtich','equals','show'),					
						'default'   => 'right'
					),
				   array(
					'id'        => 'filter_swtich', 
					'type'      => 'button_set',
					'title'     => __('Shop page price filter', 'yachtsailing'),
					'subtitle'  => __('Choose show if you want to show Price filter', 'yachtsailing'),
					'options'   => array(
						"show"  => __("Show", 'yachtsailing'),
						"hide" => __("Hide", 'yachtsailing')
					),
                    'required' => array('whole_site_color_customization','equals','yes'),					
					'default'   => 'show'
				),
					
				   
					array(
						'id'        => 'product_listing_column',
						'type'      => 'image_select',
						'title'     => __('Shop product columns?', "yachtsailing"),
						'subtitle'  => __('Select columns for shop page products. ', "yachtsailing"),
						'options'   => array(
							'two_columns' => array('alt' => 'Two column',  'img' => get_template_directory_uri() . '/includes/redux/images/2col.png'),
							'three_columns' => array('alt' => 'Three column',  'img' => get_template_directory_uri() . '/includes/redux/images/3col.png'),
							'four_columns' => array('alt' => 'Four column',  'img' => get_template_directory_uri() . '/includes/redux/images/4col.png')
						),
						'default'   => 'three_columns'
					),
				     array(
                        'id'        => 'product_per_page',
                        'type'      => 'text',       
                        'title'     => __('Number of Products per Page', 'yachtsailing'),
                        'subtitle'  => __('Enter number of products per page. In numeric like 10, 5, 4 etc.', 'yachtsailing'),                        
                        'default'   => '#'
                    ),
					array(
								'id'        => 'shop_price_switch',
								'type'      => 'button_set',
								'title'     => __('Show price on Shop page ?', 'yachtsailing'),
								'subtitle'  => __('Show price on Shop page ', 'yachtsailing'),
								'options'   => array(
									"show"        => 'Show',
									"hide"       => 'Hide',
								),
								'default' => "show"
							),
	array(
       'id' => 'shop_woocommerce_ends',
       'type' => 'section',
       'indent' => false 
   ),
	array(
       'id' => 'single_woocommerce_starts',
       'type' => 'section',
       'title' => __('SINGLE WOOCOMMERCE PAGE OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),
				   array(
					'id'        => 'single_shop_sidebar_swtich', 
					'type'      => 'button_set',
					'title'     => __('Single Shop page sidebar', 'yachtsailing'),
					'subtitle'  => __('Choose show if you want to show sidebar', 'yachtsailing'),
					'options'   => array(
						"show"  => __("Show", 'yachtsailing'),
						"hide" => __("Hide", 'yachtsailing')
					),
					'default'   => 'show'
				),
					array(
						'id'        => 'single_shop_sidebar',
						'type'      => 'image_select',
						'title'     => __('Shop page sidebar ?', "yachtsailing"),
						'subtitle'  => __('Select a sidebar position for pages.', "yachtsailing"),
						'options'   => array(
							'right' => array('alt' => 'Sidebar Right',  'img' => ReduxFramework::$_url . 'assets/img/2cr.png'),
							'fullwidth' => array('alt' => 'No Sidebar',  'img' => ReduxFramework::$_url . 'assets/img/1col.png'),
							'left' => array('alt' => 'Sidebar Left',  'img' => ReduxFramework::$_url . 'assets/img/2cl.png')
						),
                    'required' => array('single_shop_sidebar_swtich','equals','show'),						
						'default'   => 'right'
					),
				   array(
					'id'        => 'single_share_swtich', 
					'type'      => 'button_set',
					'title'     => __('Share icon', 'yachtsailing'),
					'subtitle'  => __('Choose show if you want to show Share icon', 'yachtsailing'),
					'options'   => array(
						"show"  => __("Show", 'yachtsailing'),
						"hide" => __("Hide", 'yachtsailing')
					),
					'default'   => 'show'
				),					
	array(
       'id' => 'single_woocommerce_ends',
       'type' => 'section',
       'indent' => false 
   ),
   							
									
				),
            );    
			
			
			$this->sections[] = array(
                'icon'      => 'el el-adjust-alt',
                'title'     => esc_html('Style Settings', 'yachtsailing'),                
                'fields'    => array(
				array(
       'id' => 'whole_site_styling_starts',
       'type' => 'section',
       'title' => __('GLOBAL SITE STYLING OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),
                    array(
                        'id' => 'body-font',
                        'type' => 'typography',
                        'output' => array('body'),
                        'title' => esc_html('Body Font', 'yachtsailing'),
                        'subtitle' => esc_html('Select a primary font for site', 'yachtsailing'),                        
                        'google' => true,                        
                        'text-align'    => false,
                        'subsets'       => false,
                        'line-height' => false,                        
                        'font-style'    => false,
                        'default' => array(
                            'color' => '#000',
                            'font-size' => '14px',                            
                            'font-family' => "Raleway",                            
                            'font-weight'   => "400"
                        ),
                    ),
                     array(
                        'id' => 'heading-font',
                        'type' => 'typography',
                        'output' => array('h1','h2','h3','h4','h5','h6'),
                        'title' => esc_html('Font Heading (Option)', 'yachtsailing'),                        
                        'desc'  => esc_html('Select a google font heading: h1,h2,h3,h4,h5,h6. <br/> Note To use nexa_boldregular font, you have to hit Reset Section button.','yachtsailing'),                        
                        'google' => true,                        
                        'color' => false,
                        'font-size' => false,                        
                        'line-height' => false,
                        'font-weight'   => false,
                        'text-align'    => false,
                        'subsets'       => false,                        
                        'font-style'    => false,                        
                        'default' => array(                            
                            'font-family' => "Roboto Slab",
                        ),
                    ),
					
	   array(
		'id'        => 'whole_site_color_customization', 
		'type'      => 'button_set',
		'title'     => __('Global site color customization.', 'yachtsailing'),
		'subtitle'  => __('Select "YES" if you want to customize.', 'yachtsailing'),
		'options'   => array(
		    "no" => __("Theme default", 'yachtsailing'),
			"yes"  => __("Yes", 'yachtsailing')
		),
		'default'   => 'no'
	),
	 array(
			'id'        => 'primary_color', 
			'type'      => 'color',
			'title'     => __('Primary color', 'yachtsailing'),
            'required' => array('whole_site_color_customization','equals','yes')
		),
	 array(
			'id'        => 'secondary_color', 
			'type'      => 'color',
			'title'     => __('Secondary color', 'yachtsailing'),
            'required' => array('whole_site_color_customization','equals','yes')
		),
	   array(
		'id'        => 'heading_underline_customize', 
		'type'      => 'button_set',
		'title'     => __('Heading underline customization ?', 'yachtsailing'),
		'subtitle'  => __('Select "YES" if you want to customize heading underline.', 'yachtsailing'),
		'options'   => array(
		    "no" => __("Theme default", 'yachtsailing'),
			"color_customization"  => __("Color Customization only", 'yachtsailing'),
			"image_color" => __("New image or color", 'yachtsailing')
		),
		'default'   => 'no'
	),
	 array(
			'id'        => 'heading_underline', 
			'type'      => 'color_rgba',
			'title'     => __('Header underline  color', 'yachtsailing'),
            'required' => array('heading_underline_customize','equals','color_customization')
		),
	 array(
			'id'        => 'heading_under_img_bg', 
			'type'      => 'background',
			'preview_height'      => '10px',
			'title'     => __('Header underline image or color', 'yachtsailing'),
            'required' => array('heading_underline_customize','equals','image_color')
		),
	 array(
			'id'        => 'heading_under_padding', 
			'type'      => 'spacing',
			'title'     => __('Header underline padding', 'yachtsailing'),
            'required' => array('heading_underline_customize','equals','image_color'),
             'units'          => array('em', 'px')
		),
	 array(
			'id'        => 'heading_under_margin', 
			'type'      => 'spacing',
			'title'     => __('Header underline margin', 'yachtsailing'),
            'required' => array('heading_underline_customize','equals','image_color'),
             'units'          => array('em', 'px')
		),
					
array(
    'id'     => 'whole_site_styling_ends',
    'type'   => 'section',
    'indent' => false,
),   					
					
array(
       'id' => 'section-start',
       'type' => 'section',
       'title' => __('HOMEPAGE HEADER STYLING OPTIONS', 'yachtsailing'),
       'indent' => true 
   ),
   
                     array(
					'id'        => 'home_menu_typo', 
					'type'      => 'typography',
					'title'     => __('Header menu typography', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing')
				),
               array(
					'id'        => 'home_sub_menu_typo', 
					'type'      => 'typography',
					'title'     => __('Header submenu typography', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing')
				),
array(
    'id'     => 'section-end',
    'type'   => 'section',
    'indent' => false,
),   					
array(
       'id' => 'section-start',
       'type' => 'section',
       'title' => __('INNERPAGE HEADER  STYLING OPTIONS', 'yachtsailing'),
       'subtitle' => __('Innerpage header styling options', 'yachtsailing'),
       'indent' => true 
   ),
   
               array(
					'id'        => 'inner_menu_typo', 
					'type'      => 'typography',
					'title'     => __('Header menu typography', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing')
				),
               array(
					'id'        => 'inner_sub_menu_typo', 
					'type'      => 'typography',
					'title'     => __('Header submenu typography', 'yachtsailing'),
					'subtitle'  => __('', 'yachtsailing')
				),
				
		array(
			'id'     => 'section-end',
			'type'   => 'section',
			'indent' => false,
		),
	)
);        
            $this->sections[] = array(
                'icon'      => 'el el-twitter',
                'title'     => esc_html('Social Media', 'yachtsailing'), 
				 'subtitle'  => esc_html('Remove # if you do not want to show any option', 'yachtsailing') ,                
                'fields'    => array(
				     array(
                        'id'        => 'twitter_url',
                        'type'      => 'text',                        
                        'title'     => __('Enter Twitter Url', 'yachtsailing'),
                        'subtitle'  => __('Put Your Twitter Url Here', 'yachtsailing'),                        
                        'default'   => '#'
                    ),
					 array(
                        'id'        => 'facebook_url',
                        'type'      => 'text',                        
                        'title'     => esc_html('Enter Facebook Url', 'yachtsailing'),
                        'subtitle'  => esc_html('Put Your Facebook Url Here', 'yachtsailing'),                        
                        'default'   => '#'
                    ),
					
					 array(
                        'id'        => 'gplus_url',
                        'type'      => 'text',                        
                        'title'     => esc_html('Enter Google Plus Url', 'yachtsailing'),
                        'subtitle'  => esc_html('Put Your Google Plus Url Here', 'yachtsailing'),                        
                        'default'   => '#'
                    ),
					 array(
                        'id'        => 'pinterest_url',
                        'type'      => 'text',                        
                        'title'     => esc_html('Enter Pinterest Url', 'yachtsailing'),
                        'subtitle'  => esc_html('Put Your Pinterest Url Here', 'yachtsailing'),                        
                        'default'   => '#'
                    ),
					 array(
                        'id'        => 'instagram_url',
                        'type'      => 'text',         
                        'title'     => esc_html('Enter Instagram Url', 'yachtsailing'),
                        'subtitle'  => esc_html('Put Your Instagram Url Here', 'yachtsailing'),                        
                        'default'   => ''
                    ),
					 array(
                        'id'        => 'youtube_url',
                        'type'      => 'text',         
                        'title'     => esc_html('Enter Youtube Url', 'yachtsailing'),
                        'subtitle'  => esc_html('Put Your Youtube Url Here', 'yachtsailing'),                        
                        'default'   => ''
                    ),
					 array(
                        'id'        => 'linkedin_url',
                        'type'      => 'text',         
                        'title'     => esc_html('Enter LinkedIn Url', 'yachtsailing'),
                        'subtitle'  => esc_html('Put Your LinkedIn Url Here', 'yachtsailing'),                        
                        'default'   => ''
                    ),
                
                ),
            );
	       $this->sections[] = array(
                'icon'      => 'el el-picture',
                'title'     => esc_html('Icons or Images ', 'yachtsailing'),                
                'desc'     => esc_html('Note: By default theme icons are used but from here you can upload your icons. ', 'yachtsailing'),                
                'fields'    => array(
						array(
						'id'=> 'twitter_icon',
						'type'     => 'media',
						'title'    => __('Twitter Icon', 'yachtsailing')
						),
						array(
						'id'=> 'facebook_icon',
						'type'     => 'media',
						'title'    => __('Facebook Icon', 'yachtsailing')
						),
						array(
						'id'=> 'gplus_icon',
						'type'     => 'media',
						'title'    => __('Google plus Icon', 'yachtsailing')
						),
						array(
						'id'=> 'pinterest_icon',
						'type'     => 'media',
						'title'    => __('Pinterest Icon', 'yachtsailing')
						),
						array(
						'id'=> 'instagram_icon',
						'type'     => 'media',
						'title'    => __('Instagram  Icon', 'yachtsailing')
						),
						array(
						'id'=> 'youtube_icon',
						'type'     => 'media',
						'title'    => __('Youtube  Icon', 'yachtsailing')
						),
						array(
						'id'=> 'linkedin_icon',
						'type'     => 'media',
						'title'    => __('LinkedIn Icon', 'yachtsailing')
						),
						array(
						'id'=> 'car_icon',
						'type'     => 'media',
						'title'    => __('Shopping cart in header', 'yachtsailing')
						),
						array(
						'id'=> 'phone_icon',
						'type'     => 'media',
						'title'    => __('Phone icon in header and footer', 'yachtsailing')
						),
						array(
						'id'=> 'email_icon',
						'type'     => 'media',
						'title'    => __('Email icon in header and footer', 'yachtsailing')
						),
						array(
						'id'=> 'title_unberline',
						'type'     => 'media',
						'title'    => __('Page titles underline image.', 'yachtsailing')
						),
				),
            );                
            
			
             $this->sections[] = array(
                'icon'      => 'el-icon-map-marker',
                'title'     => esc_html('Footer ', 'yachtsailing'),                
                'fields'    => array(
                 
	  
                    array(
                        'id'        => 'copyrighttext',
                        'type'      => 'textarea',
                        'title'     => esc_html('Copyright Text Here', 'yachtsailing'),
                  
                        'default'   => 'Copyright 2018 Yacht, All rights reserved. '
                    ),                            				
array(
       'id' => 'footer_columns-start',
       'type' => 'section',
       'title' => __('FOOTER COLUMNS', 'yachtsailing'),
       'indent' => true 
   ),
   array(
		'id'        => 'footer_column_switch', 
		'type'      => 'button_set',
		'title'     => __('Footer column switch', 'yachtsailing'),
		'subtitle'  => __('Select "Show" if you want to show color customizer, otherwise choose Hide', 'yachtsailing'),
		'options'   => array(
			"show"  => __("Show", 'yachtsailing'),
			"hide" => __("Hide", 'yachtsailing')
		),
		'default'   => 'hide'
	),                 
   
array(
       'id' => 'footer_columns-end',
       'type' => 'section',
       'indent' => false 
   ),
  ),
            );        
            // Import Export
            $this->sections[] = array(
                'title'     => esc_html('Import / Export', 'yachtsailing'),
                'desc'      => esc_html('Import and Export your Redux Framework settings from file, text or URL.', 'yachtsailing'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );                     
                    
           
            $this->sections[] = array(
                'icon'      => 'el-icon-info-sign',
                'title'     => esc_html('Theme Information', 'yachtsailing'),
                'desc'      => esc_html('<p class="description">This is the Description. Again HTML is allowed</p>', 'yachtsailing'),
                'fields'    => array(
                    array(
                        'id'        => 'opt-raw-info',
                        'type'      => 'raw',
                        'content'   => $item_info,
                    )
                ),
            );
        }
        public function setHelpTabs() {
            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => esc_html('Theme Information 1', 'yachtsailing'),
                'content'   => esc_html('<p>This is the tab content, HTML is allowed.</p>', 'yachtsailing')
            );
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => esc_html('Theme Information 2', 'yachtsailing'),
                'content'   => esc_html('<p>This is the tab content, HTML is allowed.</p>', 'yachtsailing')
            );
            // Set the help sidebar
            $this->args['help_sidebar'] = esc_html('<p>This is the sidebar content, HTML is allowed.</p>', 'yachtsailing');
        }
        /**
          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
         * */
        public function setArguments() {
            $theme = wp_get_theme(); // For use with some settings. Not necessary.
            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'theme_option',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html('Theme Options', 'yachtsailing'),
                'page_title'        => esc_html('Theme Options', 'yachtsailing'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyByw9j_PY1meWfxVmujxzrc7HhsQMvg_e4', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE
                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );
            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            $this->args['share_icons'][] = array(
                'url'   => '//github.com/ReduxFramework/ReduxFramework',
                'title' => 'Visit us on GitHub',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
            );
            $this->args['share_icons'][] = array(
                'url'   => '//www.facebook.com/pages/Redux-Framework/243141545850368',
                'title' => 'Like us on Facebook',
                'icon'  => 'el-icon-facebook'
            );
            $this->args['share_icons'][] = array(
                'url'   => '//twitter.com/reduxframework',
                'title' => 'Follow us on Twitter',
                'icon'  => 'el-icon-twitter'
            );
            $this->args['share_icons'][] = array(
                'url'   => '//www.linkedin.com/company/redux-framework',
                'title' => 'Find us on LinkedIn',
                'icon'  => 'el-icon-linkedin'
            );
            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace('-', '_', $this->args['opt_name']);
                }
               // $this->args['intro_text'] = sprintf(esc_html('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'yachtsailing'), $v);
            } else {
              //  $this->args['intro_text'] = esc_html('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'yachtsailing');
            }
            // Add content after the form.
           // $this->args['footer_text'] = esc_html('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'yachtsailing');
        }
    }
    
    global $reduxConfig;
    $reduxConfig = new Redux_Framework_sample_config();
}
/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;
/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';
        /*
          do your validation
          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */
        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;