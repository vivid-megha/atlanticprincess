<?php 
global $product, $woocommerce_loop;
/***Recent post widget with thumb**/
class yachtsailing_feature_product_Widget extends WP_Widget {
    /** constructor */
    function __construct() {
        parent::__construct( false, 'Woocommerce Feature Product Widget' );
    }
    /** @see WP_Widget::widget */
    function widget($args, $instance) {
        extract( $args );
        global $posttypes;
         $title          = apply_filters('widget_title', $instance['title']);
        $number         = apply_filters('widget_title', $instance['number']);
        ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
                            	<div class="shop-aside__featured">
                                <?php
                              $meta_query   = WC()->query->get_meta_query();
								    
									$meta_query[] = array(
										'key'   => '_featured',
										'value' => 'yes'
									);
									$args = array(
										'post_type'   =>  'product',
										'showposts'   =>  $number ,
										'orderby'     =>  'date',
										'order'       =>  'DESC',
										'meta_query'  =>  $meta_query
										
										
									);
								
                                    $myposts = get_posts( $args );
                                    foreach( $myposts as $post ) : setup_postdata($post);
									$thumbnail_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'yachtsailing-feature-product'); 
                                    ?> 
                                    
                                    
                                        <div class="shop-aside__product clearfix">
                                        <?php  if(!empty($thumbnail_url)){?>
                                        <div class="pull-right">
											<img src="<?php echo esc_url($thumbnail_url[0], 'yachtsailing');?>" alt='product'/>
										</div>
                                        <?php } ?>
                                        <div class="shop-aside__desc">
											<h4><a class="no-decoration" href="<?php the_permalink();?>"><?php the_title();?></a></h4>
											<div class="item-price"><?php echo  get_post_meta( get_the_ID(), '_regular_price', true);?></div>
										</div>
                                           
                                        </div>
                                    <?php endforeach; ?>
                                    <?php $post = $tmp_post; ?>
                           </div>
                        
              <?php echo $after_widget; ?>
        <?php
    }
    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {
        global $posttypes;
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['number'] = strip_tags($new_instance['number']);
         $instance['posttype'] = $new_instance['posttype'];
        return $instance;
    }
    /** @see WP_Widget::form */
    function form($instance) {
        $posttypes = get_post_types('', 'objects');
        $title =  esc_attr($instance['title'], 'yachtsailing');
        $number = esc_attr($instance['number'], 'yachtsailing');
       $posttype   = esc_attr($instance['posttype'], 'yachtsailing');
        ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo sanitize_text_field( $title );?>" />
        </p>
       <p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php esc_html_e('Number to Show Post:','yachtsailing'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo sanitize_text_field( $number ); ?>" />
        </p>
    
         <input  type="hidden" name="<?php echo $this->get_field_name('posttype'); ?>" value="<?php esc_html_e('post','yachtsailing')?>"/>
       
        <?php
    }
} 
function yachtsailing_feature_product_Widget() {
    register_widget( 'yachtsailing_feature_product_Widget' );
} 
add_action( 'widgets_init', 'yachtsailing_feature_product_Widget' );
