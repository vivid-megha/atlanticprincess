<?php 
  function yachtsailing_styles_method() 
  {
    wp_enqueue_style(
      'custom-style',
	    get_template_directory_uri() . '/assets/css/yachtsailing-custom.css'
	);
	global $theme_option; 				
    
    if(isset($theme_option))
    {
	  global $woocommerce;
	  global $post;		
	  
	  if(array_key_exists("whole_site_color_customization", $theme_option))
	  {
        $site_color_customization = $theme_option['whole_site_color_customization'];			
	  }
	  if($site_color_customization == 'yes')
	  {
        /* Primary color css starts here */
		if(array_key_exists("primary_color", $theme_option))
		{
           $primary_color = $theme_option['primary_color'];	  
		}
		
		if($primary_color != "")
		{
			          $primary_color_css = "					
								          a,h1, h2, h3, h4, h5, h6,
										  a:hover,
										  .y-landing .y-section_inner h2,
										  .y-landing .y-landing_list h3,
										  .y-header_outer address i,
										  .y-header_strip .y-header_form h3,
										  .y-header_strip .y-header_form .y-form > div.col-lg-4 > div i,
										  .y-lang_icons a, .y-login_but a,
										  .y-section_inner h2,
										  .y-contact_side form.y-form button,
										  .camera_prev > span:hover::before, .camera_next > span:hover::before,
										  .y-dest_list_single a:hover,
										  .y-boat_type .fa-anchor:hover,
										  .y-blogs .y-feature_post .y-blog_info h3,
										  .y-footer a:hover,
										  .y-footer .y-footer_strip p,
										  .y-footer .y-footer_strip .fa,
										  .y-header_01 .header .fa,
										  .y-header_01 .rmm.style > ul > li > a,
										  .y-home_02 .y-header_form h3,
										  .y-home_02 .y-header_form div.col-lg-12 button,
										  .y-home_02 .y-our_services .y-servie_info span,
										  .y-header_05 .header .fa,
										  .y-header_05 .rmm.style > ul > li > a,
										  .y-header_06 .header .fa,
										  .y-header_06 .rmm.style > ul > li > a,
										  .y-single_info_inner .y-dest_list h3,
										  .y-single_info_inner .y-dest_list h3 a,
										  .y-blog_detail_info h2,
										  .y-home_blog_content h3,
										  .y-product_listing > div a.y-heading,
										  .y-product_listing > div .y-adv_info_foot a,
										  .y-yacht_intro span,
										  .y-product_text_details h5,
										  .y-price_box span,
										  .y-relative_outer h3,
										  div.container .y-arrow_line i:hover,
										  .container .y-our_team h2,
										  .y-our_team_list .item:hover .y-team_name,
										  .y-slide_controls a,
										  .y-our_team_social a:hover,
										  .y-contact h3,
										  .y-contact form.y-form button,
										  .y-contact address a,
										  .rmm.style ul ul li a,
										  .rmm.style ul.rmm-mobile li a,
										  .rmm.style .rmm-mobile ul li a,.y-section_content h1
										  { 
										  	color:$primary_color;
										  }
						
						 .select-wrapper,
						.y-button:hover, .y-footer .y-special_offer ul li .y-read_more:hover, .y-home_blog_content .y-btn:hover,
						.y-contact_side hr,
						.y-contact_side form.y-form button,
						.y-dest_list_single h2:hover,
						.y-dest_list_single .y-button, .y-dest_list_single .y-footer .y-special_offer ul li .y-read_more, .y-footer .y-special_offer ul li .y-dest_list_single .y-read_more, .y-dest_list_single .y-home_blog_content .y-btn, .y-home_blog_content .y-dest_list_single .y-btn,
						.y-home_02 .y-header_form div.col-lg-12 button,
						.y-our_team_list .item:hover img.y-team_img,
						.y-slide_controls a,
						.y-slide_controls a:hover,.y-contact form.y-form button
								  { border-color: $primary_color;}
								/* Border Primary color ends here */	
					/* Border Left Primary color starts here */
					  .y-button, .y-footer .y-special_offer ul li .y-read_more, .y-home_blog_content .y-btn,
					  .y-footer form.y-form button
						{
							border-left-color:$primary_color; 
						}
					/* Border Left Primary color ends here */
						/* Background Primary color starts here */
						.select-wrapper,
						.y-button::before, .y-footer .y-special_offer ul li .y-read_more::before, .y-home_blog_content .y-btn::before,
						.header .fa:hover,
						form.y-form input,
						form.y-form textarea,
						.y-our_services .y-service_inner:hover .y-servie_info,
						.y-blogs .y-info_pop,
						.y-inner_page .y-sidebar,
						.y-blog_social_links ul li a:hover,
						.y-our_team_list .item:hover:after,
						.y-slide_controls a:hover,
						.y-menu_outer,
						.rmm ul ul li a:hover,
						.responsive-tabs__heading--active
						{ background: $primary_color; }
						/* Background Primary color starts here */													   
			               ";
			         wp_add_inline_style( 'custom-style', $primary_color_css );					   
        }
/* Primary color css ends here */
/* Secondary color css starts here */			
		if(array_key_exists("secondary_color", $theme_option))
		{
           $secondary_color = $theme_option['secondary_color'];	  
		}
	    if($secondary_color != "")
		{
          $secondary_color_css = "
                                  .y-landing .y-button, .y-landing .y-footer .y-special_offer ul li .y-read_more,
                                  .y-footer .y-special_offer ul li .y-landing .y-read_more,
                                  .y-landing .y-home_blog_content .y-btn,
                                  .y-home_blog_content .y-landing .y-btn,
	                              .y-header_strip .y-header_form .y-button, 
	                              .y-header_strip .y-header_form .y-footer .y-special_offer ul li .y-read_more, 
	                              .y-footer .y-special_offer ul li .y-header_strip .y-header_form .y-read_more, 
	                              .y-header_strip .y-header_form .y-home_blog_content .y-btn, 
	                              .y-home_blog_content .y-header_strip .y-header_form .y-btn,
		                          .y-dest_list_single .y-button, 
		                          .y-dest_list_single .y-footer .y-special_offer ul li .y-read_more, 
		                          .y-footer .y-special_offer ul li .y-dest_list_single .y-read_more, 
		                          .y-dest_list_single .y-home_blog_content .y-btn, 
		                          .y-home_blog_content .y-dest_list_single .y-btn,
		                          .y-boat_type .owl-controls::after,
		                          .y-blogs .y-feature_post .y-blog_info a.y-button, 
		                          .y-blogs .y-feature_post .y-blog_info .y-footer .y-special_offer ul li a.y-read_more, 
		                          .y-footer .y-special_offer ul li .y-blogs .y-feature_post .y-blog_info a.y-read_more, 
		                          .y-blogs .y-feature_post .y-blog_info .y-home_blog_content a.y-btn, 
		                          .y-home_blog_content .y-blogs .y-feature_post .y-blog_info a.y-btn,
			                      .y-side_search input,
			                      .tagcloud a,
			                      .tagcloud a:hover,
			                      .y-home_blog_content .y-btn,  
			                      .y-yacht_intro .y-button, 
			                      .y-yacht_intro .y-footer .y-special_offer ul li .y-read_more, 
			                      .y-footer .y-special_offer ul li .y-yacht_intro .y-read_more, 
			                      .y-yacht_intro .y-home_blog_content .y-btn, 
			                      .y-home_blog_content .y-yacht_intro .y-btn,
			                      .noUi-background,
			                      .y-price_box a,
			                      .rmm-toggled-button span,
			                      .owl-theme .owl-dots .owl-dot.active span, 
			                      .owl-theme .owl-dots .owl-dot:hover span
	                            {
		                           background: $secondary_color !important;
	                            }
	                            .y-header_outer address a:hover,
	                            .y-blogs .y-info_pop a:hover,
	                            .y-inner_page .y-sidebar li a:hover,
	                            .y-inner_page .y-sidebar .y-side_posts_text > a:hover,
	                            .y-product_filters .y-single_filter li span:hover,
	                            .owl-theme .owl-controls .owl-nav [class*='owl-']
	                            {
			                      color: $secondary_color !important;
	                            }	  
                                .y-side_search input,
                                .y-home_blog_content .y-btn
                                { 
                                  border-color: $secondary_color !important; 
                                }				 
				               ";
                  wp_add_inline_style( 'custom-style', $secondary_color_css );					   
			}
		  }
/* Secondary color css ends here */
/* BOXED Layout background styling starts here */
  $layout = isset($theme_option['layout_swtich']) ? $theme_option['layout_swtich'] : null; 
	if(is_page())	
	{
	    $page_layout = get_post_meta($post->ID,"_cmb_page_layout",true);	
	}
	else
	{
		$page_layout = "";
	}
	if($layout == "boxed" || $page_layout == "page_boxed")
	{
            $boxed_background = "";
        
			    if(array_key_exists("boxed_background", $theme_option))
				{
				  $boxed_background = $theme_option['boxed_background'];	 
				}
				
			    if(array_key_exists("background-color", $boxed_background))
				{
				  $bgcolor = $boxed_background['background-color'];	 
				}
			
			    if(array_key_exists("bgrepeat", $theme_option))
				{
				  $bgrepeat = $theme_option['bgrepeat'];	 
				}				
			  
			    if(array_key_exists("bgrepeat", $theme_option))
				{
				  $bgrepeat = $theme_option['bgrepeat'];	 
				}
			    $bgsize = "";
			    if(array_key_exists("background-size", $boxed_background))
				{
				  $bgsize = $boxed_background['background-size'];	 
				}
			  
			    if(array_key_exists("background-attachment", $boxed_background))
				{
				  $bgattachment = $boxed_background['background-attachment'];	 
				}
				 
				  if($bgsize != "")
				  {			  
					if(array_key_exists("background-position", $boxed_background))
					{
					  $bgposition = $boxed_background['background-position'];	 
					}
					
				    $bgposition = $bgposition."/";
				  }
				  else
				  {
			  
					if(array_key_exists("background-position", $boxed_background))
					{
					  $bgposition = $boxed_background['background-position'];	 
					}
					
				  }
			  
					if(array_key_exists("background-image", $boxed_background))
					{
					  $bgimage = $boxed_background['background-image'];	 
					}
								  
				  $bgid = $boxed_background["media"]["id"];
				  $bgheight = $boxed_background["media"]["height"];
				  $bgwidth = $boxed_background["media"]["width"];
				  $bgstyling = $bgcolor."  "."url(".$bgimage.") ".$bgposition." ".$bgsize."  ".$bgattachment." ";
				  $box_layout_bg_css = "body.y-boxed{ background: $bgstyling !important; }";
				  wp_add_inline_style( 'custom-style', $box_layout_bg_css );		  
	}
			  
// Homepage template conditions starts here 
if(is_page_template( 'page-templates/template-home.php' ))
{
/* Homepage header background color styling starts here */
	if(array_key_exists("home_header_sticky", $theme_option))
	{
	  $home_header_sticky = $theme_option['home_header_sticky'];	 
	}
	else
	{
	  $home_header_sticky = "";
	}
	 
	 if($home_header_sticky == "yes")
	 {
		$stickyclass = "y-yessticky";	
		if(array_key_exists("home_sticky_header_bg", $theme_option))
		{
		  $home_sticky_header_bg = $theme_option['home_sticky_header_bg'];	 
		}
		else
		{
		  $home_sticky_header_bg = "";
		}
		
		  $bgcolor = $home_sticky_header_bg['background-color'];	 
		  $bgrepeat = $home_sticky_header_bg['background-repeat'];	 
		  $bgsize = $home_sticky_header_bg['background-size'];	 
		  $bgattachment = $home_sticky_header_bg['background-attachment'];
		  	 
		if($bgsize != "")
		  {	
		    $bgposition = $home_sticky_header_bg['background-position'];	 
		    $bgposition = "";		  
		    $bgposition = $bgposition."/";
		  }
		  else
		  {
			  $bgposition = $home_sticky_header_bg['background-position'];	 
		  }
		 
		 $bgimage = $home_sticky_header_bg['background-image'];	 
		 $bgid = $home_sticky_header_bg["media"]["id"];
		 $bgheight = $home_sticky_header_bg["media"]["height"];
		 $bgwidth = $home_sticky_header_bg["media"]["width"];
		 
		 $bgstyling = $bgcolor."  "."url(".$bgimage.") ".$bgposition." ".$bgsize." ".$bgrepeat." ".$bgattachment." ";
		 $home_header_sticky_bg_css =  ".$stickyclass .header{ background: $bgstyling !important; }";
	     wp_add_inline_style( 'custom-style', $home_header_sticky_bg_css );	
	 }
       else if(($home_header_sticky == "no"))
		 {
			   $stickyclass = "y-notsticky";
	        if(array_key_exists("home_header_bg", $theme_option))
			{
			  $header_bg = $theme_option['home_header_bg'];	 
			}
			else
			{
			  $header_bg = "";
			}
			  $bgcolor = $header_bg['background-color'];	 
			  $bgrepeat = $header_bg['background-repeat'];	 
			  $bgsize = $header_bg['background-size'];	 
			  $bgattachment = $header_bg['background-attachment'];	 
				if($bgsize != "")
				  {
	
					  $bgposition = $header_bg['background-position'];	 
				      $bgposition = $bgposition."/";
				  }
				  else
				  {
				 $bgposition = $header_bg["background-position"];					  
				  }
	
					  $bgimage = $header_bg['background-image'];	 
				 $bgid = $header_bg["media"]["id"];
				 $bgheight = $header_bg["media"]["height"];
				 $bgwidth = $header_bg["media"]["width"];
				 
				  $bgstyling = $bgcolor."  "."url(".$bgimage.") ".$bgposition." ".$bgsize." ".$bgrepeat." ".$bgattachment." ";
				$home_header_nosticky_bg_css =  ".$stickyclass .header{ background: $bgstyling !important; }";
				wp_add_inline_style( 'custom-style', $home_header_nosticky_bg_css );	
		 }
 		 else
		 {
			 $header_bg = "";
			 $stickyclass = "";
		 }
/* Homepage header background color styling ends here */	
/* Homepage header menu styling starts here */
		if(array_key_exists("home_menu_typo", $theme_option))
		{
           $home_menu_typo = $theme_option['home_menu_typo'];
	    }
		else
		{
	       $home_menu_typo = "";
		}
		
  if($home_menu_typo != "")
  {
  
	 $hmenu_font_family = $home_menu_typo["font-family"];
	 $hmenu_font_options = $home_menu_typo["font-options"];
	 $hmenu_google = $home_menu_typo["google"];
	 $hmenu_font_weight = $home_menu_typo["font-weight"];
	 $hmenu_font_style = $home_menu_typo["font-style"];
	 $hmenu_subsets = $home_menu_typo["subsets"];
	 $hmenu_text_align = $home_menu_typo["text-align"];
	 $hmenu_font_size = $home_menu_typo["font-size"];
	 $hmenu_line_height = $home_menu_typo["line-height"];
	 $hmenu_color = $home_menu_typo["color"];	 
		$home_menu_typo_css =  ".y-menu_outer .rmm.style > ul > li > a{
			                  color: $hmenu_color;  !important;
						font-family: $hmenu_font_family !important;
						font_weight: $hmenu_font_family !important;
						font-style: $hmenu_font_family !important;
						
						text-align: $hmenu_text_align !important;
						font-size: $hmenu_font_size !important;
						line-height: $hmenu_line_height !important;
			}";
		wp_add_inline_style( 'custom-style', $home_menu_typo_css );		 
  }
/* Homepage header menu styling ends here */  
/* Homepage header submenu styling starts here */
  
	if(array_key_exists("home_sub_menu_typo", $theme_option))
	{
	   $home_sub_menu_typo = $theme_option['home_sub_menu_typo'];
	}
	else
	{
	   $home_sub_menu_typo = "";
	}
  if($home_sub_menu_typo != "")
  {
	 $hsmenu_font_family = $home_sub_menu_typo["font-family"];
	 $hsmenu_font_options = $home_sub_menu_typo["font-options"];
	 $hsmenu_google = $home_sub_menu_typo["google"];
	 $hsmenu_font_weight = $home_sub_menu_typo["font-weight"];
	 $hsmenu_font_style = $home_sub_menu_typo["font-style"];
	 $hsmenu_subsets = $home_sub_menu_typo["subsets"];
	 $hsmenu_text_align = $home_sub_menu_typo["text-align"];
	 $hsmenu_font_size = $home_sub_menu_typo["font-size"];
	 $hsmenu_line_height = $home_sub_menu_typo["line-height"];
	 $hsmenu_color = $home_sub_menu_typo["color"];	
	 
	 $home_submenu_typo_css =  "
								  .rmm.style > ul ul >li> a
										{
										 color:  $hsmenu_color   !important;
										 font-family:  $hsmenu_font_family   !important;
										 font_weight:  $hsmenu_font_family   !important;
										 font-style:  $hsmenu_font_family   !important;
						
										 text-align:  $hsmenu_text_align   !important;
										 font-size:  $hsmenu_font_size   !important;
										 line-height:  $hsmenu_line_height   !important;
										 
										}
	 ";
	 wp_add_inline_style( 'custom-style', $home_submenu_typo_css );		 
  }
/* Homepage header submenu styling ends here */  
}
else
{
/* Innerpage header background color styling starts here */
		if(array_key_exists("inner_header_sticky", $theme_option))
		{
		  $inner_header_sticky = $theme_option['inner_header_sticky'];	 
		}
		else
		{
		  $inner_header_sticky = "";
		}
		 if($inner_header_sticky == "yes" )
		 {
			 $stickyclass = "y-yessticky";
		 
				if(array_key_exists("inner_sticky_header_bg", $theme_option))
				{
				  $inner_sticky_header_bg = $theme_option['inner_sticky_header_bg'];	 
				}
				else
				{
				  $inner_sticky_header_bg = "";
				}
		 
				$bgcolor = $inner_sticky_header_bg['background-color'];	
				$bgrepeat = $inner_sticky_header_bg['background-repeat'];
				$bgsize = $inner_sticky_header_bg['background-size'];	 
				
			 
				 $bgattachment = $inner_sticky_header_bg["background-attachment"];
				if($bgsize != "")
				  {
				 $bgposition = $inner_sticky_header_bg["background-position"];
				 $bgposition = $bgposition."/";
				  }
				  else
				  {
				 $bgposition = $inner_sticky_header_bg["background-position"];					  
				  }
				 $bgimage = $inner_sticky_header_bg["background-image"];
				 $bgid = $inner_sticky_header_bg["media"]["id"];
				 $bgheight = $inner_sticky_header_bg["media"]["height"];
				 $bgwidth = $inner_sticky_header_bg["media"]["width"];
				 
				  $bgstyling = $bgcolor."  "."url(".$bgimage.") ".$bgposition." ".$bgsize." ".$bgrepeat." ".$bgattachment." ";
				  $inner_header_sticky_css =  ".y-yessticky .header{ background: $bgstyling !important; }";
				  wp_add_inline_style( 'custom-style', $inner_header_sticky_css );		 
		 }
		 else if(($inner_header_sticky == "no"))
		 {
			 $stickyclass = "y-notsticky";		
				if(array_key_exists("inner_header_bg", $theme_option))
				{
				  $header_bg_no = $theme_option['inner_header_bg'];	 
				}
				else
				{
				  $header_bg_no = "";
				}
				
				 $bgcolor = $header_bg_no["background-color"];
				 $bgrepeat = $header_bg_no["background-repeat"];
				 $bgsize = $header_bg_no["background-size"];
				 $bgattachment = $header_bg_no["background-attachment"];
				if($bgsize != "")
				  {
				 $bgposition = $header_bg_no["background-position"];
				 $bgposition = $bgposition."/";
				  }
				  else
				  {
				 $bgposition = $header_bg_no["background-position"];					  
				  }
				 $bgimage = $header_bg_no["background-image"];
				 $bgid = $header_bg_no["media"]["id"];
				 $bgheight = $header_bg_no["media"]["height"];
				 $bgwidth = $header_bg_no["media"]["width"];
				 
				  $bgstyling = $bgcolor."  "."url(".$bgimage.") ".$bgposition." ".$bgsize." ".$bgrepeat." ".$bgattachment." ";
				  $inner_header_nosticky_css =  ".y-notsticky .header{ background: $bgstyling  !important; }";
				  wp_add_inline_style( 'custom-style', $inner_header_nosticky_css );		 				  
		 }
		 else
		 {
			 $header_bg = "";
			 $stickyclass = "";
		 }
				if(array_key_exists("inner_header", $theme_option))
				{
				  $inner_header = $theme_option['inner_header'];	 
				}
$header_type = "";	
if(is_page())		 
{
	$header_type = get_post_meta($post->ID, "_cmb_header_type", true);
}
else if(is_single())
{
    $header_type = $inner_header;
}
else
{
  $inner_header = "";
}
	if($header_type == "header_default")
	{
		$in_header_type = $inner_header;
		if($inner_header == "theme_defaults")
		{
			$in_header_type = "header_inner";
		}
	}
	
	else
	{
	   $in_header_type = $header_type;
	}
		if(array_key_exists("inner_header_sticky", $theme_option))
		{
		  $inner_header_sticky = $theme_option['inner_header_sticky'];	 
		}
		else
		{
		  $inner_header_sticky = "";
		}
		   
	  if($inner_header_sticky == "yes" || $inner_header_sticky == "default" )
	  {
		if(
 $in_header_type == "header_first" || $in_header_type == "header_second" ||  $in_header_type == "header_fourth" ||  $in_header_type == "header_fifth")
		   {
				 if($in_header_type  == "header_inner")
				 {
                    $inner_header_cond3_css =  "#y-single_info.breadcrubme_section{ margin-top:0px}";
				 }
				 else
				 {
                    $inner_header_cond3_css =  "#y-single_info.breadcrubme_section{ margin-top:200px}";
				 }
				 
				  wp_add_inline_style( 'custom-style', $inner_header_cond3_css);		 				
           if(isset($woocommerce))
           {
				if(is_shop() || is_single())
				{
				  $inner_header_cond4_css =  ".y-yessticky ~ .y-inner_page, .y-notsticky ~ .y-inner_page{ margin-top:200px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond4_css);		 				
				}
			}
		   }
	   else if($in_header_type == "header_third")
		   {
				  $inner_header_cond5_css =  "#y-single_info.breadcrubme_section{ margin-top:270px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond5_css);		 				
				  if(isset($woocommerce))
				  {
					  if(is_shop() || is_single())
					  {
					  $inner_header_cond6_css =  ".y-yessticky ~ .y-inner_page, .y-notsticky ~ .y-inner_page{ margin-top:270px}";
					  wp_add_inline_style( 'custom-style', $inner_header_cond6_css);		 				
					  }
				}
		   }
	   else if($in_header_type == "header_sixth")
		  {
				  $inner_header_cond7_css =  "#y-single_info.breadcrubme_section{ margin-top:270px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond7_css);		 				
				if(isset($woocommerce)){
				  if(is_shop() || is_single())
				  {
				  $inner_header_cond8_css =  ".y-yessticky ~ .y-inner_page, .y-notsticky ~ .y-inner_page{ margin-top:120px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond8_css);		 				
				  }
				}
		  }
		   
		 }
else if($inner_header_sticky == "no")
{
	if($inner_header == "theme_defaults")
	{
		   if($in_header_type == "header_first" || $in_header_type == "header_second" ||  $in_header_type == "header_fourth" ||  $in_header_type == "header_fifth" )
		   {
				  $inner_header_cond9_css =  "@media (min-width: 768px){ #y-single_info.breadcrubme_section{ margin-top:100px !important}}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond9_css);		 				
		   }
		   else if($in_header_type == "header_third" || $in_header_type == "header_sixth")
		   {
				  $inner_header_cond10_css =  "#y-single_info.breadcrubme_section{ margin-top:100px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond10_css);		 				
		   }
	}
		  else if(
  $in_header_type == "header_first" || $in_header_type == "header_second" ||  $in_header_type == "header_fourth" ||  $in_header_type == "header_fifth"
		   )
			{
				  $inner_header_cond11_css =  "#y-single_info.breadcrubme_section{ margin-top:100px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond11_css);		 				
				 if(isset($woocommerce)){
				  if(is_shop() || is_single())
				  {
				  $inner_header_cond12_css =  ".y-yessticky ~ .y-inner_page, .y-notsticky ~ .y-inner_page{ margin-top:100px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond12_css);		 				
				  }
				}
			}
		   else if($in_header_type == "header_third" )
		   {
				  $inner_header_cond13_css =  "#y-single_info.breadcrubme_section{ margin-top:100px}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond13_css);		 							   	
				if(isset($woocommerce)){
				  if(is_shop() || is_single())
				  {
				  $inner_header_cond14_css =  ".y-yessticky ~ .y-inner_page, .y-notsticky ~ .y-inner_page{ margin-top:100px;}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond14_css);		 							   						  					  
				  }
				}
		   }
		  else if($in_header_type == "header_sixth")
		  {
				  $inner_header_cond15_css =  "#y-single_info.breadcrubme_section{ margin-top:100px;}";
				  wp_add_inline_style( 'custom-style', $inner_header_cond15_css);		 							   						  					  			  	               
				  if(isset($woocommerce)){
				  if(is_shop() || is_single())
		           {
					  $inner_header_cond16_css =  ".y-yessticky ~ .y-inner_page, .y-notsticky ~ .y-inner_page{ margin-top:120px}";
				      wp_add_inline_style( 'custom-style', $inner_header_cond16_css);
				   }
				}
		  }
		}
		
		if(array_key_exists("gap_header_menu_content", $theme_option))
		{
		  $gap_header_menu_content = $theme_option['gap_header_menu_content'];	 
		}
		else
		{
		  $gap_header_menu_content = "";
		}
	
		if(array_key_exists("header_featured", $theme_option))
		{
           $header_featured = $theme_option['header_featured'];	  
		}
		else
		{
		   $header_featured = "";
		}		  
		
		
		if(array_key_exists("gap_header_menu_content", $theme_option))
		{
		  $header_featured_swtich = $theme_option['header_featured_swtich'];	 
		}
		else
		{
		  $header_featured_swtich = "";
		}
		
	 if($header_featured != "")
	 {		 			 
		 $bgcolor = $header_featured["background-color"];
		 $bgrepeat = $header_featured["background-repeat"];
		 $bgsize = $header_featured["background-size"];
		 $bgattachment = $header_featured["background-attachment"];
		 $bgposition = $header_featured["background-position"];
		 $bgimage = $header_featured["background-image"];
		 $bgid = $header_featured["media"]["id"];
		 $bgheight = $header_featured["media"]["height"];
		 $bgwidth = $header_featured["media"]["width"];
		 $header_featured_bgstyling = $bgcolor." ".$bgrepeat." ".$bgsize." ".$bgattachment." ".$bgposition." "."url(".$bgimage.")";	
		 
		 if($header_featured_swtich == "custom")
		 {
			  $gap_header_menu_content =  ".y-inner_header{background: $header_featured_bgstyling !important}";
			  wp_add_inline_style( 'custom-style', $gap_header_menu_content);
		 }
              $header_padding_bottom_variable = $gap_header_menu_content['padding-bottom'];
			  $header_padding_bottom_css =  ".y-inner_header{ padding-bottom: $header_padding_bottom_variable }";
			  wp_add_inline_style( 'custom-style', $header_padding_bottom_css);      
	}
					  
/* Innerpage header menu styling starts here */
		if(array_key_exists("inner_menu_typo", $theme_option))
		{
		  $inner_menu_typo = $theme_option['inner_menu_typo'];	 
		}
		else
		{
		  $inner_menu_typo = "";
		}
  
  if($inner_menu_typo != "")
  {
	 $inmenu_font_family = $inner_menu_typo["font-family"];
	 $inmenu_font_options = $inner_menu_typo["font-options"];
	 $inmenu_google = $inner_menu_typo["google"];
	 $inmenu_font_weight = $inner_menu_typo["font-weight"];
	 $inmenu_font_style = $inner_menu_typo["font-style"];
	 $inmenu_subsets = $inner_menu_typo["subsets"];
	 $inmenu_text_align = $inner_menu_typo["text-align"];
	 $inmenu_font_size = $inner_menu_typo["font-size"];
	 $inmenu_line_height = $inner_menu_typo["line-height"];
	 $inmenu_color = $inner_menu_typo["color"];	 
	  $header_menu_typo_css =  ".rmm.style > ul > li > a
	                             {
									 color:     $inmenu_color   !important;
									 font-family:  $inmenu_font_family   !important;
									 font_weight:  $inmenu_font_family   !important;
									 font-style:  $inmenu_font_family   !important;
					
									 text-align:  $inmenu_text_align   !important;
									 font-size:  $inmenu_font_size  !important;
									 line-height:  $inmenu_line_height   !important;
				                  }
                                ";
      wp_add_inline_style( 'custom-style', $header_menu_typo_css);
  }
/* Innerpage header menu styling ends here */		
/* Innerpage header menu styling starts here */
		
		if(array_key_exists("inner_sub_menu_typo", $theme_option))
		{
		  $inner_sub_menu_typo = $theme_option['inner_sub_menu_typo'];	 
		}
		else
		{
		  $inner_sub_menu_typo = "";
		}
  if($inner_sub_menu_typo != "")
  {
	 $insmenu_font_family = $inner_sub_menu_typo["font-family"];
	 $insmenu_font_options = $inner_sub_menu_typo["font-options"];
	 $insmenu_google = $inner_sub_menu_typo["google"];
	 $insmenu_font_weight = $inner_sub_menu_typo["font-weight"];
	 $insmenu_font_style = $inner_sub_menu_typo["font-style"];
	 $insmenu_subsets = $inner_sub_menu_typo["subsets"];
	 $insmenu_text_align = $inner_sub_menu_typo["text-align"];
	 $insmenu_font_size = $inner_sub_menu_typo["font-size"];
	 $insmenu_line_height = $inner_sub_menu_typo["line-height"];
	 $insmenu_color = $inner_sub_menu_typo["color"];	 
	  $header_menu_typo_css =  "
								  .rmm.style ul ul li a
										{
										 color:  $insmenu_color   !important;
										 font-family:  $insmenu_font_family   !important;
										 font_weight:  $insmenu_font_family   !important;
										 font-style:  $insmenu_font_family   !important;
						
										 text-align:  $insmenu_text_align   !important;
										 font-size:  $insmenu_font_size   !important;
										 line-height:  $insmenu_line_height   !important;				 
										}
	           ";
      wp_add_inline_style( 'custom-style', $header_menu_typo_css);	  	 
  }
		
   }
   
		
		if(array_key_exists("heading_underline_customize", $theme_option))
		{
		  $h_under_opt_yes = $theme_option['heading_underline_customize'];	 
		}
		else
		{
		  $h_under_opt_yes = "";
		}
	if($h_under_opt_yes == "color_customization")
	{
		
		if(array_key_exists("heading_underline", $theme_option))
		{
		  $heading_underline = $theme_option['heading_underline'];	 
		}
		else
		{
		  $heading_underline = "";
		}
	   
	   $heading_underline_bg = $heading_underline["color"];
	  $heading_underline_bg_css =  "
									.y-section_content h1::before, .y-section_content h1::after, 
									 h3::before, h3::after,
									.y-section_inner h2::before, .y-section_inner h2::after
									{ background: $heading_underline_bg !important; }
	                           ";
      wp_add_inline_style( 'custom-style', $heading_underline_bg_css);	  	 							   	   
	}
	else  if($h_under_opt_yes == "image_color")
	{
	             $heading_under_bg = $theme_option['heading_under_img_bg'];		
				 $bgcolor = $heading_under_bg["background-color"];
				 $bgrepeat = $heading_under_bg["background-repeat"];
				 $bgsize = $heading_under_bg["background-size"];
				 $bgattachment = $heading_under_bg["background-attachment"];
				 $bgposition = $heading_under_bg["background-position"];
				 $bgimage = $heading_under_bg["background-image"];
				 $bgid = $heading_under_bg["media"]["id"];
				 $bgheight = $heading_under_bg["media"]["height"];
				 $bgwidth = $heading_under_bg["media"]["width"];
				 $bgstyling = $bgcolor." ".$bgrepeat." ".$bgsize." ".$bgattachment." ".$bgposition." "."url(".$bgimage.")";
	  $h_under_opt_yes_css =  "
									.y-section_content h1::after, 
									 h3::after,
									.y-section_inner h2::after
										  { 
											 border:0;
											 background-image:url(  $bgimage ) !important; 
											 background-color:  $bgcolor  !important;
											 background-repeat:   $bgrepeat  !important;
											 background-size:   $bgsize  !important;
											 background-attachment:   $bgattachment  !important;
											 background-position:   $bgposition  !important;
											 position:absolute !important;
										  }
										  
									.y-section_content h1::before, 
									 h3::before,
									.y-section_inner h2::before{ background: ''; height:0; width:0; }
	                          ";	
      wp_add_inline_style( 'custom-style', $h_under_opt_yes_css);	  	 							   	   
	
		if(array_key_exists("heading_under_padding", $theme_option))
		{
		  $heading_under_padding = $theme_option['heading_under_padding'];	 
		}
		else
		{
		  $heading_under_padding = "";
		}
  
  if($heading_under_padding != "")
  {
	    $hu_padding_top = $heading_under_padding["padding-top"];
	    $hu_padding_right = $heading_under_padding["padding-right"];
	    $hu_padding_bottom = $heading_under_padding["padding-bottom"];
	    $hu_padding_left = $heading_under_padding["padding-left"];
	  $heading_under_padding_css =  "
									.y-section_content h1::after, 
									 h3::after,
									.y-section_inner h2::after
									{
										position:relative !important;
										padding-top:  $hu_padding_top  !important;
										padding-right:  $hu_padding_right  !important;
										padding-bottom:  $hu_padding_bottom  !important;
										padding-left:  $hu_padding_left  !important;
										position:relative !important;
									}
	                          ";		
      wp_add_inline_style( 'custom-style', $heading_under_padding_css);	  	 							   	   
							  
  }
  
		if(array_key_exists("heading_underline_customize", $theme_option))
		{
		  $h_under_opt_yes = $theme_option['heading_underline_customize'];	 
		}
		else
		{
		  $h_under_opt_yes = "";
		}
  
		if(array_key_exists("heading_under_margin", $theme_option))
		{
		  $heading_under_margin = $theme_option['heading_under_margin'];	 
		}
		else
		{
		  $heading_under_margin = "";
		}
  
  
  if($heading_under_margin != "")
  {
	    $hu_margin_top = $heading_under_margin["padding-top"];
	    $hu_margin_right = $heading_under_margin["padding-right"];
	    $hu_margin_bottom = $heading_under_margin["padding-bottom"];
	    $hu_margin_left = $heading_under_margin["padding-left"];
	  $heading_under_margin_css =  "
										.y-section_content h1::after, 
										 h3::after,
										.y-section_inner h2::after
										{
											position:relative !important;
											margin-top:  $hu_margin_top  !important;
											margin-right:  $hu_margin_right  !important;
											margin-bottom:  $hu_margin_bottom  !important;
											margin-left:  $hu_margin_left  !important;
											position:relative !important
										}
	                                ";	
      wp_add_inline_style( 'custom-style', $heading_under_margin_css);	  	 							   	   
										
  }
  
	}
if($h_under_opt_yes == "no" || $h_under_opt_yes == "color_customization" )
{
      $h_under_opt_color_css =  "	
										.y-section_content h1::after, 
										 h3::after,
										.y-section_inner h2::after
										{
											position:absolute !important;
										}
	                               ";
      wp_add_inline_style( 'custom-style', $h_under_opt_color_css);	  	 							   	   	
}   
if(is_tag())
{
 $tag_bread_css =  "	.y-breadcrum { display: none; }";
      wp_add_inline_style( 'custom-style', $tag_bread_css);	  	 							   	   	 
}   
        $site_width = $theme_option['site_width'];	
        if(array_key_exists("width", $site_width))
        {  
            $site_widthpx = $site_width['width'];	 			  			  
            if($site_widthpx == '' || $site_widthpx == 'px' || $site_widthpx == 'em' || $site_widthpx == '%')
            {
                $site_widthpx = "1240px";
            }
			$site_width_css = ".container, .y-boxed-inner, .y-boxed .y-header_strip .header{width: $site_widthpx; }";
			wp_add_inline_style( 'custom-style', $site_width_css);	  	 							   	   	 
        }
        
    $load_icon_image = "";
    $load_background = "";
    $load_icon_val = $theme_option["load_icon_val"];
	if($load_icon_val == 1)
	{		
		if(array_key_exists("load_icon_image",$theme_option))
		{
		  $load_icon_image = esc_url($theme_option['load_icon_image']["url"], 'yachtsailing');
		}
		else
		{
		  $load_icon_image = "";
		}
		 if($load_icon_image != "")
		 {
			$load_background = 'url('.$load_icon_image.') no-repeat scroll center center';
			$site_loader_css = "#page-preloader .spinner{background: $load_background; }";
			wp_add_inline_style( 'custom-style', $site_loader_css);	  	 							   	   	 
		 }  
   }
 }
      
 $html_tag_style = "html{ margin-top:32px !important;}";
 wp_add_inline_style( 'html-tag-style', $html_tag_style);	  
}
add_action( 'wp_enqueue_scripts', 'yachtsailing_styles_method' );