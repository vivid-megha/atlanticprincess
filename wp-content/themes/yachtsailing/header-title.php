<?php   
  global $theme_option;
?>
<section id="y-single_info" class="breadcrubme_section">
    <div class="y-single_info">
        <div class="container">
            <div class="row y-single_info_inner y-section_content">
                <div class="row clearfix">
                    <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                            <h1 class="y-heading">
                                <?php                                 
                                           $post_type =  get_post_type();
                                           if(is_archive())
                                            {                                    
                                               the_archive_title();                            
                                            }
                                          else if(!is_single() && $post_type == "product")
                                           {
                                             echo esc_html("Shop", 'yachtsailing');                                                                              
                                           }
                                           else if(is_search())                     
                                          {                                   
                                            echo esc_html("Search", 'yachtsailing');                                  
                                          }  
                                
                                          else                                   
                                          {                                   
                                           esc_html(the_title(), 'yachtsailing');                                 
                                          }                            
                                        ?>
                            </h1>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                            <?php yachtsailing_breadcrumbs(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>