<?php   
   global $theme_option;   
   global $woocommerce;    
   
   if(isset($theme_option))
    {
      $layout = isset($theme_option['layout_swtich']) ? $theme_option['layout_swtich'] : null;     
	  $page_layout = get_post_meta($post->ID,"_cmb_page_layout",true);    
	  $box_class = "";    
	  $background = ""; 
	 
	  if($page_layout == "page_boxed")  
	  {   
	    $box_class = "y-boxed";       
	  }    
	 
	  if($layout == 'boxed')    
	  {   
	    $box_class = "y-boxed";    
	  }  
    }
?>
<body>
  <?php 
    $home_template = "page-templates/template-home.php";
      if(is_page_template($home_template))
      {
        echo '<div class="custom_home_page_class">';
      }
      else
      {
        echo '<div class="custom_inner_page_class">';    
      }
    
    if($box_class != "")
    { 
       echo '<div class="y-boxed-inner">';
    }    
  ?>
	<div id="page-preloader"> 
	   <span class="spinner"></span>
	   <span class="loading"></span>
	</div>        
<?php 
  if(is_page_template( 'page-templates/template-home.php' ))
  {     
    $home_header_sticky = $theme_option['home_header_sticky'];
	if($home_header_sticky == "yes")    
	{
	  $stickyclass = "y-yessticky";     
	}
	else if($home_header_sticky == "no")    
	{
	  $stickyclass = "y-notsticky";     
	}     
	else    
	{       
	  $stickyclass = "y-yessticky";     
	}
  }
  else
    {     
	  $inner_header_sticky = $theme_option['inner_header_sticky'];    
	   
	  if($inner_header_sticky == "yes")     
	  {       
	    $stickyclass = "y-yessticky";     
	  }     
	  else if($inner_header_sticky == "no")     
	  {
	    $stickyclass = "y-notsticky";     
	  }     
	  else    
	  {
	    $stickyclass = "y-yessticky";     
	  }
	}
?>    
<div id="skrollr-body" class="<?php echo $stickyclass; ?>  y-header_strip y-sticky-header y-header_05">      
  <div  id="dragons">        
    <div class="header y-header_outer">            
      <div class="container">                
        <div class="row clearfix">                    
          <div class="col-sm-2">                        
            <a href="<?php echo esc_url( home_url('/') , 'yachtsailing') ?>" class="y-logo">              
              <?php if(yachtsailing_global_var('logo_image','url') != null) { ?>                            
              <img src="<?php echo esc_url(yachtsailing_global_var('logo_image','url'), 'yachtsailing'); ?>" alt="">                            
              <?php } else{ ?>                            
              <h2 class="blog_name">
                <?php bloginfo( 'name' ); ?>
              </h2>                            
              <?php } ?>                        
            </a>                     
          </div>                
          <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 wow fadeInDown text-center" data-wow-duration="1s">                   
            <div class="top_info">
              <div class="y-social">
			   <?php echo get_template_part("social","icons");?>
              </div>          
              <?php if(isset($woocommerce)){ $cart_icon_switch = $theme_option['woo_icon_switch'] ;  if($cart_icon_switch == "show")   {?>  
              <div class="y-login_but">     
                <a href="<?php $cart_url = wc_get_cart_url(); echo esc_url($cart_url, 'yachtsailing'); ?>">          
                  <i class="material-icons">shopping_cart
                  </i>              
                  <span class="cart_items">         
                    <?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), "yachtsailing" ), WC()->cart->get_cart_contents_count() ); ?> - 
                    <?php echo WC()->cart->get_cart_total(); ?>
                  </span>     
                </a>  
              </div>            
              <?php } }?>  
            </div>    
            <div class="y-menu_outer wow fadeInDown" data-wow-duration="2s">        
              <div class="rmm style">          
                <?php              if ( has_nav_menu( 'primary' ) )              {                 wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'rmm-menu', 'menu_id' => '', 'after' => '', 'walker' => new yachtsailing_menu_walker() ) );              }          ?>        
              </div>    
            </div>         
          </div>       
        </div>   
      </div>  
    </div>
  </div>    
  <?php get_template_part("header","banner");?>                  
</div>