<?php get_header(); ?>
<?php echo get_template_part("header","title");?>
<section id="y-single_info">
  <div class="y-single_info">
    <div class="container">  
	  <?php 
        while(have_posts()): the_post();			
          the_content();
        endwhile;
      ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>