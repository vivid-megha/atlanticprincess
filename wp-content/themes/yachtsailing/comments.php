<?php
  /* The template for displaying comments */
    if ( post_password_required() ) 
    {
       return;
    }
    $GLOBALS['comment'] = $comment;
?>
<div class="container">
<div class="article-comments">
	<?php if ( have_comments() ) : ?>
		<?php the_comments_navigation(); ?>
		  <div class="article-comments__group">
		<?php
		  wp_list_comments( array(
					'style'       => 'div',
					'short_ping'  => true,
					'avatar_size' => 80,
					'callback'    => 'yachtsailing_comment'
				) );
		?>
		</div>
		<?php the_comments_navigation(); ?>
	    <?php endif; ?>
	<?php
	   // If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
	        <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'yachtsailing' ); ?></p>
	<?php endif; ?>
   <div class="row">
<div class="col-sm-8 col-xs-12">
	<?php
		$fields =  array(
		  'author' => '<div class="comment-form-author comment-form__input"><input id="author" name="author" placeholder="'.esc_html('Full Name', 'yachtsailing').'" class="search-input" type="text" value="' . esc_attr( $commenter['comment_author'] , 'yachtsailing') . '" size="30" aria-required="true" required />',
		  'email' => '<div class="comment-form-email comment-form__input"><input id="email" name="email" placeholder="'.esc_html('Email', 'yachtsailing').'" class="search-input" type="text" value="' . esc_attr(  $commenter['comment_author_email'] , 'yachtsailing') . '" size="30" aria-required="true" required /></div>',
		  'url' => '<div class="comment-form-url comment-form__input"><input id="url" name="url" placeholder="'.esc_html('Website', 'yachtsailing').'" class="search-input" type="text" value="' . esc_attr( $commenter['comment_author_url'] , 'yachtsailing') . '" size="30" /></div></div>',
		);
		comment_form( array(
			'title_reply_before'   => '<h3 class="blog-title">',
			'title_reply_after'    => '<span class="line line--title line--blog-title"><span class="line__first"></span><span class="line__second"></span></span></h3>',
			'label_submit'         => esc_html( 'Leave Comment', 'yachtsailing' ),
			'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
			'comment_field'        => '<div class="comment-form-comment "><textarea id="comment" class="comment-textarea search-input" placeholder="'.esc_html('Comment', 'yachtsailing').'" name="comment" aria-required="true"></textarea></div>',
			'class_submit'         => 'btn button button--red button--main',
			'comment_notes_before' => ''
		) );
	?>
</div>
   </div>
</div>
</div>