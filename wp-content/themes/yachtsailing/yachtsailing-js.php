<?php
function yachtsailing_enqueue_typekit() {
	
   wp_enqueue_script( 'yachtsailing-custom', get_template_directory_uri() . '/assets/js/yachtsailing-custom.min.js', array(), '1.0' );
   global $theme_option;    
   $pageid = get_the_id();
   $layout = $theme_option["layout_swtich"];
   $page_layout = get_post_meta($pageid,"_cmb_page_layout",true);
	if($page_layout == "page_boxed")
	{
			
		   wp_add_inline_script( 'yachtsailing-custom',  
				   'jQuery(document).ready(function(){
						  jQuery("body").addClass("y-boxed");
					});'
		   );
	
	}
    if($layout == 'boxed')
    {
			
		   wp_add_inline_script( 'yachtsailing-custom',  
				   'jQuery(document).ready(function(){
						  jQuery("body").addClass("y-boxed");
					});'
		   );
    }	
}
add_action( 'wp_enqueue_scripts', 'yachtsailing_enqueue_typekit' );