<?php   
  get_header();
  global $theme_option;
  global $woocommerce;    
  $layout = isset($theme_option['layout_swtich']) ? $theme_option['layout_swtich'] : null;    
  $page_layout = get_post_meta($post->ID,"_cmb_page_layout",true);
  $box_class = "";  
  $background = "";
  
  if($page_layout == "page_boxed")  
  {
    $box_class = "y-boxed"; 
  }   
  
  if($layout == 'boxed') 
 {
   $box_class = "y-boxed";    }   
?>
<body>
   <?php 
     $home_template = "page-templates/template-home.php";
    
    if(is_page_template($home_template))
    {
       echo '<div class="custom_home_page_class">';
    }
    else
   {
    echo '<div class="custom_inner_page_class">';    
   }      
   if($box_class != "")
   { 
     echo '<div class="y-boxed-inner">';
   } 
?>
<!-- Loader -->
<?php 
   if(yachtsailing_global_var('load_icon_val') != '0')
  {
?>
  <div id="page-preloader">
    <span class="spinner"></span>
    <span class="loading"></span>
  </div>
<?php } ?>
<!-- Loader end -->
<?php 
   if(is_page_template( 'page-templates/template-home.php' ))
   {     
        $home_header_sticky = $theme_option['home_header_sticky'];
          if($home_header_sticky == "yes")    
          {       
            $stickyclass = "y-yessticky";     
          }     
          else    
          {       
            $stickyclass = "y-notsticky";     
          }
   }
   else
   {     
      $inner_header_sticky = $theme_option['inner_header_sticky'];
      if($inner_header_sticky == "yes")     
      {       
        $stickyclass = "y-yessticky";     
      }     
     else if($inner_header_sticky == "no")     
      {       
        $stickyclass = "y-notsticky";     
      }     
     else    
     {       
       $stickyclass = "y-yessticky";     
     }
   }
?>
   <div class="<?php echo sanitize_html_class($stickyclass); ?>  y-header_strip  y-header_02">
      <div id="dragons">
        <div class="header">
          <div class="y-header_top_info">
            <div class="container">
              <div class="row clearfix">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"> 
                </div>
              <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <div class="top_info">
                <?php                   
                   if(isset($woocommerce))
                   {            
                      $cart_icon_switch = $theme_option['woo_icon_switch'] ;
                      if($cart_icon_switch == "show")              
                      {                            
                ?>
                <div class="y-login_but">
                    <a href="<?php  $cart_url = wc_get_cart_url(); echo esc_url($cart_url, 'yachtsailing'); ?>"> 
                      <i class="material-icons">shopping_cart</i> 
                      <span class="cart_items">                 
                          <?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() , "yachtsailing"), WC()->cart->get_cart_contents_count() ); ?> - 
                          <?php echo WC()->cart->get_cart_total(); ?>
                      </span> 
                    </a>
                </div>
                <?php 
                    } 
                  } 
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row clearfix">
          <div data-wow-duration="1s" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wow fadeInDown text-center">
            <a class="y-logo" href="<?php echo esc_url( home_url('/') , 'yachtsailing') ?>">
              <?php if(yachtsailing_global_var('logo_image','url') != null) { ?> 
              <img src="<?php echo esc_url(yachtsailing_global_var('logo_image','url'), 'yachtsailing'); ?>" alt="">
              <?php } else{ ?>
              <h2 class="blog_name">
                <?php bloginfo( 'name' ); ?>
              </h2>
              <?php } ?>
            </a>
          </div>
        </div>
        <div class="row clearfix">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="y-menu_outer">
              <div class="rmm style">
                <?php if ( has_nav_menu( 'primary' ) ) {                           
                  wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'rmm-menu', 'menu_id' => '', 'after' => '', 'walker' => new yachtsailing_menu_walker() ) );  
                  } ?>
              </div>
             <div class="y-social">
               <?php echo get_template_part("social","icons");?>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_template_part("header","banner");?>