<?php
  /* The template for displaying 404 pages (not found) */
  get_header(); 
?>
<section id="y-single_info">
          <div class="y-single_info">
            <div class="container">
                <div class="row y-single_info_inner y-section_content page_404">                    
                    <div class="col-sm-12">
                        <div class="col-lg-8 col-md-8 col-sm-7">
                         <div class="row">
                            <?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'yachtsailing' ); ?>
                               <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'yachtsailing' ); ?></p>
                               <?php get_search_form(); ?>
                         </div> 
                        </div>
                    </div>
                </div>
            </div>
          </div>
   </section>
<?php get_footer(); ?>