<?php get_header(); ?>
<?php echo get_template_part("header","title");?>
<div class="y-single_info">          
  <div class="row y-section_content">
    <div class="clearfix">
	  <?php     
	    if(!class_exists('WPBakeryShortCode'))
	    {
	      $container_class = " container";
	    }
	    else
	    {
	      $container_class = "";
	    }
	  ?>
	  <div class="y-corporate_block container <?php echo sanitize_html_class($container_class); ?> clearfix">
	    <?php
	      while(have_posts()): the_post();
	        the_content();
          
            // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
	      endwhile;
	    ?>
	  </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>