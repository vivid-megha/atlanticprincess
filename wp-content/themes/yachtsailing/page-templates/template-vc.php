<?php 
/** * Template Name: Page with visual composer  */
get_header(); 
?>
<?php echo get_template_part("header","title");?>
        <?php 
			while(have_posts()): the_post();
				echo do_shortcode(get_the_content());
			endwhile;
		?>
<?php get_footer(); ?>