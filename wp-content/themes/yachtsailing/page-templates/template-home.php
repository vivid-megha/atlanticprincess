<?php
/** * Template Name: Home */
get_header();
?>
<section id="y-single_info">   
  <div class="y-single_info">          
    <div class="row y-section_content">
      <div class="clearfix">
	      <?php     
	         if(!class_exists('WPBakeryShortCode'))
	         {
	           $container_class = " container";
	         }
	         else
	         {
	           $container_class = "";
	         }
	      ?>
	      <div class="y-corporate_block <?php echo esc_attr($container_class, 'yachtsailing'); ?> clearfix">
<?php
	while( have_posts() ): the_post();
			the_content();			
    endwhile;
?>
	      </div>
      </div>
    </div>
  </div>
</section>
<?php    
get_footer();