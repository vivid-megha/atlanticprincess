<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$template = get_option( 'template' );
switch ( $template ) {
	case 'twentyeleven' :
		echo '<div class="y-inner_page"><section id="y-single_info"  class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
	case 'twentytwelve' :
		echo '<div class="y-inner_page"><section id="y-single_info" class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
	case 'twentythirteen' :
		echo '<div class="y-inner_page"><section id="y-single_info" class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
	case 'twentyfourteen' :
		echo '<div class="y-inner_page"><section id="y-single_info" class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
	case 'twentyfifteen' :
		echo '<div class="y-inner_page"><section id="y-single_info" class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
	case 'twentysixteen' :
		echo '<div class="y-inner_page"><section id="y-single_info" class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
	default :
		echo '<div class="y-inner_page"><section id="y-single_info" class="breadcrubme_section"><div class="y-single_info"><div class="container"><div class="row y-single_info_inner y-section_content">';
		break;
}
