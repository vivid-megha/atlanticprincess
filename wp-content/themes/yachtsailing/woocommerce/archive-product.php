<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header( 'shop' ); 
 global $theme_option; 
?>
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
<div class="col-lg-12 clearfix">
 <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInDown;">
    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
      <h1 class="y-heading">
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
			<?php woocommerce_page_title(); ?>
		<?php endif; ?>		
        </h1>
     </div>
     <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12"> 
      <?php yachtsailing_breadcrumbs(); ?>
     </div>    
     
    </div>
</div>
<div class="clearfix">
 <?php
    $shop_sidebar = $theme_option['shop_sidebar'];
	if($shop_sidebar == "right" || $shop_sidebar == "left")
	{
		if($shop_sidebar == "right")
		{
			$sidebar_class = "pull-right";
		}
		else
		{
			$sidebar_class = "";			
		}
		?>
                <div class="col-sm-3 <?php echo $sidebar_class; ?>">
                  <div class="y-sidebar">
                    <?php
                        /**
                         * woocommerce_sidebar hook
                         *
                         * @hooked woocommerce_get_sidebar - 10
                         */
                        do_action( 'woocommerce_sidebar' );
                    ?>
                    </div>
                </div>    
    <?php
	}
 ?> 
   
		<?php if ( have_posts() ) : ?>
			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked wc_print_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				// do_action( 'woocommerce_before_shop_loop' );
			?>
			<?php woocommerce_product_loop_start(); ?>
				<?php woocommerce_product_subcategories(); ?>
				<?php 
				$loopnum = 0;
				 while ( have_posts() ) : the_post();
				 $loopnum++;
				?>
					<?php
						/**
						 * woocommerce_shop_loop hook.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
				      wc_get_template_part( 'content', 'product' ); 
    if($loopnum == 3)
    {
        ?>
            <div class="clearfix"></div>
        <?php
        $loopnum = 0;
    }
                    ?>
				<?php endwhile; // end of the loop. ?>
			<?php woocommerce_product_loop_end(); ?>
<div class="clearfix"></div>
			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>
		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
   
			<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			?>
		<?php endif; ?>
	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
</div>
<?php get_footer(); ?>
