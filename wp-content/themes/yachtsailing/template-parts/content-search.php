<?php
/**
 * The template part for displaying results in search pages
 */
?>
    <div id="post-<?php the_ID(); ?>" <?php // post_class('article'); ?> class="article">
       <?php if ( 'post' === get_post_type() ) { ?>
       
         
	<div class="blog-date triangle triangle--big wow slideInLeft" data-wow-delay="0.7s" data-wow-duration="1.5s">
    	<div class="blog-date__num"><?php echo get_the_time('j'); ?></div>
        <div class="blog-date__month-year"><?php echo get_the_time('F'); ?></div>
        <div class="blog-date__month-year"><?php echo get_the_time('Y'); ?></div>
    </div>
    <div class="article__comments-author wow slideInUp" data-wow-delay="0.7s" data-wow-duration="1.5s">
    	<span class="article__author"><span class="fa fa-user"></span><?php esc_html_e('Posted By:','yachtsailing');?> <?php the_author(); ?></span>
        <span class="article__comments"><span class="fa fa-comment-o"></span><?php comments_number(); ?></span>
    </div>
    <h3 class="article-title wow slideInUp" data-wow-delay="0.7s" data-wow-duration="1.5s"><?php the_title(); ?></h3>
    <?php echo do_shortcode(wpautop(get_the_excerpt())); ?>
    
    <div class="search_read">
    <a class="btn button button--main button--grey transparent wow slideInLeft" data-wow-duration="1.5s" data-wow-delay="0.7s" href="<?php the_permalink();?>"><?php esc_html_e('Read More ','yachtsailing');?></a>
    <span data-wow-duration="1.5s" data-wow-delay="0.7s" class="line line--title line--blog-title line--article wow slideInLeft"><span class="line__first"></span><span class="line__second"></span></span>
   </div>
   
   <?php }else {?>
   
   <h3 class="article-title wow slideInUp" data-wow-delay="0.7s" data-wow-duration="1.5s"><?php the_title(); ?></h3>
    <?php echo do_shortcode(wpautop(get_the_excerpt())); ?>
    <div class="search_read">
    <a class="btn button button--main button--grey transparent wow slideInLeft" data-wow-duration="1.5s" data-wow-delay="0.7s" href="<?php the_permalink();?>"><?php esc_html_e('Read More ','yachtsailing');?></a>
    <span data-wow-duration="1.5s" data-wow-delay="0.7s" class="line line--title line--blog-title line--article wow slideInLeft"><span class="line__first"></span><span class="line__second"></span></span>
   </div>
   
   <?php }?>
</div>
