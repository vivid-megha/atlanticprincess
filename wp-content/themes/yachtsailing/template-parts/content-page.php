<?php
/**
 * The template used for displaying page content
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('article'); ?>>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'yachtsailing-post-single' );
		if( isset($image) && !empty($image) ) :
		?>
            <div class="article__img wow slideInLeft" data-wow-delay="0.7s" data-wow-duration="1.5s">    	
                <img src="<?php echo esc_url($image[0], 'yachtsailing'); ?>" alt="" class="img-responsive" />       
            </div>
     <?php
		endif;
    the_content(); 
     wp_link_pages();
  ?>
</div>