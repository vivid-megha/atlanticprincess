<?php
/**
 * The template part for displaying single posts
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('article blog-text blog-text--article'); ?>>        
<?php
    if(has_post_format('gallery')) 
    {	
          $gallery = get_post_gallery( get_the_ID(), false );    
        if(isset($gallery['ids']) && !empty($gallery['ids'])) 
        {
                $gallery_ids = $gallery['ids'];
                $img_ids = explode(",",$gallery_ids);
?>
                <div class="blog-date triangle triangle--big">
                    <div class="blog-date__num"><?php echo esc_html(get_the_time('j'), 'yachtsailing'); ?></div>
                    <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('F'), 'yachtsailing'); ?></div>
                    <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
                </div>
            <div class="article__img">
                <ul class="js-blog-slider enable-bx-slider" data-auto="true" data-auto-hover="true" 
                data-mode="fade" data-pager="false" data-pager-custom="null" data-prev-selector="null" data-next-selector="null">
                    <?php 
                          foreach( $img_ids as $img_id ) {
                          $image_src = wp_get_attachment_image_src($img_id,''); 
                    ?>
                    <li>                         
                       <img class="img-responsive" src="<?php echo esc_url($image_src[0], 'yachtsailing'); ?>" />
                    </li>
                    <?php } 
                    ?>
                </ul>
            </div>
  <?php 
    } 
    else 
    { 
  ?>
      <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'yachtsailing-post-single' );   
        if( isset($image) && !empty($image) ) 
        {
   ?>
                <div class="blog-date triangle triangle--big">
                    <div class="blog-date__num"><?php echo esc_html(get_the_time('j'), 'yachtsailing'); ?></div>
                    <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('F'), 'yachtsailing'); ?></div>
                    <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
                </div>
    
                <div class="article__img">
              <img src="<?php echo esc_url($image[0], 'yachtsailing'); ?>" alt="<?php echo esc_html(get_the_title(), 'yachtsailing'); ?>" class="img-responsive" />
                </div>
    
                <?php 
        } 
        else 
        { 
    ?>
                 <div class="blog-datewithoutmedia triangle triangle--big">
                    <div class="blog-date__num"><?php echo  esc_html(get_the_time('j'), 'yachtsailing'); ?></div>
                    <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('F'), 'yachtsailing'); ?></div>
                    <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
                  </div>
                <?php 
        }	
  }
} 
elseif(has_post_format('video')) 
{
?>
		<?php 
       $video = get_post_meta(get_the_ID(),"_cmb_embed_media",true);		
       
 if(isset($video) && !empty($video)) {
?>
       <div class="blog-date triangle triangle--big">
    	<div class="blog-date__num"><?php echo esc_html(get_the_time('j'), 'yachtsailing'); ?></div>
        <div class="blog-date__month-year"><?php echo esc_html(get_the_time('F'), 'yachtsailing'); ?></div>
        <div class="blog-date__month-year"><?php echo esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
       </div>
	<div class="article__img article__img--video">
    </div>
		<?php } else { ?>
		
			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'yachtsailing-post-single' );
			if( isset($image) && !empty($image) ) { ?>
			<div class="blog-date triangle triangle--big">
                <div class="blog-date__num"><?php echo esc_html(get_the_time('j'), 'yachtsailing'); ?></div>        
                <div class="blog-date__month-year"><?php echo esc_html(get_the_time('F'), 'yachtsailing'); ?></div>        
                <div class="blog-date__month-year"><?php echo esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
           </div>
		    <div class="article__img">
			 <img src="<?php echo esc_url($image[0], 'yachtsailing'); ?>" alt="<?php echo esc_html(get_the_title(), 'yachtsailing'); ?>" class="img-responsive" />
            </div>
            <?php } else { 
			?>
			<div class="blog-datewithoutmedia triangle triangle--big">
                <div class="blog-date__num"><?php echo esc_html(get_the_time('j'), 'yachtsailing'); ?></div>        
                <div class="blog-date__month-year"><?php echo esc_html(get_the_time('F'), 'yachtsailing'); ?></div>        
                <div class="blog-date__month-year"><?php echo esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
            </div>
			<?php } ?>
		<?php } ?>
	
  <?php } 
    else 
    { 
  ?>
   <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'yachtsailing-post-single' );
		if( isset($image) && !empty($image) ) {
?>
    <div class="blog-date triangle triangle--big">
    	<div class="blog-date__num"><?php echo  esc_html(get_the_time('j'), 'yachtsailing'); ?></div>
        <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('F'), 'yachtsailing'); ?></div>
        <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
    </div>
    
    <div class="article__img">
		<img src="<?php echo esc_url($image[0], 'yachtsailing'); ?>" alt="<?php echo  esc_html( get_the_title(), 'yachtsailing'); ?>" class="img-responsive" />
   </div> 
    <?php 
    } 
    else 
   { 
?>
    <div class="blog-datewithoutmedia triangle triangle--big">
    	<div class="blog-date__num"><?php echo  esc_html(get_the_time('j'), 'yachtsailing'); ?></div>
        <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('F'), 'yachtsailing'); ?></div>
        <div class="blog-date__month-year"><?php echo  esc_html(get_the_time('Y'), 'yachtsailing'); ?></div>
    </div>
<?php } ?>
	
<?php } ?> 
    <div class="article__comments-author">
      <span class="article__author"><span class="fa fa-user"></span><?php esc_html_e('Posted By:', 'yachtsailing');?> <?php the_author(); ?>          
      </span>
    
        <span class="article__comments"><span class="fa fa-comment-o"></span><?php comments_number(); ?></span>
        <span class="article__category">
                <span class="fa fa-tag"></span>
                  <?php esc_html_e('Category:','yachtsailing');?>
                  <?php $categories = get_the_category(get_the_ID());
				
            foreach($categories as $category)
           {
					echo esc_html($category->cat_name, 'yachtsailing');
          }
			?>
        </span>
        
    <?php 
        if(is_sticky())
        { 
    ?>
        
    <span class="article__sticky">        
    <span class="fa fa-sticky-note">        
    </span><?php esc_html_e('Sticky:', 'yachtsailing')?></span>						
<?php } ?>
        
        
    </div>
    
    <h3 class="article-title">        
    <?php the_title(); ?>        
    </h3>
        <?php the_content(); ?>
        <div class="share clearfix">
             	<?php if(yachtsailing_global_var('share_icon') != '0'){?>
        	<h6>        	    
        	  <?php esc_html_e('Share This Article:','yachtsailing');?>        	    
        	</h6>
            
			<?php
                $shortURL = get_permalink();
                $shortTitle = str_replace( ' ', '%20', get_the_title());
                $twitterURL = '//twitter.com/intent/tweet?text='.$shortTitle.'&amp;url='.$shortURL.'&amp;via=Crunchify';
                $facebookURL = '//www.facebook.com/sharer/sharer.php?u='.$shortURL;
                $googleURL = '//plus.google.com/share?url='.$shortURL;
                ?>
                <div class="share__social">
                   <a href="<?php echo esc_url($facebookURL, 'yachtsailing'); ?>" class="no-decoration">
				   <?php esc_html_e('Facebook','yachtsailing');?></a>
                </div>
                
                <div class="share__social">
                  <a href="<?php echo esc_url($twitterURL, 'yachtsailing'); ?>" class="no-decoration">
				     <?php esc_html_e('Twitter','yachtsailing');?></a>
                </div>
                
                <div class="share__social">
                   <a href="<?php echo esc_url($googleURL, 'yachtsailing'); ?>" class="no-decoration">
				   <?php esc_html_e('Google+','yachtsailing');?></a>
               </div>               
           <?php }?>
     
			<?php 
                if(yachtsailing_global_var('tag_icon') != '0')
                if(has_tag()){ 
            ?>
                        <h6>                 
                           <?php esc_html_e('Tags:','yachtsailing'); ?>                
                        </h6>
                        <?php $posttags = get_the_tags(); ?>
                      <?php 
                        foreach($posttags as $tag) 
                        { 
                      ?>            
                                <div class="share__social">                
                                <a href="<?php echo esc_utl(get_tag_link($tag->term_id), 'yachtsailing'); ?>" class="no-decoration">                
                                   <?php echo esc_html($tag->name, 'yachtsailing'); ?></a>                
                                </div>
                      <?php } ?>
            
            <?php }?>
        </div>
	</div>