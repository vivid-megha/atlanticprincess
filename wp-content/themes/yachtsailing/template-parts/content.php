<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Yachtsailing
 *
 */
  global $theme_option; 
  global $woocommerce;
$post_date = get_the_date("d")." ";
$post_month = get_the_date("F")." ";
$post_year = get_the_date("Y"); 
?>
                            <div class="y-single_blog">                           
              							  <?php  
              							    $post_id = get_the_id();
              							     $attachment_id = get_post_meta($post_id,"_cmb_featured_image_id",true);
              								  $src = wp_get_attachment_image_src($attachment_id,'yachtsailing-blog-listing-full');
              				                  $post_image = $src[0];		 
              								   $short_content = get_post_meta($post_id,"wiki_test_wysiwyg",true);	
              								   $post_content = get_the_content($post_id);
              							   ?> 
                              <h1><a href="<?php the_permalink(); ?>"><?php esc_html(the_title()); ?></a></h1>
                              <img class="img-responsive" alt="" src="<?php echo  $post_image;?>">
                              <div class="y-home_single_blog y-single_blog_text clearfix">
                                <div class="y-home_blog_content">
                                  <div class="y-home_blog_content_inner">
        <?php 
                          the_content( sprintf(
                          __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'yachtsailing' ),
                          get_the_title()
                        ) );
        ?>
                                    <a class="y-btn y-btn_bg" href="<?php $permalik_more = get_the_permalink(); echo esc_url($permalik_more); ?>">read more</a>
                                  </div>
                                </div>
                              </div>
                          </div>    