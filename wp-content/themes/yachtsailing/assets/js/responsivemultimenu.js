/*
Responsive Mobile Menu v1.1
Plugin URI: responsivemultimenu.com
Author: Adam Wysocki
Author URI: http://oncebuilder.com
License: http://opensource.org/licenses/MIT
*/
function adaptMenu() {
	/* 	toggle menu on resize */
	jQuery('.rmm').each(function() {
		// initialize vars
		var maxWidth = 0;
		var width = 0;
		// width of menu list (non-toggled)
		jQuery('.rmm-menu').children("li").each(function() {
			if(jQuery(this).parent().hasClass('rmm-menu')){
				width = jQuery(this).outerWidth();//outerWidth();
				if(width>0){
					maxWidth += width;
				}
			}
		});
		// compare width
		var width = jQuery('.rmm').css('max-width');
		width = width.replace('px', ''); 
		
		if ( jQuery(this).parent().width() > width ) {
			jQuery('.rmm-menu').removeClass("rmm-mobile");
			
			//remove all classes from mobile verion
			jQuery(".rmm-menu ul").removeClass("rmm-subview");
			jQuery(".rmm-menu li").removeClass("rmm-subover-hidden");
			jQuery(".rmm-menu li").removeClass("rmm-subover-visible");
			jQuery(".rmm-menu a").removeClass("rmm-subover-header");
			jQuery(".rmm-toggled").removeClass("rmm-closed");
			jQuery('.rmm-toggled').hide();
			
			//jQuery('.rmm-toggled').removeClass("rmm-view");
			//jQuery('.rmm-toggled').addClass("rmm-closed");
		}else {
			jQuery('.rmm-menu').addClass("rmm-mobile");
			jQuery('.rmm-toggled').show();
			jQuery('.rmm-toggled').addClass("rmm-closed");
			
			//jQuery('.rmm-toggled').removeClass("rmm-closed");
		}
	});
}
function responsiveMultiMenu() {
	jQuery('.rmm').each(function() {
		// create mobile menu classes here to light up HTML
		jQuery(this).find("ul").addClass("rmm-submenu");
		jQuery(this).find("ul:first").addClass("rmm-menu");
		jQuery(this).find("ul:first").removeClass("rmm-submenu");
		jQuery(this).find('.rmm-submenu').prepend( '<li class="rmm-back"><a href="#">back</a></li>' );
		jQuery(this).find("ul").prev().addClass("rmm-dropdown");
	
		// initialize vars
		var maxWidth = 0;
		var width = 0;
		// width of menu list (non-toggled)
		jQuery('.rmm-menu').children("li").each(function() {
			if(jQuery(this).parent().hasClass('rmm-menu')){
				width = jQuery(this).outerWidth();//outerWidth();
				if(width>0){
					maxWidth += width;
				} 
			}
		});
		if (jQuery.support.leadingWhitespace) {
			jQuery(this).css('max-width' , (maxWidth+5)+'px');
		}else{
			jQuery(this).css('width' , (maxWidth+5)+'px');
		}
		
		// create dropdown button
		var str=''
		str+='<div class="rmm-toggled rmm-view rmm-closed">'
			str+='<div class="rmm-toggled-controls">'
				str+='<div class="rmm-toggled-title">Menu</div>';
				str+='<div class="rmm-toggled-button"><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span></div>';
			str+='</div>';
		str+='</div>';
		
		jQuery(this).prepend(str);
	});
	
	// click interacts in mobile wersion
	jQuery('.rmm-dropdown').click(function (e) {
		if(jQuery(this).parents(".rmm-menu").hasClass('rmm-mobile')){
			e.preventDefault();
			e.stopPropagation();
			
			jQuery(this).next().addClass("rmm-subview");
			var index=jQuery(this).parent().index();
			
			var i=0;
			jQuery(this).parent().parent().children("li").each(function() {
				if(index==jQuery(this).index()){
					jQuery(this).removeClass("rmm-subover-hidden");
					jQuery(this).addClass("rmm-subover-visible");
				}else{
					jQuery(this).removeClass("rmm-subover-visible");
					jQuery(this).addClass("rmm-subover-hidden");
				}
			});
			jQuery(this).addClass("rmm-subover-header");
		}
	});
	
	// click back interacts in mobile version
	jQuery('.rmm-back a').click(function () {
		jQuery(this).parent().parent().prev().removeClass("rmm-subover-header");
		jQuery(this).parent().parent().removeClass("rmm-subview");
		jQuery(this).parent().parent().parent().parent().find("li").removeClass("rmm-subover-hidden");
	});
	
	// click toggler interacts in mobile version
	jQuery('.rmm-toggled, .rmm-toggled .rmm-button').click(function(){
		if (jQuery(this).is(".rmm-closed")) {
			jQuery(this).removeClass("rmm-closed");
		}else {
			jQuery(this).addClass("rmm-closed");
		}
	});	
}
jQuery(window).load(function() {
    responsiveMultiMenu();
	adaptMenu();
});
jQuery(window).resize(function() {
 	adaptMenu();
});