<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php 
      if (! ( function_exists( 'has_site_icon' ) && has_site_icon() )) 
      {
          if((yachtsailing_global_var('favicon','url') !=null)) 
          {
      ?>
          <link href="<?php  echo esc_url(yachtsailing_global_var('favicon','url'), 'yachtsailing'); ?>" rel="icon" /> 
     <?php }
      }
    ?>
      <script>
          var theme_url = "<?php echo esc_url( get_template_directory_uri() ); ?>";	
      </script>
    <?php
      wp_head();
    ?>
    </head>
    <?php
      global $theme_option;
      if(is_page())
      {
        $header_type = get_post_meta($post->ID, "_cmb_header_type", true);
      }
      else
      {
         $header_type = "";        
      }
      
      if(isset($theme_option))
          {
              /* Header selection */
              if(is_page_template( 'page-templates/template-home.php' ))
                {
                  $home_header = $theme_option['home_header'];
                    if($home_header == "theme_defaults")    
                    {
                     $home_header = $header_type;
                    }
                    
                    if($home_header == 'header_first')
                    {
                     get_header('first');
                    }
                    else if($home_header == 'header_second')
                    {
                     get_header('second');
                    }
                    else if($home_header == 'header_third')
                    {
                     get_header('third');
                    }
                    else if($home_header == 'header_fourth')
                    {
                     get_header('fourth');
                    }
                    else if($home_header == 'header_fifth')
                    {
                     get_header('fifth');
                    }
                    else if($home_header == 'header_sixth')
                    {
                      get_header('sixth');
                    }
                    else if ($home_header == 'header_inner')
                    {
                      get_header('inner');
                    }
                    else
                    {
                     $classes = get_body_class();
                     if (in_array("blog", $classes))
                     {
                      get_header('inner');
                     }
                     else if(in_array("page", $classes))
                     {
                      get_header('first');
                     }
                     else
                     {
                      get_header('first');
                     }
                    }
                  }
                  else
                  {
                     $inner_header = $theme_option['inner_header'];
      if(is_page())
      {
        $header_type = get_post_meta($post->ID, "_cmb_header_type", true);
      }
      else if(is_single())
      {
         $header_type = $inner_header;
      }
     else
     {
         $header_type = "";
     }
                     if($header_type == "header_default")
                     {
                        $header_type = $inner_header;   
                     }
                     else
                     {
                        $header_type = $header_type;    
                     }
                      if($header_type == 'header_first')
                      {
                        get_header('first');  
                      }
                      else if($header_type == 'header_second')
                      {
                        get_header('second');
                      }
                      else if($header_type == 'header_third')   
                      {
                        get_header('third');    
                      }
                      else if($header_type == 'header_fourth')  
                      {      
                        get_header('fourth');   
                      } 
                      else if($header_type == 'header_fifth')   
                      {      
                        get_header('fifth');    
                      } 
                      else if($header_type == 'header_sixth')   
                      {      
                        get_header('sixth');    
                      }
                      else if ($header_type == 'header_inner')
                      {
                         get_header('inner');   
                      }
                      else
                      {
                         get_header('inner');
                      }
                   }
            }
            else
            {
               if($header_type == 'header_first')
               {
                 get_header('first'); 
               }
               else if($header_type == 'header_second')   
               {
                 get_header('second');
               }
               else if($header_type == 'header_third')  
               {
                 get_header('third');
               }
               else if($header_type == 'header_fourth')
               {
                 get_header('fourth');
               }
               else if($header_type == 'header_fifth') 
               {
                 get_header('fifth');
               }
               else if($header_type == 'header_sixth')  
               {
                 get_header('sixth');
               }
               else if ($header_type == 'header_inner') 
               {      
                get_header('inner');
               }
               else
               {
                get_header('inner');      
               }
            }