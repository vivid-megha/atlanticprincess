<?php
  global $theme_option; 
  global $woocommerce;
  get_header(); 
?>
  <section id="y-single_info">
    <div class="y-single_info">
      <div class="container">
        <div class="row y-single_info_inner y-section_content">
          
          <div class="clearfix row">
            <div class="y-breadcrum clearfix wow fadeInDown" data-wow-delay=".9s">
              <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12"> 
                <h1 class="y-heading"><?php echo esc_html("Blog listing", 'yachtsailing'); ?></h1> 
              </div>
              <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12"> 
                <ul class="pull-right">
                    <li><a href="<?php echo esc_url(site_url(), 'yachtsailing');?>"><?php echo esc_html("Home", "yachtsailing"); ?></a></li>
                    <li><span><?php echo esc_html("Blog listing", 'yachtsailing'); ?></span></li> 
                </ul> 
              </div>
            </div>
          </div>
          <div class="">
            <div class="col-lg-8 col-md-8 col-sm-7">
              <div class="row">
                <div class="y-single_blog">
                <?php if ( have_posts() ) :
                    while ( have_posts() ) : the_post();                    
                      get_template_part( 'template-parts/content', get_post_format() );
                    endwhile;
                    else :
                      get_template_part( 'template-parts/content', 'none' );                    
                    endif; 
  // Previous/next page navigation.
      the_posts_pagination( array(
        'prev_text'          => __( 'Previous page', 'yachtsailing' ),
        'next_text'          => __( 'Next page', 'yachtsailing' ),
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'yachtsailing' ) . ' </span>',
      ) );
                    ?>
                </div>
              </div>
            </div>
          	<?php 
                 if ( is_active_sidebar( 'sidebar-1' )  ) : 
            ?>
              	  <?php get_sidebar('blog'); ?>
            <?php endif; ?>
          </div>                       
        </div>
      </div>
    </div>
  </section>
<?php get_footer(); ?>