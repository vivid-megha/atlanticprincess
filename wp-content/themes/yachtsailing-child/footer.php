<?php
/*
Theme Name: Yachtsailing Child Theme
Template: yachtsailing
*/

?>

<?php
    global $theme_option; 
    if(isset($theme_option)) 
    { 
?>  
        <div class="theme_url hide_this">
           <?php echo esc_url( get_template_directory_uri(), 'yachtsailing'); ?>
        </div>
        <footer id="y-footer"> 
                  <div class="y-footer">
                     <div class="container">
                        <div class="row clearfix">
                                  <?php if ( is_active_sidebar( 'footer_widget' )  ) : ?>
                                    <?php dynamic_sidebar( 'footer_widget' ); ?>
                                  <?php endif; ?> 
                        </div> 
                     </div>  
                  </div> 
        <div class="y-footer_strip">  
          <div class="container">  
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                   <p class="text-left">
                        <?php if(yachtsailing_global_var('copyrighttext') != null) : ?>
                         <?php echo sanitize_text_field(yachtsailing_global_var('copyrighttext')); ?>
                        <?php endif; ?>
                   </p>
                </div> 
            </div>
          </div>
        </div>     
    </footer>
<?php 
  if(isset($theme_option))
  {
    if(array_key_exists("back_to_top_switch", $theme_option))
   {
        $back_to_top_switch = $theme_option['back_to_top_switch'];
   }
   else
   {          
        $back_to_top_switch  = "";
   }
    
  if($back_to_top_switch == "show")
  {
   ?>
   <div class="y-back_to_top" id="y-back_to_top">
       <?php
           if(array_key_exists("back_to_top_icon", $theme_option))
          {
             $back_to_top_icon = $theme_option['back_to_top_icon']["url"];  
          }
    else
    {
          $back_to_top_icon  = "";
    }
       
       if($back_to_top_icon != "")
        {
          echo '<img src="'.$back_to_top_icon.'">';
        }
        else
        { 
          echo '<i class="fa fa-anchor"></i>';
        }
      ?>
   </div>    
  <?php 
    }
  }
}
else
{ 
?>
       <div class="y-footer_strip">  
          <div class="container">  
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                   <p class="text-left">
                       <?php echo esc_html("Copyright all rights reserved 2018 – Leisure Cruising Pty Ltd", 'yachtsailing'); ?>
                   </p>
                </div> 
            </div>
          </div>
        </div>
        
<?php  
}
      
    $template = get_page_template_slug( get_the_id() ); 
    $layout = isset($theme_option['layout_swtich']) ? $theme_option['layout_swtich'] : null; 
         
          if(is_page())
          {    
            $header_type = get_post_meta($post->ID, "_cmb_header_type", true);
          }
          else
          {
            $header_type ="";  
          }
         
          if(is_page())
          {    
            $page_layout = get_post_meta($post->ID, "_cmb_page_layout", true);
          }
          else
          {
            $page_layout ="";  
          }
        $box_class = "";  
        if($page_layout == "page_boxed")
        {
           $box_class = "y-boxed";      
        }
        if($layout == 'boxed')
        {
          $box_class = "y-boxed";
        } 
        if($header_type == 'header_fourth')
        {
          $box_class = "y-boxed";
        } 
      if($template != "page-templates/template-home.php")
      {
           if($box_class != "")
         {  
           $box_class = '</div>';
         }
         else{}   
      }
    ?>  
</div>
<?php if($box_class != ""){ echo '</div>';}   
?>
<?php wp_footer(); ?>
</body>
</html>