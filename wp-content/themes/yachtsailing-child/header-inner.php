<?php 
  get_header();  
  global $theme_option; 
  global $woocommerce; 
      
  $box_class = "";  
  $background = "";
  if(isset($theme_option))
  {
      $layout = isset($theme_option['layout_swtich']) ? $theme_option['layout_swtich'] : null; 
      $page_layout = get_post_meta($post->ID,"_cmb_page_layout",true);
      $box_class = "";  
      $background = "";
      if($page_layout == "page_boxed")
      {
        $box_class = "y-boddxed";      
      }
      if($layout == 'boxed')
      {
       $box_class = "y-boxdded";
      }  
  }    
?>
<body>
    <?php 
       $home_template = "page-templates/template-home.php";
       
       if(is_page_template($home_template))
       {
          echo '<div class="custom_home_page_class">';
       }
       else
       {
          echo '<div class="custom_inner_page_class">';    
       }
   
     if($box_class != "")
      {
        echo '<div class="y-boxed-inner">';
      } 
    ?>
       <!-- Loader --> 
        <div id="page-preloader">
            <span class="spinner"></span>
            <span class="loading"><?php esc_html_e('','yachtsailing')?></span>
          </div>
       <!-- Loader end -->
    <?php
    if(is_page_template( 'page-templates/template-home.php' ))
     {
       $home_header_sticky = "";
            
          if(isset($theme_option))
          {
            $home_header_sticky = $theme_option['home_header_sticky'];
          }
           
           if($home_header_sticky == "yes")
           {
             $stickyclass = "y-yessticky";
           }
           else if($home_header_sticky == "no")
           {
             $stickyclass = "y-notsticky";
           }
           else
           {
             $stickyclass = "y-yessticky";
           }
      }
    else
    {
       $inner_header_sticky = "";
       if(isset($theme_option))
       {
         $inner_header_sticky = $theme_option['inner_header_sticky'];
       }
     
       if($inner_header_sticky == "yes")
       {
         $stickyclass = "y-yessticky";
       }
       else if($inner_header_sticky == "no")
       {
         $stickyclass = "y-notsticky";
       }
       else
       {
         $stickyclass = "y-yessticky";             
       }
   }
?>  
    <div id="skrollr-body" class="<?php echo sanitize_html_class($stickyclass); ?> y-header_strip y-header_01">
      <div  id="dragons">
        <div class="header y-header_outer">
            <div class="container">
                <div class="row clearfix">
                    <div class="col-sm-2">
                        <a class="y-logo" href="<?php echo esc_url( home_url('/') , 'yachtsailing') ?>">
                            <?php 
                            if(yachtsailing_global_var('logo_image','url') != null) 
                            {
                            ?>
                                  <img src="<?php echo esc_url(yachtsailing_global_var('logo_image','url'), 'yachtsailing'); ?>" alt="">
                            <?php 
                            } 
                            else
                            {
                            ?>
                                   <h2 class="blog_name"><?php esc_html(bloginfo( 'name' ), 'yachtsailing'); ?></h2>
                            <?php } ?>
                        </a>  
                    </div>
                     <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10 wow fadeInDown text-center" data-wow-duration="1s"> 
                        <div class="y-top_info_part">       
                  	         <?php 
                                 $cart_icon_switch = "";
                                 if(isset($theme_option))
                                 {
                                   $cart_icon_switch = $theme_option['woo_icon_switch'] ;
                                 }
                                
                                if(isset($woocommerce))
                                {
                                   if($cart_icon_switch == "show")
                                  {
                             ?>
                                   <div class="y-login_but">
                                      <a href="<?php $cart_url = $woocommerce->cart->get_cart_url(); echo esc_url($cart_url); ?>">
                                         <i class="material-icons">shopping_cart</i>
                                            <span class="cart_items">
                                              <?php echo sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), "yachtsailing" ), WC()->cart->get_cart_contents_count() ); ?> - <?php echo WC()->cart->get_cart_total(); ?></span>
                                      </a>
                                   </div>   
                             <?php 
                                   } 
                                }
                             ?>   
                             <div class="y-social">
                                <!-- <div><a href="tel:+6140842220242"><b>Call Us +61 408 422 242</b></a></div>-->
                      	         <?php echo get_template_part("social","icons");?>
                              </div>        
                        </div>
                                    
                              <div class="y-menu_outer wow fadeInDown" data-wow-duration="2s">
                                  <div class="rmm style">
                                     <?php 
                                        if ( has_nav_menu( 'primary' ) ) 
                                          {
                                            wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'menu_class' => 'rmm-menu', 'menu_id' => '', 'after' => '', 'walker' => new yachtsailing_menu_walker() ) ); 
                                          } 
                                     ?>
                                  </div>
                              </div>  
                     </div>  
                </div>
            </div>
        </div>
      </div>
          
          <?php 
           get_template_part("header","banner"); 
          ?>
    </div>