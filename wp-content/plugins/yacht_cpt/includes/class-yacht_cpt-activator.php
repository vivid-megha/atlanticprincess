<?php

/**
 * Fired during plugin activation
 *
 * @link       http://veepixel.com/
 * @since      1.0.0
 *
 * @package    Yacht_cpt
 * @subpackage Yacht_cpt/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Yacht_cpt
 * @subpackage Yacht_cpt/includes
 * @author     Ven Bradshaw <arjunupdates1001@gmail.com>
 */
class Yacht_cpt_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
