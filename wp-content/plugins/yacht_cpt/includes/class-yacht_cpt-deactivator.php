<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://veepixel.com/
 * @since      1.0.0
 *
 * @package    Yacht_cpt
 * @subpackage Yacht_cpt/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Yacht_cpt
 * @subpackage Yacht_cpt/includes
 * @author     Ven Bradshaw <arjunupdates1001@gmail.com>
 */
class Yacht_cpt_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
