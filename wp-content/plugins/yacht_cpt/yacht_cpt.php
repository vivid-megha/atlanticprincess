<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://veepixel.com/
 * @since             1.0.0
 * @package           Yacht_cpt
 *
 * @wordpress-plugin
 * Plugin Name:       Yacht Custom post type
 * Plugin URI:        http://veepixel.com/demo/yacht/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Ven Bradshaw
 * Author URI:        http://veepixel.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       yacht_cpt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-yacht_cpt-activator.php
 */
function activate_yacht_cpt() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-yacht_cpt-activator.php';
	Yacht_cpt_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-yacht_cpt-deactivator.php
 */
function deactivate_yacht_cpt() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-yacht_cpt-deactivator.php';
	Yacht_cpt_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_yacht_cpt' );
register_deactivation_hook( __FILE__, 'deactivate_yacht_cpt' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-yacht_cpt.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_yacht_cpt() {

	$plugin = new Yacht_cpt();
	$plugin->run();
add_action('init', 'photograph_init', 0);
	if ( !function_exists( 'photograph_init' ) ) {
        if ( !post_type_exists('portfolio') ) {		
			function photograph_init() {
						// Our Team
		 $labels = array(
		    'name' => __('Team', 'yachtsailing'),
		    'singular_name' => __('Team', 'yachtsailing'),
		    'add_new' => __('New Member', 'yachtsailing'),
		    'add_new_item' => __('Add New Member', 'yachtsailing'),
		    'edit_item' => __('Edit Member', 'yachtsailing'),
		    'new_item' => __('All Members', 'yachtsailing'),
		    'view_item' => __('View Member', 'yachtsailing'),
		    'search_items' => __('Search Member', 'yachtsailing'),
		    'not_found' =>  __('No Member found', 'yachtsailing'),
		    'not_found_in_trash' => __('Team', 'yachtsailing'),
		    'parent_item_colon' => '',
		    'menu_name' => __('Team', 'yachtsailing')
	    );
	    $args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'menu_position' => 4.5,
            'menu_icon' => 'dashicons-groups',
		    'capability_type' => 'page',
		    'hierarchical' => false,
		    'has_archive' => true,
		    'rewrite' => array( 'slug' => 'team', 'with_front' => false ),
		    'supports' => array('title','editor','thumbnail','custom-fields'),
		    'taxonomies' => array(),
	    );
	    register_post_type( 'yachtsailing_team' , $args );
		
		
		
	// Our Testimonials
		 $labels = array(
		    'name' => __('Manage Testimonials', 'yachtsailing'),
		    'singular_name' => __('Testimonial', 'yachtsailing'),
		    'add_new' => __('Add New Testimonial', 'yachtsailing'),
		    'add_new_item' => __('Add New Testimonial', 'yachtsailing'),
		    'edit_item' => __('Edit Testimonial', 'yachtsailing'),
		    'new_item' => __('All Testimonials', 'yachtsailing'),
		    'view_item' => __('View Testimonial', 'yachtsailing'),
		    'search_items' => __('Search Testimonials', 'yachtsailing'),
		    'not_found' =>  __('Testimonials not found', 'yachtsailing'),
		    'not_found_in_trash' => __('Testimonials', 'yachtsailing'),
		    'parent_item_colon' => '',
		    'menu_name' => __('Testimonials', 'yachtsailing')
	    );
	    $args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'menu_position' => 4.5,
            'menu_icon' => 'dashicons-format-quote',
		    'capability_type' => 'page',
		    'hierarchical' => false,
		    'has_archive' => true,
		    'rewrite' => array( 'slug' => 'testimonials', 'with_front' => false ),
		    'supports' => array('title', 'editor', 'author', 'thumbnail'),
		    'taxonomies' => array(),
	    );
	    register_post_type( 'yachtsailing_testi' , $args );
		
		
		
	// Our Destinations
		 $labels = array(
		    'name' => __('Manage Destinations', 'yachtsailing'),
		    'singular_name' => __('Destination', 'yachtsailing'),
		    'add_new' => __('Add New Destination', 'yachtsailing'),
		    'add_new_item' => __('Add New Destination', 'yachtsailing'),
		    'edit_item' => __('Edit Destination', 'yachtsailing'),
		    'new_item' => __('All Destinations', 'yachtsailing'),
		    'view_item' => __('View Destination', 'yachtsailing'),
		    'search_items' => __('Search Destinations', 'yachtsailing'),
		    'not_found' =>  __('Destinations not found', 'yachtsailing'),
		    'not_found_in_trash' => __('Destinations', 'yachtsailing'),
		    'parent_item_colon' => '',
		    'menu_name' => __('Destinations', 'yachtsailing')
	    );
	    $args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'menu_position' => 4.5,
            'menu_icon' => 'dashicons-location',
		    'capability_type' => 'page',
		    'hierarchical' => false,
		    'has_archive' => true,
		    'rewrite' => array( 'slug' => 'destination', 'with_front' => false ),
		    'supports' => array('title', 'editor', 'author', 'thumbnail'),
		    'taxonomies' => array(),
	    );
	    register_post_type( 'yachtsailing_desti' , $args );
		
		
		
	// Our Destinations
		 $labels = array(
		    'name' => __('Manage Charters', 'yachtsailing'),
		    'singular_name' => __('Charter', 'yachtsailing'),
		    'add_new' => __('Add New Charter', 'yachtsailing'),
		    'add_new_item' => __('Add New Charter', 'yachtsailing'),
		    'edit_item' => __('Edit Charter', 'yachtsailing'),
		    'new_item' => __('All Charters', 'yachtsailing'),
		    'view_item' => __('View Charter', 'yachtsailing'),
		    'search_items' => __('Search Charters', 'yachtsailing'),
		    'not_found' =>  __('Charters not found', 'yachtsailing'),
		    'not_found_in_trash' => __('Charters', 'yachtsailing'),
		    'parent_item_colon' => '',
		    'menu_name' => __('Charters', 'yachtsailing')
	    );
	    $args = array(
		    'labels' => $labels,
		    'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'query_var' => true,
		    'menu_position' => 4.5,
            'menu_icon' => 'dashicons-share-alt',
		    'capability_type' => 'page',
		    'hierarchical' => false,
		    'has_archive' => true,
		    'rewrite' => array( 'slug' => 'charter', 'with_front' => false ),
		    'supports' => array('title', 'editor', 'author', 'thumbnail'),
		    'taxonomies' => array(),
	    );
	    register_post_type( 'yachtsailing_charter' , $args );

			}
		}
	}
}
run_yacht_cpt();
