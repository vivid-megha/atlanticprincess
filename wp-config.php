<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'atlanti7_atlanticprincess');

/** MySQL database username */
define('DB_USER', 'atlanti7_prince');

/** MySQL database password */
define('DB_PASSWORD', '#g@O{Jf*aQ01');

/** MySQL hostname */
define('DB_HOST', 'localhost');

define('WP_HOME','https://www.atlanticprincess.com');
define('WP_SITEURL','https://www.atlanticprincess.com');

/** change autosave time to 3 minutes **/
define('AUTOSAVE_INTERVAL', 180); // seconds
/** limit post revions **/
define('WP_POST_REVISIONS', 60);


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(<=HFKrdjKz#9=9}gYwr]Hd>-im,[=2mn)q4qG3n||=@dce/JSU9O2_D;D}ltE$&');
define('SECURE_AUTH_KEY',  'iAD9)DX<f8}GsVC!+A gKzONJUn4-`$d<4@% Ja)38VRo}IXbw$v,t5b{+3o+N1u');
define('LOGGED_IN_KEY',    '# MiAZRC~oQ,>OjBxMe#BZCg7z_VoxP)d?POI~|sH%g-M$g)Pgx#K? 15}B9Kx_=');
define('NONCE_KEY',        'ehSt)LM6m1g|W_m.+ct]Ok|R{Y#6/QC(.tm3F-4Pq|4{YuC*CclkQg|OKo /q]:R');
define('AUTH_SALT',        ':D8]a[XK$Dr|`QPNGRc{U@aM:ivd.[[x]pxe/0h/~bOqae]jC7([}wfDKB~jr%Fo');
define('SECURE_AUTH_SALT', 'IpiXqZ)8Z5a-c(7zXIs$nLx@DL:;ysq([gj>hn,F>QqXi|3/$Qh1Q>G7-rpx^x*j');
define('LOGGED_IN_SALT',   'irn);)_c-t4iSw;*/c?Gw7-ief,^utBCUYOy+fIGfYNyd*f`?lDmccyqB2&5Noy<');
define('NONCE_SALT',       '1J$r;lX5b50JQL;WsT;6Hzz|CkDBYPn1k)r-5T#*J)vaM!dDrPIBjJ~Oe9VE9X,w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define( 'WP_MEMORY_LIMIT', '256M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
